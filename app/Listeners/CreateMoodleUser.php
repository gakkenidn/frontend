<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Curl;

class CreateMoodleUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        if (!env('P2KB_URL', false)) 
            return;

        $user = $event->user;
        $apiUrl = env('P2KB_URL') . '/moodle/webservice/rest/server.php';
        $student = [
            "username" => strtolower($user->email),
            "password" => str_random(16),
            "firstname" => $user->name,
            "lastname" => $user->name,
            "email" => $user->email,
            "auth" => "gakken",
            "idnumber" => 'GKSSO-' . str_pad($user->id, 8, '0', STR_PAD_LEFT),
            "lang" => "id",
            "calendartype" => "",
            "theme" => "",
            "timezone" => "Asia/Makassar",
            "mailformat" => 0,
            "description" => "",
            "city" => "",
            "country" => "",
            "firstnamephonetic" => "",
            "lastnamephonetic" => "",
            "middlename" => "",
            "alternatename" => ""
        ];
        Curl::to($apiUrl)
            ->withData([
                'wstoken' => env('P2KB_API_TOKEN', ''),
                'wsfunction' => 'core_user_create_users',
                'moodlewsrestformat' => 'json',
                'users' => [$student]
                ])
            ->enableDebug(storage_path('logs/curl-CreateMoodleUser' . \Carbon\Carbon::now()->format('YmdHis') . '.log'))
            ->post();
    }
}
