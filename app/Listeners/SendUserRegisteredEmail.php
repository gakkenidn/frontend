<?php

namespace App\Listeners;

use App\Mail\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendUserRegisteredEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(\App\Events\UserRegistered $event)
    {
        $isEmail = filter_var($event->user->email, FILTER_VALIDATE_EMAIL );
        if ($isEmail) {
            Mail::queue(new UserRegistered($event->user));
        }
    }
}
