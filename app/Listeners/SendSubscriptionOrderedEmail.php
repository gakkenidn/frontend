<?php

namespace App\Listeners;

use App\Events\SubscriptionOrderSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendSubscriptionOrderedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SubscriptionOrderSaved  $event
     * @return void
     */
    public function handle(SubscriptionOrderSaved $event)
    {
        Mail::queue(new \App\Mail\SubscriptionOrdered($event->order));
    }
}
