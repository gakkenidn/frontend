<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\PasswordChanged' => [
            'App\Listeners\SendPasswordChangedEmail',
        ],
        'App\Events\SubscriptionOrderSaved' => [
            'App\Listeners\SendSubscriptionOrderedEmail',
        ],
        'App\Events\UserRegistered' => [
            'App\Listeners\SendUserRegisteredEmail',
            'App\Listeners\SetUserNameOnCertificate',
            'App\Listeners\CreateMoodleUser',
            'App\Listeners\SubscribeUserToMailchimp',
        ],
        'App\Events\UserEnrolled' => [
            'App\Listeners\EnrolUserToMoodle',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
