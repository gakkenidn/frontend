<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    const MORPH_MAP = [
        'topic'      => 'App\Vault\Topic'
    ];
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    
    public function boot()
    {
        Relation::morphMap( AppServiceProvider::MORPH_MAP );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        \Laravel\Passport\Passport::ignoreMigrations();
    }
}
