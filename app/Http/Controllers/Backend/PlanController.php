<?php

namespace App\Http\Controllers\Backend;

use Fractal;
use Illuminate\Http\Request;
use App\Accounts\Subscription;
use App\Http\Controllers\Controller;
use App\Transformers\SubscriptionTransformer;

class PlanController extends Controller
{
  
  public function index(Request $request)
  {
  	return response()->json(Fractal::collection(Subscription::notCustom()->get())->transformWith(new SubscriptionTransformer));
  }
}
