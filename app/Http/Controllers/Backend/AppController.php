<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Accounts\App;
use App\Accounts\ModelHasApps;
use App\Accounts\User;

class AppController extends Controller
{
	public function index(Request $request)
	{
			$apps = App::orderBy('created_at', 'desc');
			if ($request->term)
					$apps->where('name', 'like', '%' . $request->term . '%');

			if ($request->wantsJson()) {
					return response()->json(
							$apps->paginate()
					);
			}
	}

	public function show(Request $request, App $app)
	{
			return response()->json([
					'app' => $app
			]);
	}

	public function store(Request $request)
	{
			$this->validate($request, [
					'name' => 'required|string',
					'url' => 'required|string',
			]);

			$app = new App;
					$app->name = $request->name;
					$app->url = $request->url;
			$app->key = bcrypt(str_random(8));
			$app->save();

			return response()->json([
					'app' => $app->toJSON()
			]);
	}

	public function update(Request $request, App $app)
	{
			$this->validate($request, [
					'name' => 'required|string',
					'url' => 'required|string',
			]);

			$app->name = $request->name;
			$app->url = $request->url;
			
			$app->save();
			return $this->show($request, $app);
	}

	public function destroy(Request $request)
	{

	}

	public function users(Request $request)
	{
			$users = [];

			switch ($request->context) {
					case 'availableToAdd':
							$users = [
								'users' => User::whereNotIn('id', function ($query) use ($request) {
										$query
												->select('model_id')
												->from('model_has_apps')
												->where('app_id', $request->id)
												->where('model_type', 'App\Accounts\User');
										})->where('name', 'like', '%' . $request->term . '%')->get()
							];
							break;
					
					default:
							$users = App::find($request->id)->users()->paginate(10);
							break;
			}

			return response()->json( $users );
	}

	public function assignuser(Request $request) 
	{
			$app = App::find( $request->id );

			try {
				$app->users()->attach( $request->user );
				return response()->json(['message' => "success"]);
			} catch (\Throwable $th) {
				return response()->json(['message' => "error"]);
			}
	}

	public function removeUser(Request $request) 
	{
			$app = App::find( $request->id );

			try {
				$app->users()->detach( $request->user );
				return response()->json(['message' => "success"]);
			} catch (\Throwable $th) {
				return response()->json(['message' => "error"]);
			}
	}
}
