<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Storage;

class PitPogiController extends Controller
{

	protected $videos;

	public function __construct()
	{
		if ('cli' != php_sapi_name()) {
			try {
				$file = Storage::get('pit-pogi/list.json');
				$this->videos = collect(json_decode($file));
			} catch(FileNotFoundException $e) {
				return abort(404);
			}
		}
	}

  public function index(Request $request)
  {
    return view('pit-pogi.home', [
    	'title' => page_title('PIT-POGI XXIII Makassar'),
    	'videos' => $this->videos
    	]);
  }

  public function video(Request $request, $slug)
  {
  	$video = $this->videos->where('name', $slug)->first();
  	if (!$video)
  		return abort(404);

  	return view('pit-pogi.video', [
  		'title' => page_title('PIT-POGI XXIII: ' . $video->title),
    	'video' => $video
  		]);
  }

  public function file(Request $request, $slug)
  {
  	$video = $this->videos->where('name', $slug)->first();
  	if (!$video)
  		return abort(404);

  	return response()->file(storage_path('app/pit-pogi/' . $video->file));
  }
}
