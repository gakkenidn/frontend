<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Storage;

class MarineController extends Controller
{

	protected $videos;

	public function __construct()
	{
		if ('cli' != php_sapi_name()) {
			try {
				$file = Storage::get('marine/list.json');
				$this->videos = collect(json_decode($file));
			} catch(FileNotFoundException $e) {
				return abort(404);
			}
		}
	}

  public function index(Request $request)
  {
    return view('marine.home', [
    	'title' => page_title('MARINE Symposium'),
    	'videos' => $this->videos
    	]);
  }

  public function video(Request $request, $slug)
  {
  	$video = $this->videos->where('name', $slug)->first();
  	if (!$video)
  		return abort(404);

  	return view('marine.video', [
  		'title' => page_title('MARINE Symposium: ' . $video->title),
    	'video' => $video
  		]);
  }
}
