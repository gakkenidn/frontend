<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegistered;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Auth;
use Socialite;
use Carbon\Carbon;

use App\Accounts\UserToken;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function generateToken() 
    {
        $user_token = new UserToken;
        $user_token->user_id = Auth::id();
        $user_token->token = Str::random(32);
        $user_token->expires_at = Carbon::now()->addSeconds(30);
        $user_token->save();

        return $user_token->token;
    }

    public function login(Request $request) {
        $redirect = false;
        if ($request->redirect) $redirect = $request->redirect;
        elseif ($request->appredirect) $redirect = $request->appredirect;

        return view('auth/login', ["redirect" => $redirect ? $redirect : "/"]);
    }

    public function doLogin(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $redirect = false;
            if ($request->redirect) $redirect = $request->redirect;
            elseif ($request->appredirect) $redirect = urldecode($request->appredirect) . '/auth/sso/' . $token;

            $token = $this->generateToken();
            return redirect()->intended($redirect ? $redirect : '/');
        } else {
            return redirect()->intended('/login')->withErrors(['email', 'Account not found']);
        }
    }

    public function logout(\Illuminate\Http\Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        if ($request->wantsJson())
            return response()->json(['status' => 'success']);
        return redirect('/');
    }

    public function redirectToProvider($provider)
    {
        if (!in_array($provider, ['facebook', 'google', 'twitter']))
            return abort(404);

        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback(Request $request, $provider)
    {
        if (!in_array($provider, ['facebook', 'google', 'twitter']))
            return abort(404);

        $socialUser = Socialite::driver($provider)->user();
        $user = \App\Accounts\User::where('email', $socialUser->getEmail())->first();
        $register = false;

        // Create new user if email doesn't exist
        if (!$user) {
            $user = new \App\Accounts\User;
            $user->name = $socialUser->getName();
            $user->email = $socialUser->getEmail();
            $user->password = bcrypt(str_random(32));
            $user->avatar_url = $socialUser->getAvatar();
            $user->is_provider = true;
            $user->role_id = \App\Accounts\Role::where('is_user', true)->first()->id;
            $user->verify_token = '';
            $user->verified_at = \Carbon\Carbon::now();
            $user->save();

            event(new UserRegistered($user));
            $request->session()->flash('profession-select', true);
            $register = true;
        }

        Auth::login($user, true);
        $this->generateToken();
        return redirect('/' . ($register ? '?signup=success' : ''));
    }

    public function handleCheckToken(Request $request)
    {
        $user_token = UserToken::where('token', $request->token)
            ->with('user')
            ->whereHas('user.apps', function($query) use ($request) {
                $query->where('key', $request->sso_key);
            })
            ->active()
            ->first();

        return [
            "user" => $user_token ? $user_token->user : null
        ];
    }
    
}
