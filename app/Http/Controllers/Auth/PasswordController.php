<?php

namespace App\Http\Controllers\Auth;

use App\Events\PasswordChanged;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PasswordController extends Controller
{

  public function edit(Request $request)
  {
    return view('profile.password', [
      'inverseNavbar' => true,
      'title' => page_title('Ubah Kata Sandi'),
      'user' => $request->user()
      ]);
  }
  
  public function patch(Request $request)
  {
  	$this->validate($request, [
  		'password' => 'required',
  		'newPassword' => 'required|min:8|confirmed|different:password',
  		]);

  	$user = $request->user();
  	if (!\Illuminate\Support\Facades\Hash::check($request->password, $user->password)) {
      if ($request->wantsJson())
  	  	return response()->json(['password' => ['Kata sandi tidak sesuai']], 422);

      $request->session()->flash('alert-danger', 'Kata sandi tidak sesuai.');
      return redirect(route('profile'));
    }
  		
  	$user->password = bcrypt($request->newPassword);
  	$user->save();

    event(new PasswordChanged($user));

    if ($request->wantsJson())
    	return response()->json(['status' => 'success']);

    $request->session()->flash('alert-success', 'Kata sandi Anda berhasil diubah.');
    return redirect(route('profile'));
  }
}
