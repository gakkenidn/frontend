<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JobController extends Controller
{
    public function index(Request $request)
    {
        return view('new/job/list', [
            'jobs' => \App\Vault\Job::orderBy('created_at', 'desc')->take(8)->get() ,
            'nearJobs' => \App\Vault\Job::orderBy('created_at', 'desc')->take(4)->get()
		]);
    }

    public function single(Request $request)
    {
        return view('new/job/single', [
            'job' => \App\Vault\Job::where('slug', $request->slug)->first() ,
            'relatedJob' => \App\Vault\Job::where('slug', '<>',$request->slug)->take(4)->get()
		]);
    }
}
