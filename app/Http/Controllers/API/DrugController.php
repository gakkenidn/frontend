<?php

namespace App\Http\Controllers\API;

use App\Vault\Drug;
use Illuminate\Http\Request;
use App\Transformers\DrugTransformer;
use App\Http\Controllers\Controller;

class DrugController extends Controller
{

    public function index(Request $request)
    {
        if ($request->has('relatedTo'))
            return $this->_indexRelated($request);

        $drugs = Drug::orderBy('name');
        if ($request->has('term'))
            $drugs->where('name', 'like', '%' . trim($request->term) . '%');

        return fractal()
            ->collection($drugs->get())
            ->transformWith(new DrugTransformer)
            ->respond();
    }

    public function show(Request $request, $slug)
    {
        return fractal()
            ->item(Drug::where('slug', $slug)->firstOrFail())
            ->transformWith(new DrugTransformer(true))
            ->respond();
    }

    public function _indexRelated(Request $request)
    {
        $relatable = Drug::where('slug', $request->relatedTo)->firstOrFail();
        $byManufacturer = Drug::where('drug_manufacturer_id', $relatable->drug_manufacturer_id)->get();
        $byClass = Drug::where(
            $relatable->drug_subclass_id ? 'drug_subclass_id' : 'drug_class_id',
            $relatable->drug_subclass_id || $relatable->drug_class_id
            )
            ->get();

        return response()->json([
            'data' => [
                'byManufacturer' => fractal()->collection($byManufacturer->take(10)->values())->transformWith(new DrugTransformer)->toArray(),
                'byClass' => fractal()->collection($byClass->take(10)->values())->transformWith(new DrugTransformer)->toArray(),
            ]]);
    }
}
