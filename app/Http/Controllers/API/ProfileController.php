<?php

namespace App\Http\Controllers\API;

use Fractal;
use Validator;
use App\Accounts\Role;
use App\Accounts\User;
use App\Events\UserRegistered;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;

class ProfileController extends Controller
{
	public function index(Request $request)
	{
		$user = request()->user();
		return response()->json(Fractal::item($user)->transformWith(new UserTransformer)->toArray());
	}

	public function store(Request $request)
	{
		$user = $request->user();
		$validator = Validator::make($request->all(), [
			'idNo'				=> 'min:8',
			'name'				=> 'required|min:3',
			'password'			=> 'nullable|min:8',
			'profession'		=> 'in:doctor,dentist',
			'passwordConfirm'	=> 'nullable|same:password',
		]);
		if ($validator->fails())
			return response($validator->errors()->toJson(), 422);

		$user->name = $request->name;
		$user->id_no = $request->idNo;
		$user->profession = $request->profession;
		if ($request->password)
			$user->password = bcrypt($request->password);

		$user->save();
		return fractal()->item($user)->transformWith(new UserTransformer)->respond();
	}

	public function register(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name'				=> 'required|min:3',
			'email'				=> 'required|email|unique:users',
			'password'			=> 'min:8',
			'passwordConfirm'	=> 'same:password',
		]);

		if ($validator->fails())
			return response($validator->errors()->toJson(), 422);

		$user = new User;
		$user->name = $request->name;
		$user->email = $request->email;
		$user->password = bcrypt($request->password);
		$user->role_id = Role::where('is_user', true)->first()->id;
		$user->verify_token = str_random(64);
		$user->save();

		event(new UserRegistered($user));

		return fractal()->item($user)->transformWith(new UserTransformer)->respond();
	}

}
