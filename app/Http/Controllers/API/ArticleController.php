<?php

namespace App\Http\Controllers\API;

use Fractal;
use App\WP\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\ArticleTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ArticleController extends Controller
{
  public function index(Request $request)
  {
    $posts = Post::articles()
      ->published()
      ->with(['featuredImages', 'featuredImages.attachmentMetadata', 'subheading', 'author']);

    if ($request->term)
      $posts = $posts->where('post_title', 'like', '%' . $request->term . '%');
    $posts = $posts->paginate(10)
      ->appends(['term' => $request->term]);

  	return Fractal::collection($posts)
      ->transformWith(new ArticleTransformer)
      ->paginateWith(new IlluminatePaginatorAdapter($posts));
  }

  public function show(Request $request, $slug)
  {
    $post = Post::articles()
      ->published()
      ->with(['featuredImages', 'author'])
      ->where('post_name', $slug)
      ->firstOrFail();
    $popularPostsData = $post->popularPostsData;
    if ($popularPostsData) {
      $popularPostsData->pageviews = $popularPostsData->pageviews + 1;
    } else {
      $popularPostsData = new \App\WP\PopularPostsData;
      $popularPostsData->day = \Carbon\Carbon::now();
      $popularPostsData->postid = $post->ID;
    }

    $popularPostsData->last_viewed = \Carbon\Carbon::now();
    $popularPostsData->save();
  	return Fractal::item($post)->transformWith(new ArticleTransformer('show'))->toArray();
  }
}
