<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InfographicController extends Controller
{
    public function index(Request $request)
    {
        return view('new/infographic/list', [
            'articles' => $category->termTaxonomy->posts()
				->published()
				->with([
					'featuredImages',
					'featuredImages.attachmentMetadata',
					'author',
					'subheading',
					'categoryTaxonomies',
					'categoryTaxonomies.term',
					'popularPostsData'
				])->simplePaginate(10)
        ]);
    }
}
