<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        $category = \App\WP\Term::where('slug', 'infografik')->first();
        $infographics = [];
        
        if ($category) {
            $infographics = $category->termTaxonomy->posts()
            ->published()
            ->with([
                'featuredImages',
                'featuredImages.attachmentMetadata',
                'author',
                'subheading',
                'categoryTaxonomies',
                'categoryTaxonomies.term',
                'popularPostsData'
            ])
            ->take(4)
            ->get();
        }

    	return view('new/home', [
            'sliders' => \App\WP\PostSticky::post() ,
            'recentArticles' => \App\WP\Post::articles()
                ->whereHas('categoryTaxonomies.term', function ($q) {
                    $q->where('slug', '!=', 'infografik');
                })
                ->with(['featuredImages', 'author', 'subheading','categoryTaxonomies', 'categoryTaxonomies.term',])
                ->published()
                ->take(6)
                ->get(),
            'popularArticles' => \App\WP\PopularPostsData::orderBy('pageviews', 'desc')
                ->whereHas('post', function($query) {
                    $query->whereYear('post_date', '=', date('Y'));
                    $query->orWhere(function($nest) {
                        $nest->whereYear('post_date', '=', 2018);
                    });
                })
                ->with(['post', 'post.author', 'subheading','featuredImages', 'categoryTaxonomies', 'categoryTaxonomies.term'])
                ->take(4)
                ->get(),
            'infographics' => $infographics ,
            'p2kbTopics' => \App\Vault\Topic::p2kb()->published()->take(4)->get(),
            'p3kgbTopics' => \App\Vault\Topic::p3kgb()->published()->take(3)->get(),
            'promptPitPogiPage' => \Storage::exists('pit-pogi/list.json') ,
            'recentEvents' => \App\Vault\Event::orderBy('end_at', 'desc')->take(4)->get(),
            'sponsor' =>  \App\Vault\Sponsor::with(['sponsorContent.content'])->first() ,
            'inverseNavbar' => true
        ]);
    }

    public function p2kb(Request $request)
    {
        $professionStatus = !$request->user() ? 'undefined' : $request->user()->profession ?: 'undefined';
        $subscribeStatus = !$request->user() ? false : $request->user()->userSubscriptions()->active()->count() > 0;

    	return view('p2kb', [
            'newReleaseP2KB' => in_array($professionStatus, ['doctor', 'undefined']) ? \App\Vault\Topic::p2kb()->newRelease('doctor')->first() : null,
    		'newReleaseP3KGB' => in_array($professionStatus, ['dentist', 'undefined']) ? \App\Vault\Topic::p3kgb()->newRelease('dentist')->first() : null,
            'backnumbersP2KB' => in_array($professionStatus, ['doctor', 'undefined']) ? \App\Vault\Topic::p2kb()->backnumbers('doctor')->orderBy('release', 'desc')->take(12)->get() : null,
    		'backnumbersP3KGB' => in_array($professionStatus, ['dentist', 'undefined']) ? \App\Vault\Topic::p3kgb()->backnumbers('dentist')->orderBy('release', 'desc')->take(12)->get() : null,
            'promptP2KBSubscription' => in_array($professionStatus, ['doctor', 'undefined']) && !$subscribeStatus,
            'promptP3KGBSubscription' => in_array($professionStatus, ['dentist', 'undefined']) && !$subscribeStatus,
            'title' => page_title('Gakken P2KB')
    		]);
    }

    public function service(Request $request, $service)
    {
        if (!in_array($service, ['journals', 'drugs', 'genius'], true))
            return abort(404);

        $data = [];
        switch ($service) {
            case 'journals':
                $data['journals'] = \App\Vault\Journal::all()->random(5);
                $data['title'] = page_title('Jurnal Premium');
                break;
            case 'drugs':
                $data['title'] = page_title('Indeks Obat');
                break;
            case 'genius':
                $data['title'] = page_title('GENIUS');
                break;

        }

        return view('service-' . $service, $data);
    }

}
