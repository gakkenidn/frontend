<?php

namespace App\Http\Controllers;

use Artisan;
use Fractal;
use Storage;
use Dompdf\Dompdf;
use App\Vault\Topic;
use App\Vault\Content;
use App\Accounts\User;
use App\Accounts\ApiToken;
use App\Accounts\UserTopic;
use Illuminate\Http\Request;
use App\Transformers\UserTopicTransformer;
use App\Transformers\TopicsListTransformer;

class TopicController extends Controller
{

	public function single(Request $request, $slug, $topic = false)
	{
		$topic = $topic ?: \App\Vault\Topic::where('slug', $slug)->with(['contents', 'lecturers'])->firstOrFail();
		if ($request->wantsJson()) {
			if ($request->user()->userTopics()->where('topic_id', $topic->id)->count()) {
				$userTopic = UserTopic::where('user_id', $request->user()->id)
					->where('topic_id', $topic->id)
					->firstOrFail();
				return response()->json(Fractal::item($userTopic)->transformWith(new UserTopicTransformer('app-index', $topic))->toArray());
			}
		}

		return view('topic.single', ['topic' => $topic]);
	}

	public function own(Request $request)
	{
		$topics = $request->user()->userTopics()->where('is_accessible', true)->with('topic')->orderBy('id', 'desc')->get();
		return response()->json(Fractal::collection($topics)->transformWith(new TopicsListTransformer())->toArray());
	}

	public function contents(Request $request, $slug)
	{
		return response()->make(
			$request->user()->topics()->where('slug', $slug)->firstOrFail()->contents
			);
	}

	public function viewContent(Request $request)
	{
		$topicContent = Content::where('slug', $request->content['slug'])->firstOrFail()
			->topicContents()
			->whereHas('topic', function ($query) use ($request)
			{
				$query->where('slug', $request->topic);
			})->first();
		$userTopic = $request->user()->userTopics()->where('topic_id', $topicContent->topic_id)->firstOrFail();
		$viewedContents = collect($userTopic->viewed_contents);
		$userTopic->viewed_contents = $viewedContents->push($topicContent->id)->unique();
		$userTopic->save();
		$topic = $topicContent->topic;
		return $this->single($request, $topic->slug, $topic);
	}

	public function certificate(Request $request, $topic)
	{
		if ($request->hasHeader('api-token'))
			$user = ApiToken::where('token', $request->header('api-token'))->firstOrFail()->user;
		else $user = $request->user();

		if (!$user)
			return abort(404);

		$topic = \App\Vault\Topic::where('slug', $topic)->firstOrFail();

		if ($request->has('user')) {
			if (!$user->isAdmin()) abort(404);
			$user = User::where('email', $request->user)->firstOrFail();
		}
		$userTopic = $topic->userTopics()->where('user_id', $user->id)->firstOrFail();

		if (!$userTopic->passed_at)
			return abort(404, 'Not Found');
		if ($userTopic->is_trial)
			return abort(404, 'Not Found');

		$dompdf = new Dompdf();
		$dompdf->loadHtml(view('topic.certificate', [
			'topic' => $topic,
			'user' => $user,
			'certificateNo' => $userTopic->certificate_no,
			'passedAt' => $userTopic->passed_at,
			'isTrial' => $userTopic->is_trial
			])->render());
		$dompdf->setPaper('A4', 'landscape');

		$dompdf->render();
		$dompdf->stream('Certificate' . str_replace(' ', '', title_case($topic->title)) . '.pdf', ['Attachment' => false]);
	}

	public function ownCertificates(Request $request)
	{
		$passedTopics = $request->user()->userTopics()
			->whereNotNull('passed_at')
			->whereNotNull('certificate_no')
			->where('is_trial', false)
			->with('topic')
			->get();
		return response()->json(
			fractal()
				->collection($passedTopics)
				->transformWith(new UserTopicTransformer('index'))
				->toArray()
		);
	}

	public function file(Request $request, $filename)
	{
		$file = Content::file()->where('data', $filename)->firstOrFail();

		// $apiToken = $request->header('api-token') ? ApiToken::active()->where('token', $request->header('api-token'))->first() : null;
		// $user = $apiToken ? $apiToken->user : $request->user();

		// // allow iPhone app user agent because the don't send session cookies
		// $iPhoneSession = 'GakkenP2KB/1.0 (iPhone; iOS 10.3.3; Scale/2.00)';

		// if (!$file->is_free && $request->userAgent() != $iPhoneSession) {
		// 	if (!$user) return abort(403, 'Unauthorized');
		// 	if (!$user->userSubscriptions()->active()->count()) return abort(403, 'Unauthorized');
		// }
		// if (!Storage::disk('local')->exists('vault/content-files/' . $filename))
		// 	return abort(404);
		return response()->file(storage_path('app/vault/content-files/' . $filename));
	}

	public function enableTrial(Request $request)
	{
		if (!$request->user()->isTrialEnableEligible())
			return abort(403, 'Unauthorized');

		$this->validate($request, ['topic.slug' => 'required|exists:mysql_vault.topics,slug']);
		$topic = Topic::where('slug', $request->topic['slug'])->first();

		$enableTopic = $request->user()->userTopics()->where('topic_id', $topic->id)->first();
		if (!$enableTopic)
			return abort(403, 'Unauthorized');

		$request->user()->userTopics()->where('is_trial', true)->update(['is_accessible' => false]);
		$enableTopic->is_accessible = true;
		$enableTopic->is_trial = false;
		$enableTopic->save();

		return response()->json(['status' => 'success']);
	}

}
