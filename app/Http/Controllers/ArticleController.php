<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArticleController extends Controller
{

	public function index(Request $request)
	{
		return view('article.home', [
			'articles' => \App\WP\Post::articles()
				->published()
				->with([
					'featuredImages',
					'featuredImages.attachmentMetadata',
					'author',
					'subheading',
					'categoryTaxonomies',
					'categoryTaxonomies.term',
					'popularPostsData'
					])
				// ->simplePaginate(4),
				->simplePaginate(10),
			'categories' => \App\WP\TermTaxonomy::where('taxonomy', 'category')
				->where('count', '>', 0)
				->orderBy('count', 'desc')
				->with('term')
				// ->with(['term', 'posts' => function($query) {
				// 	return $query->limit(4);
				// }])
				// ->take(3)
				->get(),
			'populars' => \App\WP\PopularPostsData::orderBy('pageviews', 'desc')
				->with(['post', 'subheading','featuredImages', 'categoryTaxonomies', 'categoryTaxonomies.term'])
				->take(5)
				// ->take(4)
				->get(),
			'title' => page_title('Artikel Kesehatan')
			]);
	}

	public function single(Request $request, $slug)
	{
		$article = \App\WP\Post::articles()->published()->where('post_name', $slug)->firstOrFail();
		$popularPostsData = $article->popularPostsData;
		if ($popularPostsData) {
			$popularPostsData->pageviews = $popularPostsData->pageviews + 1;
		} else {
			$popularPostsData = new \App\WP\PopularPostsData;
			$popularPostsData->day = \Carbon\Carbon::now();
			$popularPostsData->postid = $article->ID;
		}

		$popularPostsData->last_viewed = \Carbon\Carbon::now();
		$popularPostsData->save();
		return view('article.single', [
			'article' => $article,
			'title' => $article->post_title . ' | ' . config('app.name') ,
			'relatedArticle' => \App\WP\Post::articles()
				->published()
				->with([
					'featuredImages',
					'featuredImages.attachmentMetadata',
					'author',
					'subheading',
					'categoryTaxonomies',
					'categoryTaxonomies.term' => function($query) use($article) {
						$query->where('name', $article->categoryTaxonomies[0]->term->name);
					},
					'popularPostsData'
					])
				->where('post_name', '<>', $article->post_name)
				->take(4)
				->get()
		]);
	}

	public function category(Request $request, $slug)
	{
		$category = \App\WP\Term::where('slug', $slug)->firstOrFail();
		return view('article.category', [
			'category' => $category,
			'articles' => $category->termTaxonomy->posts()
				->published()
				->with([
					'featuredImages',
					'featuredImages.attachmentMetadata',
					'author',
					'subheading',
					'categoryTaxonomies',
					'categoryTaxonomies.term',
					'popularPostsData'
				])->simplePaginate(10),
			'categories' => \App\WP\TermTaxonomy::where('taxonomy', 'category')
				->where('count', '>', 0)
				->orderBy('count', 'desc')
				->with('term')
				->get(),
			'populars' => \App\WP\PopularPostsData::orderBy('pageviews', 'desc')
				->with(['post', 'post.subheading'])
				->take(5)
				->get(),
			'title' => page_title('Artikel ' . $category->name)
			]);
	}

}
