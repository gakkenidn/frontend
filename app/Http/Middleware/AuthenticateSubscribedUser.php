<?php

namespace App\Http\Middleware;

use Closure;

class AuthenticateSubscribedUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->user())
            return abort(404);
        if (!$request->user()->userSubscriptions()->active()->first())
            return abort(404);

        return $next($request);
    }
}
