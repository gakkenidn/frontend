<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

use App\Accounts\UserToken;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

    public function generateToken() 
    {
        $user_token = new UserToken;
        $user_token->user_id = Auth::id();
        $user_token->token = str_random(32);
        $user_token->expires_at = Carbon::now()->addSeconds(30);
        $user_token->save();

        return $user_token->token;
    }

    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $token = $this->generateToken();
            $redirect = false;
            if ($request->redirect) $redirect = $request->redirect;
            elseif ($request->appredirect) $redirect =  urldecode($request->appredirect) . "/auth/sso/" . $token;
            
            return redirect($redirect ? $redirect : '/');
        }

        return $next($request);
    }
}
