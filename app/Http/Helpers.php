<?php

function page_title($title)
{
	return $title . ' - ' . config('app.name');
}

function jwplayer_url($key, $video = false, $videoFormat = 'm3u8')
{
	$secret = env('JWPLAYER_SECRET', '');
	$expires = round((time() + 3600) / 300) * 300;

	$path = ($video ? "manifests/" : "libraries/") . $key . ($video ? '.' . $videoFormat : ".js");
	$signature = md5($path . ":" . $expires . ":" . $secret);

	return "https://content.jwplatform.com/" . $path . "?sig=" . $signature . "&exp=" . $expires;
}