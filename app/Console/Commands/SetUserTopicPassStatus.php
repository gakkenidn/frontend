<?php

namespace App\Console\Commands;

use Mail;
use App\Accounts\User;
use App\Mail\TopicPassed;
use App\Accounts\UserTopic;
use Illuminate\Console\Command;

class SetUserTopicPassStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:pass-topics {--email= : Limit checking to this email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set the passed status for mapped user topics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userTopics = UserTopic::whereNull('passed_at')->with(['topic', 'user'])->orderBy('topic_id');
        if ($this->option('email')) {
            if (User::where('email', $this->option('email'))->count() > 0)
                $userTopics->whereHas('user', function ($query)
                {
                    $query->where('email', $this->option('email'));
                });
            else return $this->error('Provided email does not exist');
        }

        $userTopics = $userTopics->get();

        if ($userTopics->count() > 0) {
            $passingGrade = 80;
            $total = 0;
            foreach ($userTopics as $userTopic) {

                $topic = $userTopic->topic;
                $user = $userTopic->user;
                $course = \App\Moodle\Course::where('idnumber', $topic->p2kb_url)->first();
                $moodleUser = \App\Moodle\User::where('email', $user->email)->first();

                if (!$course)
                    $this->error('Invalid Course ID: ' . $topic->p2kb_url);
                elseif (!$moodleUser)
                    $this->error('Invalid User Email: ' . $user->email);
                else {

                    $grade = $course
                        ->gradeItems()->quiz()->first()
                        ->gradeGrades()->where('userid', $moodleUser->id)->first();

                    if (!$grade) {}
                    elseif (!$grade->finalgrade) {}
                    elseif ($grade->finalgrade < $passingGrade) {}
                    else {

                        $userTopic->grade = $grade->finalgrade;
                        $userTopic->passed_at = \Carbon\Carbon::createFromTimestamp($grade->timemodified, env('APP_TIMEZONE', 'Asia/Makassar'));
                        $userTopic->save();

                        Mail::queue(new TopicPassed($userTopic));
                        $this->line('Passed ' . $user->name . ' for topic ' . $topic->title . ' (' . $grade->finalgrade . ')');
                        $total++;
                    }
                }
            }

            $this->info('Updated ' . $total . ' user topics');
        } else {
            $this->info('No pending user topics');
        }
    }
}
