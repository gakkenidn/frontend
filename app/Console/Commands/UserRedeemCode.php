<?php

namespace App\Console\Commands;

use App\Accounts\AppSetting;
use App\Accounts\Subscription;
use App\Accounts\User;
use App\Accounts\UserSubscription;
use App\Accounts\UserTopic;
use App\Events\UserEnrolled;
use App\Mail\SubscriptionActive;
use App\Vault\Topic;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Mail;

class UserRedeemCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:redeem {code : The redeem code} {email : The user\'s email address}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Redeem a subscription code for user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscription = Subscription::where('redeem_code', $this->argument('code'))->first();
        if (!$subscription)
            return $this->error('No matching subscription was found by the redeem code');

        $user = User::where('email', $this->argument('email'))->first();
        if (!$user)
            return $this->error('No user found for ' . $this->argument('email'));
        if (!$user->profession || !$user->id_no)
            return $this->error('The specified user has not set professional details');

        $this->line('Redeeming code');
        $userSubscription = new UserSubscription;
        $userSubscription->user_id = $user->id;
        $userSubscription->subscription_id = $subscription->id;
        $userSubscription->starts_at = Carbon::now()->subSeconds(1);
        $userSubscription->is_trial = $subscription->is_trial;

        $expireMethod = 'add' . ucfirst($subscription->period);
        $userSubscription->expired_at = Carbon::now()->$expireMethod($subscription->duration);
        $userSubscription->save();

        $this->info('User subscribed successfully');

        $currentTopicIndex = AppSetting::find('topic.current_index_' . ($user->profession ?: 'doctor'))->value;
        $topic = Topic::where('category', $user->profession ?: 'doctor')->where('release', $currentTopicIndex)->first();
        if (UserTopic::where('user_id', $user->id)->where('topic_id', $topic->id)->count() == 0) {
            $subscribeTopic = new UserTopic;
            $subscribeTopic->topic_id = $topic->id;
            $subscribeTopic->is_accessible = true;
            $subscribeTopic->is_trial = $subscription->is_trial;
            $user->userTopics()->save($subscribeTopic);

            event(new UserEnrolled($subscribeTopic));

            $this->info('User enrolled for initial topic: ' . $topic->title);
        }

        Mail::queue(new SubscriptionActive($userSubscription));
        $this->info('User has been emailed at ' . $user->email);
    }
}
