<?php

namespace App\Console\Commands;

use App\Accounts\User;
use Illuminate\Console\Command;
use Newsletter;

class SubscribeUserToNewsletter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:subscribe-newsletter {email : The user\'s email address (type \'all\' to subscribe all users)}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscibe user to Mailchimp newsletters';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->argument('email') != 'all') {
            $user = User::where('email', $this->argument('email'))->first();
            if (!$user)
                return $this->error('User account does not exist');
            Newsletter::subscribeOrUpdate($user->email, ['FNAME' => $user->name]);
            return $this->info('User subscribed successfully');
        }

        $users = User::all();
        $total = 0;
        foreach ($users as $user) {
            Newsletter::subscribeOrUpdate($user->email, ['FNAME' => $user->name]);
            $this->line('Subscribed: ' . $user->email);
            $total++;
        }
        return $this->info($total . ' users subscribed');
    }
}
