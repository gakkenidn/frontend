<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\CheckEndingSubscription::class,
        \App\Console\Commands\CreateUser::class,
        \App\Console\Commands\EnrolUsersToMoodleCourse::class,
        \App\Console\Commands\GenerateBcryptString::class,
        \App\Console\Commands\GenerateSitemap::class,
        \App\Console\Commands\MakeReactComponent::class,
        \App\Console\Commands\SendTestEmail::class,
        \App\Console\Commands\SetUserTopicPassStatus::class,
        \App\Console\Commands\SubscribeUserToNewsletter::class,
        \App\Console\Commands\SubscribeUserToPlan::class,
        \App\Console\Commands\UserRedeemCode::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('backup:clean')->daily();
        // $schedule->command('backup:run')->dailyAt('01:00');
        $schedule->command('sitemap:generate')->daily();
        $schedule->command('user:check-ending-sub --notify')->daily();
        $schedule->command('user:pass-topics')->everyMinute();
        $schedule->command('user:enrol-monthly')
            ->monthlyOn(1, '00:00')
            ->before(function ()
            {
                $doctorIndex = \App\Accounts\AppSetting::find('topic.current_index_doctor');
                $doctorIndex->value = $doctorIndex->value + 1;
                $doctorIndex->save();

                $dentistIndex = \App\Accounts\AppSetting::find('topic.current_index_dentist');
                $dentistIndex->value = $dentistIndex->value + 1;
                $dentistIndex->save();

		$doctorTopic = \App\Vault\Topic::newRelease('doctor')->first();
		$doctorTopic->post_status = 1;
		$doctorTopic->save();

		$dentistTopic = \App\Vault\Topic::newRelease('dentist')->first();
		$dentistTopic->post_status = 1;
		$dentistTopic->save();
            });
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
