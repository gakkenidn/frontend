<?php

namespace App\Moodle;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
	protected $connection = 'mysql_moodle';
	protected $table = 'user';
	
}
