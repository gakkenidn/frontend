<?php

namespace App\Moodle;

use Illuminate\Database\Eloquent\Model;
use Ixudra\Curl\Facades\Curl;

class Moodle extends Model
{
  
  const SERVER_PATH = '/webservice/rest/server.php';

  public static function getServiceUrl()
  {
    return env('MOODLE_API_URL') . Moodle::SERVER_PATH;
  }

  public static function remoteCall()
  {
  	$url = Moodle::getServiceUrl();
  }

}
