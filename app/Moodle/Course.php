<?php

namespace App\Moodle;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

	protected $connection = 'mysql_moodle';
	protected $table = 'course';

	public function gradeItems()
	{
		return $this->hasMany('App\Moodle\GradeItem', 'courseid');
	}
}
