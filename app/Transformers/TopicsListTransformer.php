<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Accounts\UserTopic;
use League\Fractal\TransformerAbstract;

class TopicsListTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(UserTopic $userTopic)
    {
        $category = $userTopic->topic->category == 'doctor' ? "Gakken P2KB" : "Gakken P3KGB";

        return [
            'slug' => $userTopic->topic->slug,
            'image' => $userTopic->topic->featured_image,
            'title' => $userTopic->topic->title,
            'release' => Carbon::parse($userTopic->topic->publish_at)->format('M Y'),
            'category' => $category,
            'passedAt' => $userTopic->passed_at == null ? null : Carbon::parse($userTopic->passed_at)->format('j/m/Y'),
        ];
    }
}
