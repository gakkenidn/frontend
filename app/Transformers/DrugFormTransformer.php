<?php

namespace App\Transformers;

use App\Vault\DrugForm;
use League\Fractal\TransformerAbstract;

class DrugFormTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(DrugForm $form)
    {
        return [
            'slug' => $form->slug,
            'name' => $form->name
        ];
    }
}
