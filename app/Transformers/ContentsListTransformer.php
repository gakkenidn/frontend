<?php

namespace App\Transformers;

use App\Accounts\UserTopic;
use App\Vault\TopicContent;
use League\Fractal\TransformerAbstract;

class ContentsListTransformer extends TransformerAbstract
{

    private $userTopic;
    private $topic;
    private $default;

    public function __construct(UserTopic $userTopic, $topic = null, $context = 'default')
    {
        $this->userTopic = $userTopic;
        $this->topic = $topic;
        $this->context = $context;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TopicContent $topicContent)
    {
        $topic = $this->topic ?: $this->userTopic->topic;
        $content = $topicContent->content;
        $response = [];

        if (trim(strlen($content->url_jw)) > 0) {
            $type = 'video';
            $data = $content->url_jw;
            $response['url_jw'] = $content->getOriginal('url_jw');
        }
        elseif ($content->appearance == 'download') {
            $type = 'certificate';
            $data = route('topic.certificate', ['topic' => $topic->slug]);
        }
        elseif ($content->appearance == 'quiz') {
            $type = $content->appearance;
            $data =
                'https://p2kb.gakken-idn.id/moodle/auth/gakken/sso' . ($this->context == 'mobile' ? '-mobile' : '') . '.php?' .
                'redirect=' . urlencode($content->data) .
                (auth()->check() ? '&email=' . urlencode(auth()->user()->email) : '');
        }
        else {
            $type = $content->appearance;
            $data = route('topic.file', ['filename' => $content->data]);
        }

        $pendingPrerequisite = null;
        if ($topicContent->prerequisites != null) {
            foreach ($topicContent->prerequisites as $p) {
                if ($this->userTopic->viewed_contents == null) {
                    $pendingPrerequisite = TopicContent::find($p)->content->label;
                    break;
                }

                if (!in_array($p, $this->userTopic->viewed_contents)) {
                    $pendingPrerequisite = TopicContent::find($p)->content->label;
                    break;
                }
            }
        }

        return array_merge([
            'data' => $data,
            'slug' => $content->slug,
            'type' => $type,
            'label' => $content->label,
            'order' => $content->order,
            'pendingPrerequisite' => $pendingPrerequisite
        ], $response);
    }
}
