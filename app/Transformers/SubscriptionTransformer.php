<?php

namespace App\Transformers;

use App\Accounts\Subscription;
use League\Fractal\TransformerAbstract;

class SubscriptionTransformer extends TransformerAbstract
{
    protected $context;

    public function __construct($context = 'index')
    {
        $this->context = $context;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Subscription $sub)
    {
        $baseContext = [
            'slug' => $sub->name,
            'name' => $sub->label,
            'isTrial' => $sub->is_trial
        ];

        switch ($this->context) {
            case 'index':
                return array_merge($baseContext, [
                    'duration' => $sub->duration . ' ' . ($sub->duration > 1 ? str_plural($sub->period) : str_singular($sub->period))
                    ]);
        }
    }
}
