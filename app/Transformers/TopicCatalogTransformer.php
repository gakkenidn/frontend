<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Vault\Topic;
use League\Fractal\TransformerAbstract;
use App\Transformers\CatalogContentTransformer;

class TopicCatalogTransformer extends TransformerAbstract
{

    protected $context;

    public function __construct($context = 'index')
    {
        $this->context = $context;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Topic $topic)
    {
        $baseContext = [
            'slug'          => $topic->slug,
            'title'         => $topic->title,
            'release'       => Carbon::parse($topic->publish_at)->format('M Y'),
            'category'      => $topic->category,
            'featuredImage' => env('VAULT_URL', '/') . '/topic/files/' . $topic->featured_image,
            'summary'       => clean($topic->summary, ['HTML.Allowed' => '']),
            'passed'        => isset($topic->passed) ? $topic->passed : false,
            'alert'         => isset($topic->alert) ? $topic->alert : null,
            'isSubscribe'   => $topic->isSubscribe ,
            'subscriptions' => $topic->subscriptions
        ];

        switch ($this->context) {
            case 'show':
                return array_merge($baseContext, [
                    'content'   => fractal()->collection($topic->contents()->catalog()->get())->transformWith(new CatalogContentTransformer)->toArray(),
                    'lecturer'  => $topic->lecturers->first()->getNameWithTitlesAttribute()
                ]);
            default: return $baseContext;
        }
    }
}
