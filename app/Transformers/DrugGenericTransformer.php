<?php

namespace App\Transformers;

use App\Vault\DrugGeneric;
use League\Fractal\TransformerAbstract;

class DrugGenericTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(DrugGeneric $drugGeneric)
    {
        return [
            'form' => $drugGeneric->drug_form_id ? [
                'slug' =>  $drugGeneric->form->slug,
                'name' =>  $drugGeneric->form->name,
            ] : null,
            'generic' => $drugGeneric->generic_id ? [
                'slug' => $drugGeneric->generic->slug,
                'name' => $drugGeneric->generic->name,
            ] : null,
            'quantity' => $drugGeneric->quantity,
            'unit' => $drugGeneric->unit ?: null,
        ];
    }
}
