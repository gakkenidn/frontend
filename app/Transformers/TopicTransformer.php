<?php

namespace App\Transformers;

use Carbon\Carbon;
use App\Vault\Topic;
use League\Fractal\TransformerAbstract;

class TopicTransformer extends TransformerAbstract
{

    protected $context;

    public function __construct($context = 'index')
    {
        $this->context = $context;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Topic $topic)
    {
        $baseContext = [
            'slug' => $topic->slug,
            'title' => $topic->title
        ];

        return $baseContext;
    }
}
