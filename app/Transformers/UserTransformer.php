<?php

namespace App\Transformers;

use Fractal;
use Carbon\Carbon;
use App\Accounts\Role;
use App\Accounts\User;
use App\Transformers\RoleTransformer;
use League\Fractal\TransformerAbstract;
use App\Transformers\UserSubscriptionTransformer;

class UserTransformer extends TransformerAbstract
{

    protected $context;

    public function __construct($context = 'single')
    {
        $this->context = $context;
    }

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        if ($this->context == 'single') {
            $userSubscription = $user->userSubscriptions()->active()->with('subscription')->first();
            return [
                'name' => $user->name,
                'email' => $user->email,
                'avatar' => $user->avatar_url,
                'joinedAt' => $user->created_at->format('j/m/Y'),
                'profession' => $user->profession,
                'idNo' => $user->id_no,
                'currentSubscription' => $userSubscription == null ? null : Fractal::item($userSubscription)->transformWith(new UserSubscriptionTransformer)->toArray()['data']
            ];
        }
        
        if ($this->context == 'single-admin') {
            $userSubscription = $user->userSubscriptions()->active()->with('subscription')->first();
            return [
                'name' => $user->name,
                'email' => $user->email,
                'avatar' => $user->avatar_url,
                'joinedAt' => $user->created_at->format('j/m/Y'),
                'profession' => $user->profession == null ? null : ['slug' => $user->profession, 'label' => ucfirst($user->profession)],
                'nameOnCertificate' => $user->name_on_certificate,
                'role' => Fractal::item($user->role)->transformWith(new RoleTransformer)->toArray()['data'],
                'topics' => Fractal::collection($user->userTopics)->transformWith(new UserTopicTransformer('show-user'))->toArray()['data'],
                'idNo' => $user->id_no ?: '',
                'currentSubscription' => $userSubscription == null ? null : Fractal::item($userSubscription)->transformWith(new UserSubscriptionTransformer)->toArray()['data']
            ];
        }

        if ($this->context == 'index')
            return [
                'name'          => $user->name,
                'role'          => $user->role->name,
                'email'         => $user->email,
                'groups'        => $user->groups->map(function ($group)
                {
                    return $group->name;
                })->all(),
                'isProvider'    => $user->is_provider == 1,
                'isVerified'    => $user->verified_at != null,
                'registeredAt'  => Carbon::parse($user->created_at)->diffForHumans(),
            ];
    }
}
