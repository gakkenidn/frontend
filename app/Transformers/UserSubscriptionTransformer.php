<?php

namespace App\Transformers;

use App\Accounts\UserSubscription;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class UserSubscriptionTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(UserSubscription $userSubscription)
    {
        return [
            'startsAt' => Carbon::parse($userSubscription->starts_at)->format('j/m/Y'),
            'expiredAt' => Carbon::parse($userSubscription->expired_at)->format('j/m/Y'),
            'isTrial' => $userSubscription->is_trial,
            'subscription' => $userSubscription->subscription->label
        ];
    }
}
