<?php

namespace App\Transformers;

use App\WP\TermTaxonomy;
use League\Fractal\TransformerAbstract;
use App\Transformers\ArticleTransformer;

class ArticleCategoryTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TermTaxonomy $category)
    {
        return [
            'name' => $category->term->name,
            'slug' => $category->term->slug,
            'posts' => fractal()->collection($category->posts->slice(0, 5))->transformWith(new ArticleTransformer)->toArray()
        ];
    }
}
