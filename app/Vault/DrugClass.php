<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class DrugClass extends Model
{
  protected $connection = 'mysql_vault';

  public function subclasses()
  {
    return $this->hasMany('App\Vault\DrugSubclass')->orderBy('name');
  }

  public function directDrugs()
  {
    return $this->hasMany('App\Vault\Drug')
      ->whereNull('drug_subclass_id')
      ->orderBy('name');
  }

  public function drugs()
  {
    return $this->hasMany('App\Vault\Drug')
      ->orderBy('name');
  }

}
