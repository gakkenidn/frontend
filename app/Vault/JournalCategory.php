<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class JournalCategory extends Model
{

    protected $connection = 'mysql_vault';

    public function journals() {
        return $this->hasMany('App\Vault\Journal')->orderBy('title', 'desc');
    }

}
