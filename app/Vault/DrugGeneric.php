<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class DrugGeneric extends Model
{
  protected $connection = 'mysql_vault';
  protected $table = 'drug_generic';

  public function generic()
  {
  	return $this->belongsTo('App\Vault\Generic');
  }

  public function form()
  {
  	return $this->belongsTo('App\Vault\DrugForm', 'drug_form_id');
  }
}
