<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public $connection = 'mysql_vault';
}
