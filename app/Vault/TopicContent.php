<?php

namespace App\Vault;

use App\Vault\Content;
use Illuminate\Database\Eloquent\Model;

class TopicContent extends Model
{

	protected $connection = 'mysql_vault';
	protected $table = 'topic_content';

	public function content()
	{
		return $this->belongsTo('App\Vault\Content');
	}

	public function topic()
	{
		return $this->belongsTo('App\Vault\Topic');
	}

	public function getPrerequisitesAttribute($value)
	{
		if ($value == null)
			return $value;

		return json_decode($value);
	}
  
}
