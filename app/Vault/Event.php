<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public $connection = 'mysql_vault';
}
