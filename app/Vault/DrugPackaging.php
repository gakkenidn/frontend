<?php

namespace App\Vault;

use Illuminate\Database\Eloquent\Model;

class DrugPackaging extends Model
{
	protected $connection = 'mysql_vault';
	protected $table = 'drug_packaging';

	protected $hidden = [
		'created_at', 'updated_at'
	];

	public function form()
	{
		return $this->belongsTo('App\Vault\DrugForm', 'drug_form_id');
	}
}
