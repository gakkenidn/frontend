<?php

namespace App\WP;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
	public $connection = 'mysql_web';
  public $timestamps = false;
	public $primaryKey = 'term_id';

	public function termTaxonomy()
	{
		return $this->hasOne('\App\WP\TermTaxonomy', 'term_id');
	}

	public function scopeCategory($query)
	{
		return $query->where('taxonomy', 'category');
	}

	public function scopeTag($query)
	{
		return $query->where('taxonomy', 'post_tag');
	}
}
