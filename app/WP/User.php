<?php

namespace App\WP;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
  public $connection = 'mysql_web';
	public $timestamps = false;
}
