<?php

namespace App\WP;

use Illuminate\Database\Eloquent\Model;

class PopularPostsData extends Model
{

	public $connection = 'mysql_web';
    public $timestamps = false;
	public $primaryKey = 'postid';
	protected $table = 'popularpostsdata';

	public function post()
	{
		return $this->belongsTo('App\WP\Post', 'postid')->published();
	}

    public function featuredImages()
    {
    	return $this->belongsToMany('\App\WP\Post', 'postmeta', 'post_id', 'meta_value')
    		->select(['ID as id', 'guid as url', 'post_mime_type as mime_type'])
    		->where('meta_key', '_thumbnail_id');
    }

    public function categoryTaxonomies()
    {
        return $this->belongsToMany('\App\WP\TermTaxonomy', 'term_relationships', 'object_id', 'term_taxonomy_id')
            ->whereHas('category');
    }

    public function subheading()
    {
    	return $this->hasOne('\App\WP\PostMeta', 'post_id')
    		->select(['post_id', 'meta_value as content'])
    		->where('meta_key', 'subheading');
    }
}
