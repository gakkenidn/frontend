<?php

namespace App\BNI;

use Illuminate\Database\Eloquent\Model;

class ECollection extends Model
{

	/**
	* Client key
	* @static
	*/
	public static $clientKey;

	/**
	* Secret key
	* @static
	*/
	public static $secretKey;

	/**
	* true for production
	* false for sandbox mode
	* @static
	*/
	public static $isProduction;

    const TIME_DIFF_LIMIT = 300;
    const SANDBOX_BASE_URL = 'https://apibeta.bni-ecollection.com/';
    const PRODUCTION_BASE_URL = 'https://api.bni-ecollection.com/';

    public static function getBaseUrl() {
    	return ECollection::$isProduction ?
    		ECollection::PRODUCTION_BASE_URL : ECollection::SANDBOX_BASE_URL;
    }
    
    public static function remoteCall($post = '') {
    	$header[] = 'Content-Type: application/json';
    	$header[] = "Accept-Encoding: gzip, deflate";
    	$header[] = "Cache-Control: max-age=0";
    	$header[] = "Connection: keep-alive";
    	$header[] = "Accept-Language: en-US,en;q=0.8,id;q=0.6";

    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, ECollection::getBaseUrl());
    	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    	curl_setopt($ch, CURLOPT_HEADER, false);
    	curl_setopt($ch, CURLOPT_VERBOSE, false);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    	curl_setopt($ch, CURLOPT_ENCODING, true);
    	curl_setopt($ch, CURLOPT_AUTOREFERER, true);
    	curl_setopt($ch, CURLOPT_MAXREDIRS, 5);

    	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.120 Safari/537.36");

    	if ($post)
    	{
    		curl_setopt($ch, CURLOPT_POST, true);
    		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    	}

    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    	$rs = curl_exec($ch);

    	if(empty($rs)){
    		// var_dump($rs, curl_error($ch));
    		curl_close($ch);
    		return false;
    	}
    	curl_close($ch);
    	return $rs;
    }

	public static function hashData(array $data) {
		return self::doubleEncrypt(
			strrev(time()) . '.' . json_encode($data), 
			ECollection::$clientKey, 
			ECollection::$secretKey
			);
	}

	public static function parseData($hashedString) {
		$parsed_string = self::doubleDecrypt($hashedString, ECollection::$clientKey, ECollection::$secretKey);
		list($timestamp, $data) = array_pad(explode('.', $parsed_string, 2), 2, null);
		if (self::tsDiff(strrev($timestamp)) === true) {
			return json_decode($data, true);
		}
		return null;
	}

	private static function tsDiff($ts) {
		return abs($ts - time()) <= self::TIME_DIFF_LIMIT;
	}

	private static function doubleEncrypt($string) {
		$result = '';
		$result = self::encrypt($string, ECollection::$clientKey);
		$result = self::encrypt($result, ECollection::$secretKey);
		return strtr(rtrim(base64_encode($result), '='), '+/', '-_');
	}

	private static function encrypt($string, $key) {
		$result = '';
		$strls = strlen($string);
		$strlk = strlen($key);
		for($i = 0; $i < $strls; $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % $strlk) - 1, 1);
			$char = chr((ord($char) + ord($keychar)) % 128);
			$result .= $char;
		}
		return $result;
	}

	private static function doubleDecrypt($string) {
		$result = base64_decode(strtr(str_pad($string, ceil(strlen($string) / 4) * 4, '=', STR_PAD_RIGHT), '-_', '+/'));
		$result = self::decrypt($result, ECollection::$clientKey);
		$result = self::decrypt($result, ECollection::$secretKey);
		return $result;
	}

	private static function decrypt($string, $key) {
		$result = '';
		$strls = strlen($string);
		$strlk = strlen($key);
		for($i = 0; $i < $strls; $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % $strlk) - 1, 1);
			$char = chr(((ord($char) - ord($keychar)) + 256) % 128);
			$result .= $char;
		}
		return $result;
	}
}
