<?php

namespace App\Accounts;

use Illuminate\Database\Eloquent\Model;

class PageView extends Model
{
    public $connection = 'mysql_accounts';
    const CREATED_AT = 'accessed_at';
    const UPDATED_AT = null;
}
