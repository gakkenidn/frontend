<?php

namespace App\Accounts;

use Illuminate\Database\Eloquent\Model;

class AppSetting extends Model
{
    public $connection = 'mysql_accounts';
    public $primaryKey = 'key';
    public $incrementing = false;
}
