<?php

namespace App\Accounts;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{
    protected $connection = 'mysql_accounts';

    public function users()
    {
        return $this->morphedByMany('App\Accounts\User', 'model', 'model_has_apps', 'app_id', 'model_id');
    }
}
