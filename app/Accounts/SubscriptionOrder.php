<?php

namespace App\Accounts;

use App\Events\SubscriptionOrderSaved;
use Illuminate\Database\Eloquent\Model;

class SubscriptionOrder extends Model
{
    public $connection = 'mysql_accounts';

    protected $appends = [
        'payment_method_label'
    ];

    protected $events = [
        'created' => SubscriptionOrderSaved::class
    ];

    public function subscription()
    {
    	return $this->belongsTo('App\Accounts\Subscription');
    }

    public function userTopic()
    {
    	return $this->belongsTo('App\Accounts\UserTopic');
    }

    public function user()
    {
    	return $this->belongsTo('App\Accounts\User');
    }

    public function getPaymentMethodLabelAttribute()
    {
        $label = false;
        switch ($this->method) {
            case 'mt-cc':
                $label = 'Kartu Kredit';
                break;
            case 'mt-ccinstallment':
                $label = 'Cicilan Kartu Kredit';
                break;
            case 'banktransfer':
                $label = 'Transfer Bank';
                break;
        }

        return $label;
    }

    public function intoSubscription()
    {
    	if ($this->subscribed_at)
    		return $this;

    	$subscription = $this->subscription;
    	$user = $this->user;

    	$userSubscription = new \App\Accounts\UserSubscription;
    	$userSubscription->user_id = $user->id;
    	$userSubscription->subscription_id = $subscription->id;
    	$userSubscription->starts_at = \Carbon\Carbon::now();

    	$expireMethod = 'add' . ucfirst($subscription->period);
    	$userSubscription->expired_at = \Carbon\Carbon::now()->$expireMethod($subscription->duration);
    	$userSubscription->save();

    	$currentTopicIndex = \App\Accounts\AppSetting::find('topic.current_index_' . ($user->profession ?: 'doctor'))->value;
    	$subscribeTopic = new \App\Accounts\UserTopic;
    	$subscribeTopic->topic_id = \App\Vault\Topic::where('category', $user->profession ?: 'doctor')->where('release', $currentTopicIndex)->first()->id;
    	$subscribeTopic->is_accessible = true;
    	if ($subscribeTopic) {
    		$user->userTopics()->save($subscribeTopic);
    	}

    	$this->subscribed_at = \Carbon\Carbon::now();
    	$this->save();
    	return $userSubscription;
    }

    public static function generateOrderNo()
    {
        return 'GK-' .
            \Carbon\Carbon::now()->format('ymd') .
            str_pad(SubscriptionOrder::whereDate('created_at', \Carbon\Carbon::now()->format('y-m-d'))->count() + 1, 4, '0', STR_PAD_LEFT);
    }
}
