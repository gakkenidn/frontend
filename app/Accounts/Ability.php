<?php

namespace App\Accounts;

use Illuminate\Database\Eloquent\Model;

class Ability extends Model
{
  
  protected $connection = 'mysql_accounts';

  public function getAppAttribute()
  {
  	$namespace = explode('.', $this->name);
  	return title_case($namespace[0]);
  }

  public function getModuleAttribute()
  {
  	$namespace = explode('.', $this->name);
  	return title_case(str_replace('_', ' ', $namespace[1]));
  }

  public function getActionAttribute()
  {
  	$namespace = explode('.', $this->name);
  	return title_case(str_replace('_', ' ', $namespace[2]));
  }

}
