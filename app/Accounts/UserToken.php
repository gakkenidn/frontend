<?php

namespace App\Accounts;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class UserToken extends Model
{
    protected $dates = ['expires_at'];

    public $timestamps = false;
    public $connection = 'mysql_accounts';

    public function user()
    {
        return $this->belongsTo('App\Accounts\User');
    }

    public function scopeActive($query)
    {
        return $query->where('expires_at', '>', Carbon::now());
    }
}
