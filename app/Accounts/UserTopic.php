<?php

namespace App\Accounts;

use Illuminate\Database\Eloquent\Model;

class UserTopic extends Model
{
    protected $connection = 'mysql_accounts';
    public $timestamps = false;

    public function user()
    {
    	return $this->belongsTo('App\Accounts\User');
    }

    public function topic()
    {
    	return $this->belongsTo('App\Vault\Topic');
    }

    public function getViewedContentsAttribute($value)
    {
        if ($value == null) return $value;
        return json_decode($value);
    }

    public function generateCertificateNumber()
    {
        $certificateNo = 'GK-' . ($this->topic->category == 'doctor' ? '01' : '02');
        $certificateNo .= str_pad($this->id, 6, '0', STR_PAD_LEFT);
        $certificateNo .= '-' . \Carbon\Carbon::parse($this->passed_at)->format('Y');

        return $certificateNo;
    }

}
