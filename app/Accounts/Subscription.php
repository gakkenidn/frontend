<?php

namespace App\Accounts;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
  public $connection = 'mysql_accounts';
  protected $hidden = [
  	'id', 'is_custom', 'duration', 'period', 'created_at', 'updated_at'
  ];

  public function scopeNotCustom($query)
  {
  	$query->where('is_custom', false);
  }

  public function topics()
  {
    return $this->hasMany(
      'App\Vault\TopicSubscription'
    );
  }
}
