<?php

namespace App\Accounts;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ApiToken extends Model
{

	protected $dates = ['expires_at'];

  public $connection = 'mysql_accounts';

  public function user()
  {
  	return $this->belongsTo('App\Accounts\User');
  }

  public function scopeActive($query)
  {
  	return $query->where('expires_at', '>', Carbon::now());
  }
}
