<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Accounts\UserSubscription;

class SubscriptionActive extends Mailable
{
    use Queueable, SerializesModels;

    public $userSubscription;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(UserSubscription $userSubscription)
    {
        $this->userSubscription = $userSubscription;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('support@gakken-idn.co.id', 'Gakken Indonesia')
            ->to($this->userSubscription->user->email)
            ->subject('[Gakken] Selamat Menikmati Layanan Kami')
            ->markdown('emails.subscription.active');
    }
}
