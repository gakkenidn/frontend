<?php

namespace App\Mail;

use App\Accounts\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserVerified extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->to($this->user->email)
            ->from('support@gakken-idn.co.id', 'Gakken Indonesia')
            ->subject('[Gakken] Akun Anda telah Terverifikasi')
            ->markdown('emails.user.verified');
    }
}
