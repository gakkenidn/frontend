<?php

namespace App\Mail;

use App\Accounts\SubscriptionOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SubscriptionOrdered extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SubscriptionOrder $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from('support@gakken-idn.co.id', 'Gakken Indonesia')
            ->to($this->order->user->email)
            ->subject('[Gakken] Pembelian Paket Berlangganan')
            ->markdown('emails.subscription.ordered');
    }
}
