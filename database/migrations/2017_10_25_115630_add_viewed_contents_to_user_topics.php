<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewedContentsToUserTopics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_accounts')->table('user_topics', function (Blueprint $table) {
            $table->string('viewed_contents')->nullable()->after('is_trial');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_accounts')->table('user_topics', function (Blueprint $table) {
            $table->dropColumn('viewed_contents');
        });
    }
}
