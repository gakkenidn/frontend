<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function ()
{

	Route::resource('profile', 'API\ProfileController', ['only' => ['index', 'store']]);
	Route::post('topics/redeem', 'API\TopicController@redeemCertificate');
	Route::resource('topics', 'API\TopicController', ['only' => ['index', 'show']]);
	Route::get('journals/read/{slug}', 'API\JournalController@getTicketedURL');
	Route::get('topic-catalog-user', 'API\TopicCatalogController@index');

	Route::post('logout', function() {
	  auth()->user()->token()->revoke();
	  return "success";
	});

});
Route::resource('topic-catalog', 'API\TopicCatalogController', ['only' => ['index', 'show']]);
Route::resource('articles', 'API\ArticleController', ['only' => ['index', 'show']]);
Route::resource('article-categories', 'API\ArticleCategoriesController', ['only' => ['index']]);
Route::resource('journals/categories', 'API\JournalCategoriesController', ['only' => ['index', 'show']]);
Route::resource('journals', 'API\JournalController', ['only' => ['index', 'show']]);
Route::resource('drugs/classes', 'API\DrugClassesController', ['only' => ['index']]);
Route::resource('drugs', 'API\DrugController', ['only' => ['index', 'show']]);
Route::post('user/register', 'API\ProfileController@register');
Route::get('video/{videoUrl}', function ($videoUrl)
{
	$controller = new \App\Http\Controllers\AppController;
	return $controller->getJWPlayerScript($videoUrl);
});

Route::get('sponsor', 'SponsorController@index');
