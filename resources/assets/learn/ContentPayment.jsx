import React, { Component } from 'react';
import Axios from 'axios';
import $ from 'jquery';

class ContentPayment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: false,
    };
  }

  pay() {
    this.setState({
			loading: true,
			errors: {}
    });
    Axios.post(window.SubscribeApp.snapTokenUrl,
			{
				topic: this.props.topic.slug,
        method: 'mt-cc',
        type: 'certificate'
			}
		).then((response) => {
      this.setState({ loading: false });

			function changeResult(type, data) {
				$("#payment-form #result-type").val(type);
				$("#payment-form #result-data").val(JSON.stringify(data));
			}
      $("#payment-form #payment-method").val('midtrans');
      console.log('paying..');
      snap.pay(response.data, {
        onSuccess: function(result){
          changeResult('success', result);
          $("#payment-form").submit();
        },
        onPending: function(result){
          changeResult('pending', result);
          $("#payment-form").submit();
        },
        onError: function(result){
          changeResult('error', result);
          $("#payment-form").submit();
        }
      });
		}).catch((error) => {
			this.setState({ loading: false });
			if (error.response) {
				this.setState({ errors: error.response.data });
			}
		});
  }

  redeem() {
    this.setState({ loading: true, errors: false });
    Axios.post(window.App.url.base + '/pay/redeem', { topic: this.props.topic.slug })
      .then(response => {
        this.props.onSuccess(response.data);
        this.setState({ loading: false })
      })
      .catch(() => this.setState({ error: true, loading: false }));
  }

  render() {
    return (
      <div className="container pt-5">
        <div className="row justify-content-center">
          <div className="col-md-8">
            <div className="border bg-light px-4 py-5">
              <div>Selamat, Anda telah melulusi topik</div>
              <h2 className="mb-4">{ this.props.topic.title }</h2>

              <div className="d-flex">
                <div className="mr-4">
                  <div className="text-muted">SKP</div>
                  <div className="h5">2</div>
                </div>
                <div>
                  <div className="text-muted">Sertifikasi</div>
                  <div className="h5">{ this.props.topic.category === 'doctor' ? 'IDI' : 'PDGI' } Wilayah Sulselbar</div>
                </div>
              </div>

              <hr />

              <div className="d-flex">
                <p className="mr-3">
                  {
                    window.App.remainingQuota ?
                    'Anda masih memiliki sisa kuota untuk mendapatkan sertifikat sebanyak ' + window.App.remainingQuota + ' kali.' :
                    'Dapatkan sertifikat kompetensi untuk topik ini seharga Rp. 200.000,- melalui jalur pembayaran favorit Anda.'
                  }
                </p>
                <button type="button" onClick={ () => window.App.remainingQuota ? this.redeem() : this.pay() } className="btn btn-lg btn-primary" disabled={ this.state.loading }>Dapatkan sertifikat</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ContentPayment;