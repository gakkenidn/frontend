import React, { Component } from 'react';

export default class ContentImage extends Component {

	componentDidMount() {
		return this.props.onComplete(this.props.content);
	}

	render() {
		return (
			<div className="container my-4">
				<img src={ this.props.url } className="w-100" />
			</div>
			);
	}

}
