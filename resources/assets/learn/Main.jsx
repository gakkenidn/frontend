import React, { Component } from 'react';
import Sidebar from './Sidebar';
import ContentOverview from './ContentOverview';
import ContentView from './ContentView';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      learnt: [],
      topic: window.App.topic,
    };
  }

  activate({ page }) {
    this.setState({ active: page });
  }

  complete() {
    if (!this.state.learnt.filter(content => content === this.state.active - 1).length) {
      let learnt = this.state.learnt;
      learnt.push(this.state.active);
      this.setState({ learnt });
    }
  }

  render() {
    return (
      <div>
        <div className="nav navbar-nav bg-dark navbar-dark">
          <div className="px-4 py-4" style={{ width: 300 }}>
            <a href={ window.App.url.base + '/p2kb/' + this.state.topic.slug } className="no-underline">
              <div className="text-light text-truncate h6 text-uppercase m-0"><i className="fa fa-chevron-left mr-3" /> { this.state.topic.title }</div>
            </a>
          </div>
        </div>
        <div className="sidebar-view">
          <Sidebar { ...this.state } onChange={ page => this.activate({ page }) } />

          <div className="main-container">
            { !this.state.active && <ContentOverview { ...this.state.topic } /> }
            {
              this.state.active &&
              (
                <ContentView
                  { ...this.state.topic.contents[this.state.active - 1] }
                  topic={ this.state.topic }
                  onComplete={ this.complete.bind(this) }
                  onPaymentSuccess={ topic => this.setState({ topic }) } />
              )
            }
          </div>
        </div>
      </div>
    );
  }
}

export default Main;