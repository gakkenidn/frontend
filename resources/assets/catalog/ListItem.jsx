import React from 'react';

const ListItem = props => (

  <div className="col-lg-4 col-md-6 mb-5" title={props.title}>
    <a href={window.CatalogApp.baseUrl + '/p2kb/' + props.slug} style={{ position: 'relative', display: 'block' }}>
      <img src={props.featuredImage} className="w-100 mb-3 rounded" />
      {
        props.passed &&
        (
          <div style={{ position: 'absolute', top: 10, right: -5, display: 'inline-block', backgroundColor: "#3CA55C", color: '#fff', padding: '3px 10px', borderTopRightRadius: 5, boxShadow: '0 2px 5px rgba(0,0,0,0.2)' }}>
            Telah dilulusi
          </div>
        )
      }
    </a>
    <div className="mb-3">
      <a href={window.CatalogApp.baseUrl + '/p2kb/' + props.slug}>
        <div className="h4 text-truncate text-dark">{props.title}</div>
      </a>
      <p style={{ lineHeight: '1.4em', height: '4.2em', overflow: 'hidden', textOverflow: 'ellipsis' }}>{props.summary}</p>
    </div>
    <div>
      <div className="text-muted">2 SKP | {props.category === 'doctor' ? 'IDI' : 'PDGI'} Wilayah Sulselbar</div>
    </div>
  </div>

);

export default ListItem;