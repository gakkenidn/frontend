import React from 'react';
import { render } from 'react-dom';
import List from './List';

const CatalogApp = () => (
  <div>
    <List />
  </div>
);

render(<CatalogApp />, document.getElementById('catalog-app'));