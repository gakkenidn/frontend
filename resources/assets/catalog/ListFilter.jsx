import React from 'react';

const ListFilter = props => (
  <div className="d-none d-md-block d-lg-flex justify-content-between align-items-center mb-5">
    <div className="d-none d-lg-block">
      <i className="fa fa-filter h4 mr-3" />

      <a href="#" onClick={ e => { e.preventDefault(); props.filter({ category: false })}} className={"h4 mr-3" + (!props.filterCategory ? ' text-primary' : '')}>Semua Topik</a>
      <a href="#" onClick={ e => { e.preventDefault(); props.filter({ category: 'doctor' })}} className={"h4 mr-3" + (props.filterCategory === 'doctor' ? ' text-primary' : '')}>Dokter</a>
      <a href="#" onClick={ e => { e.preventDefault(); props.filter({ category: 'dentist' })}} className={"h4" + (props.filterCategory === 'dentist' ? ' text-primary' : '')}>Dokter Gigi</a>
    </div>
    <div>
      <input type="text" className="form-control form-control-lg" placeholder="Cari topik.." onChange={ e => props.filter({ term: e.target.value })} value={ props.filterTerm } />
    </div>
  </div>
);

export default ListFilter;