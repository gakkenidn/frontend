import React from 'react';

const List = props => (
  <div className="row">
    {
      props.drugs.map(drug => (
        <div className="col-md-4 py-3 mb-4" key={ drug.slug }>
          <div><a href={ window.App.baseUrl + '/drugs/' + drug.slug } className="h6 text-dark">{ drug.name }</a></div>
          <div className="text-muted small mb-1 text-truncate">{ drug.drugSubclass ? drug.drugSubclass + ' / ' : '' }{ drug.manufacturer }</div>
          <div style={{ lineHeight: '1.3em', height: '3.9em', overflow: 'hidden' }}>
            { drug.indication }
          </div>
        </div>
      ))
    }
  </div>
);

export default List;