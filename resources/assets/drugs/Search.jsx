import React, { Component } from 'react';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      term: ''
    };
  }
  render() {
    return (
      <div className="form-inline mb-5 d-flex justify-content-end">
        <input
          type="text"
          className="form-control form-control-lg"
          placeholder="Cari obat.."
          value={ this.state.term }
          onChange={ e => this.setState({ term: e.target.value })}
          onKeyPress={ e => { if (e.which === 13) this.props.onSearch(this.state.term) }} />
      </div>
    );
  }
}

export default Search;