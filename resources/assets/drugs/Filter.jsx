import React from 'react';

const Filter = props => (
  <div className="d-flex justify-content-start">
    <div className="mr-3"><i className="fa fa-filter fa-2x text-muted" /></div>
    <div className="pt-1">
      <h5><a href="#" onClick={ e => { e.preventDefault(); props.onChange(false) }} className={ props.active ? 'text-dark' : 'text-primary' }>Semua Kategori</a></h5>
      {
        window.App.classes.map(drugClass => (
          <div key={ drugClass.slug } className="py-2 border-top">
            <a
              href="#"
              onClick={ e => { e.preventDefault(); props.onChange(drugClass.slug) }}
              className={ props.active === drugClass.slug ? 'text-primary' : 'text-dark' }>
              { drugClass.name }
            </a>
          </div>
        ))
      }
    </div>
  </div>
);

export default Filter;