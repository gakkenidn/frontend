import React from 'react'

import { FormGroup, Input, FormFeedback, Label, Col } from 'reactstrap'

class BillingDetails extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			phone: '',
			address: '',
			city: '',
			postalCode: ''
		};
	}

	handlePhoneChange(e) {
		this.setState({phone: e.target.value});
		return this.props.onChange(this.state);
	}

	handleAddressChange(e) {
		this.setState({address: e.target.value});
		return this.props.onChange(this.state);
	}

	handleCityChange(e) {
		this.setState({city: e.target.value});
		return this.props.onChange(this.state);
	}

	handlePostalCodeChange(e) {
		this.setState({postalCode: e.target.value});
		return this.props.onChange(this.state);
	}

	render() {
		return (
			<div className="mb-5">
				<FormGroup row>
					<Label sm="4">Email</Label>
					<Col sm="8">
						<p className="form-control-static">{ window.SubscribeApp.userEmail }</p>
					</Col>
				</FormGroup>
				<FormGroup row>
					<Label sm="4">Nama Lengkap</Label>
					<Col sm="8">
						<p className="form-control-static">{ window.SubscribeApp.userName }</p>
					</Col>
				</FormGroup>
				<FormGroup row color={ this.props.errors.phone ? 'danger' : ''}>
					<Label sm="4">No. Telepon</Label>
					<Col sm="8">
						<Input className="form-control-minimal" onChange={ this.handlePhoneChange.bind(this) } value={ this.state.phone } />
						{
							this.props.errors.phone ? 
							<FormFeedback className="small">{ this.props.errors.phone }</FormFeedback> : ''
						}
					</Col>
				</FormGroup>
				{
					this.props.method != 'banktransfer' ?
					(
						<div>
							<FormGroup row color={ this.props.errors.address ? 'danger' : ''}>
								<Label sm="4">Alamat</Label>
								<Col sm="8">
									<Input className="form-control-minimal" onChange={ this.handleAddressChange.bind(this) } value={ this.state.address } />
									{
										this.props.errors.address ? 
										<FormFeedback className="small">{ this.props.errors.address }</FormFeedback> : ''
									}
								</Col>
							</FormGroup>
							<FormGroup row color={ this.props.errors.city ? 'danger' : ''}>
								<Label sm="4">Kota</Label>
								<Col sm="8">
									<Input className="form-control-minimal" onChange={ this.handleCityChange.bind(this) } value={ this.state.city } />
									{
										this.props.errors.city ? 
										<FormFeedback className="small">{ this.props.errors.city }</FormFeedback> : ''
									}
								</Col>
							</FormGroup>
							<FormGroup row color={ this.props.errors.postalCode ? 'danger' : ''}>
								<Label sm="4">Kode Pos</Label>
								<Col sm="8">
									<Input className="form-control-minimal" onChange={ this.handlePostalCodeChange.bind(this) } value={ this.state.postalCode } />
									{
										this.props.errors.postalCode ? 
										<FormFeedback className="small">{ this.props.errors.postalCode }</FormFeedback> : ''
									}
								</Col>
							</FormGroup>
							<FormGroup row>
								<Label sm="4">Negara</Label>
								<Col sm="8">
									<p className="form-control-static">Indonesia</p>
								</Col>
							</FormGroup>
						</div>
						) : ''
				}	
			</div>
			);
	}

}

export default BillingDetails