import React from 'react'

import Icon from '../../component/Icon'

class PaymentOption extends React.Component {

	handleClick(e) {
		e.preventDefault();
		this.link.blur();
		return this.props.onSelect(this.props.option.id);
	}

	render() {
		return (
			<a 
				href="#" 
				className={ 'list-group-item h6' + (this.props.selected ? ' text-primary': ' list-group-item-action') } 
				onClick={ this.handleClick.bind(this) } 
				ref={(link) => { this.link = link; }}>
				<Icon fixed name={ this.props.selected ? 'check-square-o' : 'square-o' } />
				<span className="ml-2">{ this.props.option.label }</span>
				{
					this.props.selected ?
						(
							<div className="small text-muted mt-2">
								{ this.props.option.description }
							</div>
							) : ''
				}
			</a>
			);
	}

};

export default PaymentOption