import React from 'react'

import PaymentOption from './PaymentOption'

const PaymentOptions = (props) => {

	return (
		<div>
			<div className="list-group mb-4">
				{
					props.options.map((option) => {
						return <PaymentOption 
							key={ option.id } 
							option={ option } 
							selected={ props.selection == option.id } 
							onSelect={ props.onSelect } />
					})
				}
			</div>
		</div>
		);

};

export default PaymentOptions