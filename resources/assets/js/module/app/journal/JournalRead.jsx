import React from 'react'

const JournalRead = (props) => {
	
	return (
		<iframe src={ props.url } width="100%" height="100%" style={{ border: 'none', marginBottom: '-10px' }} />
		);

}

export default JournalRead