import React from 'react'
import { Nav, NavItem, NavLink } from 'reactstrap'

const DrugSingleNav = (props) => {

	return (
		<Nav pills className="mb-5 text-uppercase">
			<NavItem>
				<NavLink href="#" onClick={ (e) => props.onChange(e) } data-tab="overview" active={ props.tab == 'overview' }>Overview</NavLink>
			</NavItem>
			<NavItem>
				<NavLink href="#" onClick={ (e) => props.onChange(e) } data-tab="comp" active={ props.tab == 'comp' }>Komposisi</NavLink>
			</NavItem>
			<NavItem>
				<NavLink href="#" onClick={ (e) => props.onChange(e) } data-tab="dosage" active={ props.tab == 'dosage' }>Dosis</NavLink>
			</NavItem>
			<NavItem>
				<NavLink href="#" onClick={ (e) => props.onChange(e) } data-tab="pack" active={ props.tab == 'pack' }>Kemasan/Harga</NavLink>
			</NavItem>
			<NavItem>
				<NavLink href="#" onClick={ (e) => props.onChange(e) } data-tab="related" active={ props.tab == 'related' }>Obat Terkait</NavLink>
			</NavItem>
		</Nav>
		);

}

export default DrugSingleNav