import React from 'react'

const DrugComposition = (props) => {

	let form = null;

	return (
		<div>
		{
			props.generics.map((generic, i) => {
				let skipForm = false;
				if (i > 0) {
					skipForm = generic.form == null ? form == null : generic.form.slug == form;
				}
				form = generic.form == null ? null : generic.form.slug;

				return (
					<div key={ (generic.form == null ? '0' : generic.form.slug) + '-' + generic.generic.slug } className={ "mb-3 row" + (!skipForm && i > 0 ? ' pt-3' : '') }>
						<div className="col-lg-2 col-md-4">
							{
								!skipForm ?
									<span className="h6 text-muted text-uppercase">{ generic.form == null ? 'Semua Sediaan' : generic.form.name }</span> : null
							}
						</div>
						<div className="col-lg-10 col-md-8">
							<span className="h6 mr-3">
								{ generic.generic.name }
							</span>
							{
								generic.quantity != null && generic.quantity != 0 ?
									<span>{ generic.quantity + ' ' + generic.unit }</span> : null
							}
						</div>
					</div>
					);
			})
		}
		</div>
		);

}

export default DrugComposition