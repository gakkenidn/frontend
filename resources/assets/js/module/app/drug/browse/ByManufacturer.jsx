import React from 'react'
import Axios from 'axios'
import { Card, CardBlock, CardImg, CardText, Nav, NavItem, NavLink } from 'reactstrap';

import Loading from '../../../../component/Loading'

class ByManufacturer extends React.Component {

	constructor(props) {
		super(props);

		this.cancelRequest = false;
		this.state = {
			alphabetSelection: 'a',
			availableAlphabet: [],
			loading: true,
			manufacturers: []
		};
	}

	componentDidMount() {
		Axios.get(window.App.url.rest + '/drugs/manufacturers', {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; })
		}).then((response) => {
			let alphabet = [];
			response.data.forEach((man) => {
				let hasAlpha = false;
				for (var i = 0; i < alphabet.length; i++) {
					if (man.name.substr(0, 1).toLowerCase() == alphabet[i])
						hasAlpha = true;
				}
				if (!hasAlpha) alphabet.push(man.name.substr(0, 1).toLowerCase());
			});
			this.setState({ 
				availableAlphabet: alphabet,
				loading: false,
				manufacturers: response.data
			});
		}).catch((error) => {
			this.setState({ loading: false });
		});
	}

	componentWillUnmount() {
		if (this.cancelRequest != false)
			this.cancelRequest();
	}

	handleAlphabetChange(alpha) {
		if (this.state.alphabetSelection != alpha)
			this.setState({ alphabetSelection: alpha });
	}

	render() {
		return (
			<div>
				{
					this.state.loading ?
						<Loading /> : 
						(
							<div>
								<Nav pills className="mb-3">
									{ 
										this.state.availableAlphabet.map((alphabet) => {
											return (
												<NavItem key={ alphabet }>
													<NavLink href="#" active={ this.state.alphabetSelection == alphabet } onClick={() => this.handleAlphabetChange(alphabet)} className="small">
														{ alphabet.toUpperCase() }
													</NavLink>
												</NavItem>
												);
										}) 
									}
								</Nav>
								<div className="row no-gutters">
									{
										this.state.manufacturers.map((man) => {
											if (man.name.substr(0, 1).toLowerCase() == this.state.alphabetSelection)
												return (
													<div key={ man.slug } className="col-2 pr-3">
														<Card className="mb-3" style={{ minHeight: 70 }}>
															{
																man.logo_image != null ?
																	(
																		<CardImg top width="100%" src={ 'https://vault.gakken-idn.id/drug/manufacturer/logo/' + man.slug } />
																		) : (
																		<CardBlock>
																			<CardText className="ellipsis">
																				{ man.name }
																			</CardText>
																		</CardBlock>
																	)
															}
														</Card>
													</div>
													);

											return null;
										})
									}
								</div>
							</div>
							)
				}
			</div>
			);
	}

}

export default ByManufacturer