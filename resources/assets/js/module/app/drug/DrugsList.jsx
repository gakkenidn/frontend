import React from 'react'
import { NavLink } from 'react-router-dom'
import { ListGroup } from 'reactstrap'

import Loading from '../../../component/Loading'

class DrugsList extends React.Component {

	constructor(props) {
		super(props);
	}

	handleListScrolled(e) {
		if ((100 * e.target.scrollTop / (e.target.scrollHeight - e.target.clientHeight)) > 80)
			return this.props.onLoadMore();
	}

	render() {
		return (
			<div className="master-list" onScroll={ this.handleListScrolled.bind(this) }>
				<ListGroup className="drugs-list">
					{
						this.props.drugs.map((drug) => {
							return (
								<NavLink key={ drug.slug } to={ '/drugs/' + drug.slug } className="list-group-item d-flex flex-column flex-nowrap align-items-start">
									<div className="h6 mb-1">{ drug.name } <span className="small text-muted">({ drug.manufacturer })</span></div>
									<div className="small ellipsis">{ drug.indication }</div>
								</NavLink>
								);
						})
					}
				</ListGroup>
				{
					this.props.loading ?
						<div className="my-4"><Loading /></div> : ''
				}
			</div>
			);

	}

}

export default DrugsList