import React from 'react'
import { Link } from 'react-router-dom'

import Headerbar from '../../../component/Headerbar'
import Icon from '../../../component/Icon'

const TopicHeaderBar = (props) => {

	if (props.topic.title == undefined)
		return null;

	let segments = props.location.pathname.split('/');
	let content = {
		index: -1,
		label: 'Overview Topik'
	};
	if (
		props.topic.contents != undefined
		&& segments[4] != undefined
		) {
		for (var i = props.topic.contents.length - 1; i >= 0; i--) {
			if (props.topic.contents[i].slug == segments[4])
				content = {
					index: i,
					label: props.topic.contents[i].label
				};
		}
	}


	return (
		<Headerbar>
			<div>
				{
					content.index > -1 ?
						(
							<Link to={ 
								'/topics/' + props.topic.slug + 
								(content.index > 0 ? '/content/' + props.topic.contents[content.index - 1].slug : '') }>
								<Icon name="angle-left" size="2" />
							</Link>
							) : ''
				}
			</div>
			<div className="title mx-5">
				<span className="text-muted">{ props.topic.title }</span> <Icon name="angle-right" /> { content.label }
			</div>
			<div>
				{
					props.topic.contents[content.index + 1] != undefined ?
						(
							<Link to={ '/topics/' + props.topic.slug + '/content/' + props.topic.contents[content.index + 1].slug }>
								<Icon name="angle-right" size="2" />
							</Link>
							) : ''
				}
			</div>
		</Headerbar>
		);

}

export default TopicHeaderBar;