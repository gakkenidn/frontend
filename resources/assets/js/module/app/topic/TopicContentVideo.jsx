import React from 'react'
import Axios from 'axios'
import ReactJWPlayer from 'react-jw-player'

import Loading from '../../../component/Loading'

class TopicContentVideo extends React.Component {

	constructor(props) {
		super(props);
		this.cancelRequest = false;
		this.state = {
			loading: true,
			playerKey: '',
			scriptUrl: ''
		};
	}

	componentDidMount() {
		Axios.get(window.App.url.rest + '/jwplayer-script', {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; })
		}).then((response) => {
			this.cancelRequest = false;
			this.setState({ 
				loading: false,
				playerKey: response.data.playerKey,
				scriptUrl: response.data.url
			});
		}).catch((error) => { this.cancelRequest = false; });
	}

	componentWillUnmount() {
		if (this.cancelRequest != false)
			this.cancelRequest();
	}

	render() {
		return (
			<div className="container my-4">
				{
					this.state.loading ?
						<Loading /> :
						<ReactJWPlayer 
							file={ this.props.videoUrl }
							playerId={ this.state.playerKey } 
							playerScript={ this.state.scriptUrl }
							onNinetyFivePercent={ () => this.props.onComplete(this.props.content) }
							image={ this.props.videoUrl.replace('manifests', 'thumbs').replace('.m3u8', '-720.jpg') } />
				}
			</div>
			);
	}

}

export default TopicContentVideo