import React from 'react'
import Axios from 'axios'
import { Link } from 'react-router-dom'
import { Card, CardBlock } from 'reactstrap'

import Icon from '../../../component/Icon'

class TopicSingleContents extends React.Component {

	constructor(props) {
		super(props);

		this.cancelRequest = false;
		this.state = {
			contents: [],
			loading: true,
		};
	}

	componentDidMount() {
		Axios.get(window.App.url.rest + '/topics/' + this.props.topic + '/contents', {
			cancelToken: new Axios.CancelToken((c) => { this.cancelRequest = c; })
		}).then((response) => {
			this.cancelRequest = false;
			this.setState({
				contents: response.data,
				loading: false
			});
		}).catch((error) => { this.cancelRequest = false; });
	}

	componentWillUnmount() {
		if (this.cancelRequest != false)
			this.cancelRequest();
	}

	render() {
		return (
			<div>
				{
					this.state.contents.map((content) => {
						return (
							<Card key={ content.slug } className="mb-2">
								<CardBlock className="d-flex flex-column justify-content-start align-items-start">
									<p className="lead">
										<strong>{ content.label }</strong><br />
										<span className="small text-muted text-uppercase">{ content.appearance } / { content.type }</span>
									</p>
									<Link to={ '/topics/' + this.props.topic + '/content/' + content.slug } 
										className="btn btn-outline-primary btn-sm text-uppercase">
										Mulai <Icon name="angle-right" />
									</Link>
								</CardBlock>
							</Card>
							)
					})
				}
			</div>
			);
	}

}

export default TopicSingleContents