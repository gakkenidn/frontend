import React from 'react'
import { Link, NavLink } from 'react-router-dom'
import { ListGroup } from 'reactstrap'

import Icon from '../../../component/Icon'
import Sidebar from '../../../component/Sidebar'

const TopicContentListSidebar = (props) => {

	return (
		<Sidebar>
			<Link to="/topics" className="d-flex flex-nowrap justify-content-start align-items-center mx-2 my-3 text-white no-underline">
				<div className="px-3"><Icon name="angle-left" size="4" /></div>
				<div className="lead pt-1">
					Kembali ke Daftar Topik
				</div>
			</Link>

			{
				props.contents != undefined ?
					(
						<ListGroup className="content-list">
							<NavLink exact to={ '/topics/' + props.topic } className="list-group-item">
								<div>Overview Topik</div>
							</NavLink>
							{
								props.contents.map((content) => {
									if (content.type == 'certificate' && props.isTrial)
										return null;

									let icon = null;
									
									switch(content.type) {
										case 'certificate'	: icon = 'certificate'; break;
										case 'image'    		: icon = 'picture-o'; break;
										case 'quiz'     		: icon = 'question-circle'; break;
										case 'video'    		: icon = 'video-camera'; break;
										default         		: icon = 'file';
									}
									if (
										content.pendingPrerequisite !== null
										|| ( content.type === 'certificate' && props.passed === null )
										)
										return (
											<div 
												key={ content.slug } 
												className="list-group-item disabled">
												<div className="d-flex flex-nowrap justify-content-start align-items-start">
													<div className="mr-2"><Icon name={ icon } fixed /></div>
													<div className="text">{ content.label }</div>
												</div>
												{
													content.type === 'certificate' ?
														<div className="small">Anda belum melulusi tes soal</div> :
														<div className="small">Anda perlu menyelesaikan <strong>{ content.pendingPrerequisite }</strong></div>
												}
											</div>
											);
									return (
										<NavLink 
											key={ content.slug } 
											to={ '/topics/' + props.topic + '/content/' + content.slug } 
											className="list-group-item d-flex flex-nowrap justify-content-start align-items-start">
											<div className="mr-2"><Icon name={ icon } fixed /></div>
											<div className="text">{ content.label }</div>
										</NavLink>
										);
								})
							}
						</ListGroup>
						) : ''
			}
		</Sidebar>
		);

}

export default TopicContentListSidebar