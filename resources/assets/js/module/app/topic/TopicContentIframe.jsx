import React from 'react'

export default class TopicContentIframe extends React.Component {

	componentDidMount() {
		return this.props.onComplete(this.props.content);
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.content.slug !== this.props.content.slug) {
			console.log('fire onComplete!');
			return this.props.onComplete(nextProps.content);
		}
	}

	render() {
		return (
			<iframe src={ this.props.url } height="100%" width="100%" style={{ border: 'none' }} />
			);
	}

}
