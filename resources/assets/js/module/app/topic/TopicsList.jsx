import React from 'react'
import { Link } from 'react-router-dom'
import { ListGroup } from 'reactstrap'
import PromptEnableTrialTopic from './PromptEnableTrialTopic'

const TopicsList = (props) => {

	if (!props.topics.length) {
		return (
			<div className="col-lg-10 text-center">
				<p className="lead">
					Daftar topik pembelajaran Anda saat ini kosong.<br />
					Silakan beli paket berlangganan di <a href={ window.App.url.base + '/subscribe' }>halaman berlangganan</a>.
				</p>
			</div>
			);
	}

	return (
		<div className="col-lg-10">
			{
				App.promptEnableTrialTopic !== false ? <PromptEnableTrialTopic options={ App.promptEnableTrialTopic } onSave={ props.onReload } /> : null
			}

			<h6 className="section-header text-muted">Topik Saya</h6>
			<ListGroup className="topics-list">
				{
					props.topics.map((topic) => {
						return (
							<Link key={ topic.slug } to={ '/topics/' + topic.slug } className="list-group-item p-0 d-flex flex-nowrap justify-content-start align-items-center">
								<img src={ 'https://vault.gakken-idn.id/topic/files/' + topic.image } className="mr-3" style={{ height: 100 }} />
								<div className="py-2 pr-3">
									<div className="h5">{ topic.title }</div>
									<div>
										<span className="small text-muted text-uppercase">Topik { topic.category }&reg; { topic.release }</span>
										{
											topic.passedAt !== null ?
												<span className="badge badge-success ml-3"><i className="fa fa-check" /> Telah dilulusi pada { topic.passedAt }</span> : null
										}
									</div>
								</div>
							</Link>
							);
					})
				}
			</ListGroup>
		</div>
		);

}

export default TopicsList