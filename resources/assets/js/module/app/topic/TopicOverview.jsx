import React from 'react'
import { Link } from 'react-router-dom'

import Icon from '../../../component/Icon'
import Loading from '../../../component/Loading'

const TopicOverview = (props) => {

	return (
		<div className="container py-5">
			<div className="row justify-content-center">
				{
					props.loading == true ?
						<Loading /> :
						(
							<div className="col-lg-10">
								<h6 className="section-header text-muted">Topik Gakken P2KB&reg;</h6>

								<div className="row mb-5">
									<div className="col-md-4">
										<img src={ 'https://vault.gakken-idn.id/topic/files/' + props.topic.image } className="w-100" />
									</div>
									<div className="col-md-8 d-flex flex-column justify-content-between">
										<div>
											<h1>{ props.topic.title }</h1>
											<p className="lead">{ props.topic.release }</p>
										</div>
										<div>
											<Link to={ '/topics/' + props.topic.slug + '/content/' + props.firstContent } className="btn btn-outline-primary text-uppercase">
												Belajar dari awal <Icon name="play" className="ml-1" />
											</Link>
										</div>
									</div>
								</div>

								<hr className="mb-5" />

								<div className="d-flex mb-5">
									<div className="mr-4">
										<div className="h6 text-uppercase text-muted ">Jumlah SKP</div>
										<div className="lead">2</div>
									</div>
									<div className="mx-4">
										<h6 className="text-uppercase text-muted">Standar Kelulusan</h6>
										<div className="lead">80%</div>
									</div>
									<div className="mx-4">
										<h6 className="text-uppercase text-muted">Pengajar</h6>
										<div className="lead">{ props.topic.lecturer }</div>
									</div>
								</div>

								<div className="mb-5" dangerouslySetInnerHTML={{ __html: props.topic.summary }} />

							</div>
							)
				}
			</div>
		</div>
		);

}

export default TopicOverview