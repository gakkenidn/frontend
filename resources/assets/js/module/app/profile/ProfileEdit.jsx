import React from 'react'
import { Link } from 'react-router-dom'
import { 
	Button, 
	ButtonDropdown, 
	DropdownItem,
	DropdownMenu, 
	DropdownToggle, 
	FormFeedback, 
	FormGroup, 
	Input, 
	InputGroup, 
	InputGroupButton, 
	Label
} from 'reactstrap'

class ProfileEdit extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			profile: props.profile,
			email: props.profile.email,
			name: props.profile.name,
			profession: props.profile.profession,
			idNo: props.profile.idNo != null ? props.profile.idNo : '',

			professionDropdownOpen: false
		};
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.profile.email == undefined)
			this.setState({ 
				profile: nextProps.profile,
				email: nextProps.profile.email,
				name: nextProps.profile.name,
				profession: nextProps.profile.profession,
				idNo: nextProps.profile.idNo
			});
	}

	componentWillUnmount() {
		return this.props.onUnmount();
	}

	handleChange(e) {
		const field = e.target.name;
		const newState = { [field]: e.target.value};
		this.setState(newState);
	}

	handleSave() {
		return this.props.onSave({
			name: this.state.name,
			profession: this.state.profession,
			idNo: this.state.idNo
		});
	}

	toggleProfessionDropdown() {
		if (!this.props.loading)
			this.setState({ professionDropdownOpen: !this.state.professionDropdownOpen });
	}

	render() {
		if (this.props.profile.email == undefined)
			return null;

		return (
			<div>
				<h1 className="mb-4">Edit Profil</h1>
				<FormGroup>
					<Label className="small text-uppercase text-muted">Alamat Email</Label>
					<p>
						<span className="h5 mr-3">{ this.state.profile.email }</span>
						<small><a href="#">Ubah</a></small>
					</p>
				</FormGroup>
				<FormGroup color={ this.props.errors.name != undefined ? "danger" : ''}>
					<Label className="small text-uppercase text-muted">Nama Lengkap</Label>
					<Input name="name" value={ this.state.name } size="lg" onChange={ this.handleChange.bind(this) } disabled={ this.props.saving } />
					{
						this.props.errors.name != undefined ? 
							<FormFeedback>{ this.props.errors.name[0] }</FormFeedback> : null
					}
				</FormGroup>
				<FormGroup color={ this.props.errors.profession != undefined || this.props.errors.idNo != undefined ? "danger" : ''}>
					<Label className="small text-uppercase text-muted">Profesi / Nomor ID Keprofesian</Label>
					<div>
						<InputGroup>
							<InputGroupButton>
								<ButtonDropdown isOpen={ this.state.professionDropdownOpen } toggle={ this.toggleProfessionDropdown.bind(this) } size="lg">
					        <DropdownToggle caret>
					          { this.state.profession == null ? 'Pilih profesi' : (this.state.profession == 'doctor' ? 'Dokter (IDI)' : 'Dokter Gigi (PDGI)') }
					        </DropdownToggle>
					        <DropdownMenu>
					          <DropdownItem name="profession" value="doctor" onClick={ this.handleChange.bind(this) }>Dokter</DropdownItem>
					          <DropdownItem name="profession" value="dentist" onClick={ this.handleChange.bind(this) }>Dokter Gigi</DropdownItem>
					        </DropdownMenu>
					      </ButtonDropdown>
				      </InputGroupButton>
					    <Input name="idNo" size="lg" value={ this.state.idNo } onChange={ this.handleChange.bind(this) } disabled={ this.props.saving } />
					  </InputGroup>
			    </div>
					{
						this.props.errors.profession != undefined || this.props.errors.idNo != undefined ? 
							<FormFeedback>{ this.props.errors.profession != undefined ? this.props.errors.profession[0] : this.props.errors.idNo[0] }</FormFeedback> : null
					}
				</FormGroup>
				<hr className="my-5" />
				<FormGroup>
					<Button size="lg" color="primary" disabled={ this.props.saving } onClick={ this.handleSave.bind(this) }>Simpan</Button>
					<Link to="/profile" className="btn btn-lg btn-link" disabled={ this.props.saving }>Batal</Link>
				</FormGroup>
			</div>
			);
	}

}

export default ProfileEdit