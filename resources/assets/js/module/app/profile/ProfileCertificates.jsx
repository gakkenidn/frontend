import React from 'react'
import Axios from 'axios'
import Loading from '../../../component/Loading'

export default class ProfileCertificates extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			highlight: null,
			isFetching: true,
			certificates: [],
		};
		this.cancelRequest = false;
	}

	componentDidMount() {
		Axios.get(App.url.rest + '/topics/certificate/my', {
			cancelToken: new Axios.CancelToken(c => { this.cancelRequest = c; })
		})
			.then(r => {
				this.cancelRequest = false;
				this.setState({ 
					isFetching: false,
					certificates: r.data.data.slice(0) 
				});
			})
			.catch(e => { 
				this.cancelRequest = false;
				throw(e);
			});
	}

	componentWillUnmount() {
		if (this.cancelRequest !== false)
			this.cancelRequest();
	}

	render() {
		return (
			<div>
				<h1 className="mb-4">Sertifikat</h1>
				<p className="mb-5">
					Berikut daftar topik yang telah Anda lulusi. Unduh dan cetak file 
					sertifikat Anda disini.
				</p>
				{
					this.state.isFetching ?
						(
							<div className="text-center">
								<Loading />
							</div>
							) : (
							<div>
								{
									this.state.certificates.length ?
										(
											<div>
												<div className="alert alert-info">
													<div className="mb-2"><strong><i className="fa fa-info-circle" /> Informasi</strong></div>
													<p className="mb-0">
														Pastikan Anda mendapatkan cap asli dari 
														{
															this.props.profile.profession === 'doctor' ?
																<span> Ikatan Dokter Indonesia (IDI) </span> :
																<span> Persatuan Dokter Gigi Indonesia (PDGI) </span>
														}
														Wilayah Sulselbar sebelum menggunakan sertifikat untuk pembaruan STR/SIP.
													</p>
												</div>
												{
													this.state.certificates.map((cert, i) => {
														return (
															<div 
																key={ i } 
																style={{ transition: 'all 0.2s ease-out' }}
																className={ 'pr-3' + (this.state.highlight === i ? ' bg-light px-3' : '') }
																onMouseOut={ () => this.setState({ highlight: null })}
																onMouseOver={ () => this.setState({ highlight: i })}>
																<div className="py-3 d-flex justify-content-between align-items-center">
																	<div>
																		<div className="lead">{ cert.title }</div>
																		<div>
																			<span><span className="text-muted">Nilai</span> { cert.grade }%</span>
																			<span className="ml-3"><span className="text-muted">Poin SKP</span> 2</span>
																			<span className="ml-3"><span className="text-muted">Tanggal Kelulusan</span> { cert.passedAt }</span>
																		</div>
																	</div>
																	<div>
																		<a target="_blank" href={ App.url.base + '/topics/certificate/' + cert.slug } className="btn btn-outline-primary">
																			Unduh/Cetak Sertifikat <i className="fa fa-download" />
																		</a>
																	</div>
																</div>
															</div>
															);
														})
													}
											</div>
											) : null
								}
							</div>
							)
				}
			</div>
			);
	}

}