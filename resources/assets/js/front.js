/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$(document).ready(() => {
	
	if ($('.navbar-fixed').length) {
		const threshold = 300;
		const mainNavbar = $('.navbar-main').get(0);
		const fixedNavbar = $('.navbar-fixed').get(0);
		let scrolled = false;

		$(window).on('scroll', function() {
			let nowScrolled = $(this).scrollTop() > threshold;
			$(fixedNavbar).toggleClass('scrolled', nowScrolled);

			if (scrolled != nowScrolled) {
				$(mainNavbar).find('.dropdown.show').removeClass('show');
				$(fixedNavbar).find('.dropdown.show').removeClass('show');
			}
			scrolled = nowScrolled;
		});
	}

	if ($('#auth-dialog').length) {
		const modal = $('#auth-dialog');
		modal.on('show.bs.modal', function (e) {
			if ($(window).width() < 768) {
				window.location.href = "/login";
				return false;
			}
			e.relatedTarget.blur();
		});
	}

});