import React from 'react'
import ReactDOM from 'react-dom'
import ReactGA from 'react-ga'
import Axios from 'axios'
import { BrowserRouter, NavLink, Redirect, Route, Switch } from 'react-router-dom'
import { Navbar, Nav, NavItem, NavbarBrand, Collapse } from 'reactstrap'

import Icon from './component/Icon'
import RouteListener from './component/RouteListener'

import Topics from './module/app/topic/Topics'
import TopicSingle from './module/app/topic/TopicSingle'
import Journals from './module/app/journal/Journals'
import Drugs from './module/app/drug/Drugs'
import Profile from './module/app/profile/Profile'

class App extends React.Component {

	handleLogout(e) {
		e.preventDefault();
		Axios.post(window.App.url.logout)
			.then((response) => {
				if (response.data.status == 'success')
					window.location.href = window.App.url.base;
			});
	}

	handleRouteChange(location) {
		if (window.App.analytics.active) {
			ReactGA.set({ page: '/app' + location });
		  ReactGA.pageview('/app' + location);
		}
	}

	render() {
		return (
			<BrowserRouter basename="/app">
				<div>
					<Navbar className="justify-content-between align-items-center navbar-expand-lg navbar-dark bg-dark" color="inverse" fixed="top">
						<div className="d-flex justify-content-start align-items-center">
							<NavbarBrand href="/">
								<img src="/images/logo-white.png" style={{ height: 30 }} />
							</NavbarBrand>
							<Collapse isOpen={ true }>
								<Nav navbar>
									<NavItem>
										<NavLink to="/topics" className="nav-link">Topik</NavLink>
									</NavItem>
									<NavItem>
										<NavLink to="/journals" className="nav-link">Jurnal</NavLink>
									</NavItem>
									<NavItem>
										<NavLink to="/drugs" className="nav-link">Indeks Obat</NavLink>
									</NavItem>
									<NavItem>
										<NavLink to="/profile" className="nav-link">{ window.App.user.name }</NavLink>
									</NavItem>
								</Nav>
							</Collapse>
						</div>
						<div>
							<Nav navbar>
								<NavItem>
									<a href="#" className="nav-link" onClick={ this.handleLogout.bind(this) }>Keluar <Icon name="sign-out" /></a>
								</NavItem>
							</Nav>
						</div>
					</Navbar>

					<Switch>
						<Route exact path="/topics" component={ Topics } />
						<Route path="/topics/:topic" component={ TopicSingle } />
						<Route path="/journals" component={ Journals } />
						<Route path="/drugs" component={ Drugs } />
						<Route path="/profile" component={ Profile } />

						<Redirect to="/topics" />
					</Switch>
					<RouteListener onChange={ this.handleRouteChange.bind(this) } />
				</div>
			</BrowserRouter>
			);
	}

}

if (window.App.analytics.active) {
	ReactGA.initialize(window.App.analytics.trackingId);
	ReactGA.set({ userId: window.App.user.id });
	ReactGA.set({ dimension1: window.App.user.id });
}
ReactDOM.render(<App />, document.getElementById('app'));