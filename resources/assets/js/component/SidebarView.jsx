import React from 'react'

const SidebarView = (props) => {

	return (
		<div className="sidebar-view">
			{ props.children }
		</div>
		);

}

export default SidebarView