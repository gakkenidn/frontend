import React from 'react'

const DetailView = (props) => {

	return (
		<div className="detail-view">
			{ props.children }
		</div>
		);

}

export default DetailView