import React from 'react'

const MasterView = (props) => {

	return (
		<div className="master-view d-flex flex-column flex-nowrap justify-content-start">
			{ props.children }
		</div>
		);

}

export default MasterView