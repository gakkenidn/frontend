import React from 'react'

const MasterDetailView = (props) => {

	return (
		<div className="master-detail-view">
			{ props.children }
		</div>
		);

}

export default MasterDetailView