import React from 'react';

const Filter = props => (
  <div className="d-flex">
    <div className="mr-3"><i className="fa fa-filter fa-2x text-muted" /></div>
    <div className="pt-1">
      <h5><a href="#" onClick={ e => { e.preventDefault(); props.onChange(false) }} className={ props.active ? 'text-dark' : 'text-primary' }>Semua Kategori</a></h5>
      {
        window.App.categories.map(cat => (
          <div key={ cat.slug }>
            <a
              href="#"
              onClick={ e => { e.preventDefault(); props.onChange(cat.slug) }}
              className={ props.active === cat.slug ? 'text-primary' : 'text-dark' }>
              { cat.label }
            </a>
          </div>
        ))
      }
    </div>
  </div>
);

export default Filter;