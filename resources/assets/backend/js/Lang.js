export const GLOBAL_BACK = "Back";
export const GLOBAL_FETCHING = "Loading...";
export const GLOBAL_RESET = "Reset";
export const GLOBAL_SAVE_NEW = "Save";
export const GLOBAL_SAVE_EDIT = "Save Changes";

export const USERS_LIST_TITLE = "All Users";
export const USERS_LIST_COLUMNS = {
	name: 'Name',
	role: 'Role',
	email: 'Email',
	groups: 'Group',
	registeredAt: 'Registered'
};
export const USERS_LIST_FETCHING = "Fetching users list...";
export const USERS_LIST_FETCH_ERROR = "Oops! We couldn't fetch the users list.";
export const USERS_LIST_FETCH_ERROR_RETRY = "Try again";

export const USERS_SINGLE_TITLE = "Editing User:";
export const USERS_SINGLE_EMAIL_HELP = "Please note that when changing email address here, the email address for the corresponding user in Moodle should also be changed.";
export const USERS_SINGLE_PASSWORD_RESET_HELP = "Send a an email to this user's email address containing a reset link.";
export const USERS_SINGLE_SECTIONS = {
	OVERVIEW: 'Overview',
	SUBSCRIPTIONS: 'Subscriptions',
	LEARNING: 'Learning'
};
export const USERS_SINGLE_SUBSCRIPTION_HELP = "Select plan to immediately apply for the user. If the user has an active subscription, the plan will be applied immediately after the current active subscription has expired."

export const APPS_LIST_TITLE = "All Apps";
export const APPS_FORM = {
	NAME: 'Name' ,
	URL: 'URL' ,
	SAVE: 'Save New App'
};
export const APPS_LIST_COLUMNS = {
	name: 'Name',
	url: 'URL',
	key: 'Key' ,
	createdAt: 'Created At'
};

export const APP_USERS_LIST_COLUMNS = {
	name: 'Name',
};
export const APP_USERS_NEW_TITLE = "Fill form below to add new user";
export const APP_USERS_FETCHING = "Fetching users list...";
export const APP_USERS_FETCH_ERROR = "Oops! We couldn't fetch the users list.";
export const APP_USERS_FETCH_ERROR_RETRY = "Try again";