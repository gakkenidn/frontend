import Thunk from 'redux-thunk'
import RootReducer from './Reducers/Index'
import { createStore, compose, applyMiddleware } from 'redux'

const defaultState = {
	plans: {
		list: {
			data: [],
			isFetching: false
		}
	},
	roles: {
		list: {
			data: [],
			isFetching: false
		}
	},
	topics: {
		list: { data: [] }
	},
	users: {
		list: {
			data: [],
			searchTerm: '',
			pagination: {
				page: 1,
				perPage: 10,
				totalPages: 1
			},
			isError: false,
			isFetching: false
		},
		single: {
			user: { 
				email: '',
				topics: [],
				currentSubscription: null
			},
			isError: false,
			isFetching: false,
			availableRoles: [],
			isEnrolling: false,
			isNewlyAdded: false,
			isSubscribing: false
		},
		create: {
			isPosting: false
		}
	} ,
	apps: {
		list: {
			data: [],
			searchTerm: '',
			pagination: {
				page: 1,
				perPage: 10,
				totalPages: 1
			},
			isError: false,
			isFetching: false
		},
		single: {
			app: { 
				name: '',
				url: ''
			},
			isError: false,
			isFetching: false,
			isNewlyAdded: false
		},
		create: {
			isPosting: false
		} ,
		users: {
			list : {
				data: [],
				searchTerm: '',
				pagination: {
					page: 1,
					perPage: 10,
					totalPages: 1
				},
				isError: false,
				isFetching: false
			}
		}
	}
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
	RootReducer,
	defaultState,
	composeEnhancers(applyMiddleware(Thunk))
	);