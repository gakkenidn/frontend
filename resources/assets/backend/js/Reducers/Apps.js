import {
	APPS_LIST_CHANGE_PAGE,
	APPS_LIST_INVALIDATE,
	APPS_LIST_POPULATE,
	APPS_LIST_SET_SEARCH_TERM,
	APPS_POST,
	APPS_SINGLE_INVALIDATE,
	APPS_SINGLE_SET_APP ,
	APP_USERS_LIST_POPULATE ,
	APP_USERS_LIST_INVALIDATE
} from '../Actions/Types'

export default (state = {}, action) => {

	let newState = Object.assign({}, state);

	switch(action.type) {

		case APPS_SINGLE_SET_APP:
			newState.single = Object.assign({}, {
				app: Object.assign({}, action.data.app),
				isNewlyAdded: action.newlyAdded,
				isFetching: false,
			});
			newState.create.isPosting = false;
			break;

		case APPS_LIST_POPULATE:
			newState.list = Object.assign(newState.list, {
				isFetching : false,
				data : action.apps.slice(0),
				pagination: Object.assign(newState.list.pagination, {
					page: 1,
					totalPages: Math.ceil(action.apps.length / newState.list.pagination.perPage)
				})
			});
			break;

		case APPS_LIST_CHANGE_PAGE:
			newState.list.pagination = Object.assign(newState.list.pagination, {
				page: newState.list.pagination.page + (action.next ? 1 : -1)
			});
			break;

		case APP_USERS_LIST_POPULATE:
			newState.users.list = Object.assign(newState.users.list, {
				isFetching : false,
				data : action.users.data,
				pagination: Object.assign(newState.users.list.pagination, {
					page: action.users.current_page,
					totalPages: Math.ceil(action.users.total / action.users.per_page)
				})
			});
			break;

		case APPS_LIST_INVALIDATE: 			  newState.list.isFetching = true; break;
		case APPS_LIST_SET_SEARCH_TERM: 	newState.list.searchTerm = action.term;	break;
		case APPS_POST: 									newState.create.isPosting = true; break;
		case APPS_SINGLE_INVALIDATE:			newState.single.isFetching = true; break;
		case APP_USERS_LIST_INVALIDATE: 	newState.users.list.isFetching = true; break;

		default: return newState;
	}

	return newState;
}