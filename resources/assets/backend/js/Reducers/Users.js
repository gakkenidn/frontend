import {
	USERS_LIST_CHANGE_PAGE,
	USERS_LIST_INVALIDATE,
	USERS_LIST_POPULATE,
	USERS_LIST_SET_SEARCH_TERM,
	USERS_POST,
	USERS_SINGLE_ENROL,
	USERS_SINGLE_INVALIDATE,
	USERS_SINGLE_SET_USER,
	USERS_SINGLE_SUBSCRIBE
} from '../Actions/Types'

export default (state = {}, action) => {

	let newState = Object.assign({}, state);

	switch(action.type) {

		case USERS_SINGLE_SET_USER:
			newState.single = Object.assign({}, {
				user: Object.assign({}, action.data.user),
				availableRoles: action.data.availableRoles.slice(0),
				isNewlyAdded: action.newlyAdded,
				isFetching: false,
				isEnrolling: false,
				isSubscribing: false
			});
			newState.create.isPosting = false;
			break;

		case USERS_LIST_POPULATE:
			newState.list = Object.assign(newState.list, {
				isFetching : false,
				data : action.users.slice(0),
				pagination: Object.assign(newState.list.pagination, {
					page: 1,
					totalPages: Math.ceil(action.users.length / newState.list.pagination.perPage)
				})
			});
			break;

		case USERS_LIST_CHANGE_PAGE:
			newState.list.pagination = Object.assign(newState.list.pagination, {
				page: newState.list.pagination.page + (action.next ? 1 : -1)
			});
			break;

		case USERS_LIST_INVALIDATE: 			newState.list.isFetching = true; break;
		case USERS_LIST_SET_SEARCH_TERM: 	newState.list.searchTerm = action.term;	break;
		case USERS_POST: 									newState.create.isPosting = true; break;
		case USERS_SINGLE_ENROL:					newState.single.isEnrolling = true; break;
		case USERS_SINGLE_INVALIDATE:			newState.single.isFetching = true; break;
		case USERS_SINGLE_SUBSCRIBE: 			newState.single.isSubscribing = true; break;

		default: return newState;
		
	}

	return newState;

}