import Axios from 'axios'

export default class AppsApi {

	static get(params = {}, url = null) {
		return Axios.get(url == null ? App.restUrl + '/apps' : url, { params }).then(response => {
			return response.data.data;
		}).catch(error => error);
	}

	static single(id) {
		return Axios.get(App.restUrl + '/apps/' + id)
			.then(response => response.data)
			.catch(error => error);
	}

	static post(data) {
		return Axios.post(App.restUrl + '/apps', data)
			.then(response => response.data)
			.catch(error => error);
	}

	static patch(id, data) {
		return Axios.patch(App.restUrl + '/apps/' + id, data)
			.then(response => response.data)
			.catch(error => error);
	}

	static getAvailableUsers( id , params = {} ) {
		return Axios.get(App.restUrl + '/apps/' + id + '/users' , { params } )
			.then(response => response.data)
			.catch(error => error);
	}

	static getAssignedUsers( id , params = {} ) {
		return Axios.get(App.restUrl + '/apps/' + id + '/users' , { params } )
			.then(response => response.data)
			.catch(error => error);
	}

	static assignUser( data ) {
		return Axios.post(App.restUrl + '/apps/assignuser', data)
			.then(response => response.data)
			.catch(error => error);
	}

	static removeUser( data ) {
		return Axios.delete(App.restUrl + '/apps/removeuser', { data })
			.then(response => response.data)
			.catch(error => error);
	}
}