import Axios from 'axios'

export default class PlansApi {

	static get() {
		return Axios.get(App.restUrl + '/plans')
			.then(r => r.data)
			.catch(e => e);
	}

}