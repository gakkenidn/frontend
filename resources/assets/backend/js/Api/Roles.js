import Axios from 'axios'

export default class RolesApi {

	static get() {
		return Axios.get(App.restUrl + '/roles')
			.then(r => r.data)
			.catch(e => { throw(e) });
	}

	static show(id, cb) {
		return Axios.get(App.restUrl + '/roles/' + id, {
			cancelToken: new Axios.CancelToken(c => cb(c))
		})
			.then(r => Object.assign({}, r.data.data))
			.catch(e => { throw(e) });
	}
}