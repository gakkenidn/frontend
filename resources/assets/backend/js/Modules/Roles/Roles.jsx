import List from './List'
import React from 'react'
import Single from './Single'
import { Redirect, Route, Switch } from 'react-router-dom'

export default props => 
	(
		<Switch>
			<Route exact path="/users/roles/all" render={ route => <List { ...props.roles.list } { ...props.actions.Roles } { ...route } /> } />
			<Route path="/users/roles/:roleId" render={ route => <Single { ...props.actions.Roles } { ...route } /> } />

			<Redirect to="/users/roles/all" />
		</Switch>
		);
