import Edit from './Edit'
import React from 'react'
import Api from '../../Api/Roles'
import Abilities from './Abilities'
import { NavLink, Link, Route, Switch } from 'react-router-dom'

export default class Single extends React.Component {

	constructor(props) {
		super(props);
		this.cancelRequest = false;
		this.state = {
			role: {
				name: '',
				abilities: []
			},
			title: '',
			isError: false,
			isSaving: false,
			isFetching: true
		};
	}

	componentDidMount() {
		return this.load();
	}

	componentWillUnmount() {
		if (this.cancelRequest !== false)
			this.cancelRequest();
	}

	load() {
		this.setState({ isFetching: true, isError: false });
		Api.show(this.props.match.params.roleId, c => { this.cancelRequest = c; })
			.then(role => {
				this.setState({ role, isFetching: false, title: role.name });
				this.cancelRequest = false;
			})
			.catch(e => { 
				this.cancelRequest = false;
			});
	}

	save(context) {

	}

	render() {
		return (
			<div>
				<div className="headerbar bg-white d-flex align-items-center">
					<div className="container-fluid py-3 d-flex justify-content-between align-items-center">
						<div><Link to="/users/roles/all"><i className="fa fa-angle-left" /> Roles List</Link></div>
						<div className="h5 m-0">
							{
								!this.state.isError ?
									<span>Edit Role: { this.state.title.length ? this.state.title : 'Fetching..' }</span> : null
							}
						</div>
						<div>
						</div>
					</div>
				</div>
				<div className="main-container">
					<div className="container py-5">

						{
							this.state.isError ?
								<div>Failed fetching role data. <a href="#" onClick={ e => { e.preventDefault(); return this.load(); }}>Try again</a></div> :
								(
									<div className="row">
										<div className="col-2 sticky-top">
											<ul className="nav nav-pills flex-column">
												<li className="nav-item"><NavLink exact to={ '/users/roles/' + this.props.match.params.roleId } className="nav-link">Overview</NavLink></li>
												<li className="nav-item"><NavLink exact to={ '/users/roles/' + this.props.match.params.roleId + '/abilities' } className="nav-link">Abilities</NavLink></li>
											</ul>
										</div>
										<div className="col-10">
											<Switch>
												<Route exact path="/users/roles/:roleId" children={ route => <Edit { ...route } { ...this.state } onChange={ value => this.setState({ role: Object.assign(this.state.role, value) }) } onSave={ () => this.save('overview') } /> } />
												<Route path="/users/roles/:roleId/abilities" render={ route => <Abilities { ...route } isFetching={ this.state.isFetching } abilities={ this.state.role.abilities } /> } />
											</Switch>
										</div>
									</div>
									)
						}

					</div>
				</div>
			</div>
			);
	}

}