import React from 'react'
import Axios from 'axios'

export default class Abilities extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			abilities: props.abilities.slice(0)
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ abilities: nextProps.abilities.slice(0) });
	}

	handleChange(namespace) {
		return this.setState({ abilities: this.state.abilities.map(ab => {
			if (ab.namespace === namespace)
				ab.isPermitted = !ab.isPermitted;
			return ab;
		})});
	}

	handleSave() {
		console.log(this.state.abilities);
	}

	render() {
		let structured = this.structurize(this.state.abilities);
		return (
			<div>
				{
					this.props.isFetching ?
						<p className="text-center">Loading abilities..</p> :
						(
							<div>
								{
									structured.map((app, i) => {
										return (
											<div key={ i }>
												<h3>{ app.name }</h3>
												{
													app.modules.map((mod, j) => {
														return (
															<div key={ j } className="mb-3">
																<h5>{ mod.name }</h5>
																{
																	mod.actions.map((action, k) => {
																		return (
																			<div key={ k }>
																				<label className="custom-control custom-checkbox">
																				  <input type="checkbox" className="custom-control-input" checked={ action.isPermitted } onChange={ () => this.handleChange(action.namespace) } />
																				  <span className="custom-control-indicator"></span>
																				  <span className="custom-control-description">{ action.action }</span>
																				</label>
																			</div>
																			);
																	})
																}
															</div>
															);
													})
												}
											</div>
											);
									})
								}
							</div>
							)
				}
			</div>
			);
	}

	structurize(abilities) {
		let structured = [];
		let current = { app: null };
		for (let i = 0; i < abilities.length; i++) {
			if (abilities[i].app !== current.app) {
				current = abilities[i];
				structured.push({name: current.app, modules: []});
			}
		}
		for (let i = 0; i < structured.length; i++) {
			current = { module: null };
			for (let j = 0; j < abilities.length; j++) {
				if (
					abilities[j].module !== current.module
					&& abilities[j].app === structured[i].name) {
					current = abilities[j];
					structured[i].modules.push({ name: current.module, actions: []});
				}
			}
		}
		for (let i = 0; i < structured.length; i++) {
			for (let j = 0; j < structured[i].modules.length; j++) {
				current = { action: null };
				for (let k = 0; k < abilities.length; k++) {
					if (
						abilities[k].action !== current.action
						&& abilities[k].app === structured[i].name
						&& abilities[k].module === structured[i].modules[j].name) {
						structured[i].modules[j].actions.push(abilities[k]);
					}
				}
			}
		}
		return structured;
	}

}