import React, { Component } from 'react'
// import SearchBox from '../../Components/SearchBox'
import SearchSuggest from '../../Components/SearchSuggest'
import appsApi from '../../Api/Apps'
import {
  APP_USERS_LIST_COLUMNS ,
  APP_USERS_NEW_TITLE ,
  APP_USERS_FETCHING ,
  APP_USERS_FETCH_ERROR ,
  APP_USERS_FETCH_ERROR_RETRY
} from '../../Lang'

export default class List extends Component {
  constructor( props ) {
    super( props );

    this.state = {
      newuser: ""
    };
  }
	componentDidMount() {
		this.props.loadUsers( this.props.match.params.id );
	}

	handlePager(next) {
		this.props.pagerUser( this.props.match.params.id, next);
	}

	handleRefresh() {
		// this.props.invalidateList();
		// return this.props.loadList({ type: 'reload' });
	}

	handleCancelSearch() {
		// this.props.setSearchTerm('');
		// this.props.invalidateList();
		// return this.props.loadList({ type: 'reload' });
	}

	handleSearch(term) {
		// this.props.setSearchTerm(term);
		// return this.props.loadList({ type: 'reload' });
  }
  
  callUsers( value ) {
    return appsApi.getAvailableUsers( this.props.match.params.id, { term: value , context: 'availableToAdd' } )
      .then(result => {        
        const users = [];
        if ( result.users ) {
          result.users.forEach(user => {
            users.push({
              value: user.id ,
              label: user.name
            })
          });
        }
        return users;
      })
      .catch(err => console.log(err));
  }

  assignUser() {
    appsApi.assignUser({ id: this.props.match.params.id, user: this.state.newuser })
      .then(result => {
        this.refs.searchSuggestRef.clear();
        this.props.loadUsers( this.props.match.params.id, {type: 'reload'} )
      })
      .catch(err => console.log(err));
  }

  removeUser(e) {
    e.preventDefault();
    appsApi.removeUser({ user: e.currentTarget.dataset.id, id: this.props.match.params.id})
      .then(result => this.props.loadUsers( this.props.match.params.id, {type: 'reload'} ))
      .catch(err => console.log(err));
  }

	render() {
    const { users } = this.props;    
		return (
        <div className="container">
          {
            users.list.isFetching &&
              (
                <div className="text-center">
                  { APP_USERS_FETCHING }
                </div>
                )
          }
          {
            users.list.isError &&
              (
                <div className="text-center">
                  { APP_USERS_FETCH_ERROR }
                </div>
                )
          }
          <div className={ 'card fade rounded-0' + (!users.list.isFetching ? ' show' : '') }>
            <div className="card-body">
              <div className="d-flex justify-content-between">
                <div>
                {/* { 
                  this.props.searchTerm.length ?  
                    <span>Searched for "{ this.props.searchTerm }", </span> : null
                }
                { this.props.data.length } users found */}
                </div>
                {/* <div>
                  <span className="text-muted mr-3">Legend</span>
                </div> */}
              </div>
            </div>
            <table className="table table-hover">
              <thead className="text-muted text-uppercase small">
                <tr>
                  <th>{ APP_USERS_LIST_COLUMNS.name }</th>
                  <th width="220"></th>
                </tr>
              </thead>
              <tbody>
                {
                  users.list.data.map((user, i) => 
                    <tr key={ i } style={{ transition: 'background 0.1s ease-out' }}>
                      <td>
                        { user.name }
                      </td>
                      <td className="align-middle"> 
                        <a onClick={ this.removeUser.bind(this) } href="#" data-id={ user.id }> Remove </a> 
                      </td>
                    </tr>
                  )
                }
              </tbody>
              <tfoot className="text-muted text-uppercase small">
                <tr>
                  <th style={{lineHeight: "37.77px"}}> 
                    <span className="ml-3">
                      { users.list.pagination.page } / { users.list.pagination.totalPages } Pages
                    </span>
                  </th>
                  <th>
                    <button 
                      type="button" 
                      className="btn btn-light float-right" 
                      onClick={ e => this.handlePager(true) }
                      disabled={ users.list.pagination.page >= users.list.pagination.totalPages || users.isFetching }>
                      <i className="fa fa-fw fa-angle-right" />
                    </button>
                    <button 
                      type="button" 
                      className="btn btn-light float-right" 
                      onClick={ e => this.handlePager(false) }
                      disabled={ users.list.pagination.page <= 1 || users.isFetching }>
                      <i className="fa fa-fw fa-angle-left" />
                    </button>
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
          <div className="mt-3 row">
            <div className="col col-12">
              { APP_USERS_NEW_TITLE }
            </div>
          </div>
          <div className="mt-3 row">
            <div className="col col-8">
              <SearchSuggest
                className="form-control" 
                placeholder="Type a name, username or email"
                value={ this.state.suggestValue }
                ref="searchSuggestRef" 
                onChange={ newuser => this.setState({ newuser })}
                getSuggestions= { this.callUsers.bind(this) }
              />
            </div>
            <div className="col col-4">
              <button 
                className="btn btn-primary" 
                onClick={ this.assignUser.bind(this) } 
                disabled={!this.state.newuser}
                style={ !this.state.newuser ? {
                  cursor: "not-allowed"
                } : {}}
              >
                Add
              </button>
            </div>
          </div>
          
        </div>
			);
	}

}