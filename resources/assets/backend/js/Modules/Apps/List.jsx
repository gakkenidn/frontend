import React from 'react'
import { Link } from 'react-router-dom'
import SearchBox from '../../Components/SearchBox'
import {
	APPS_LIST_TITLE ,
	APPS_LIST_COLUMNS
} from '../../Lang'

export default class List extends React.Component {

	componentDidMount() {
		this.props.loadList();
	}

	handlePager(next) {
		// this.props.pager(next)
	}

	handleRefresh() {
		// this.props.invalidateList();
		// return this.props.loadList({ type: 'reload' });
	}

	handleCancelSearch() {
		// this.props.setSearchTerm('');
		// this.props.invalidateList();
		// return this.props.loadList({ type: 'reload' });
	}

	handleSearch(term) {
		// this.props.setSearchTerm(term);
		// return this.props.loadList({ type: 'reload' });
	}

	render() {
		return (
			<div>
				<div className="headerbar bg-white">
					<div className="container-fluid py-3 d-flex justify-content-between align-items-center">
						<div className="btn-toolbar align-items-center">
							<div className="btn-group">
								<button 
									type="button" 
									className="btn btn-light" 
									// onClick={ e => this.handleRefresh() }
									disabled={ this.props.isFetching }>
									<i className="fa fa-fw fa-refresh" />
								</button>
							</div>
							<div className="btn-group ml-2">
								<button 
									type="button" 
									className="btn btn-light" 
									// onClick={ e => this.handlePager(false) }
									// disabled={ this.props.pagination.page <= 1 || this.props.isFetching }
                >
									<i className="fa fa-fw fa-angle-left" />
								</button>
								<button 
									type="button" 
									className="btn btn-light" 
									// onClick={ e => this.handlePager(true) }
									// disabled={ this.props.pagination.page >= this.props.totalPages || this.props.isFetching }
                  >
									<i className="fa fa-fw fa-angle-right" />
								</button>
							</div>
              <div className="ml-3">
                {/* { this.props.pagination.page } / { this.props.pagination.totalPages } */}
              </div>
						</div>
						<div className="h5 m-0">
							{ APPS_LIST_TITLE }
						</div>
						<div className="d-flex">
							<SearchBox 
								// term={ this.props.searchTerm } 
								// disabled={ this.props.isFetching } 
								// onSearch={ this.handleSearch.bind(this) } 
								// isSearched={ this.props.searchTerm.length }
								// onCancel={ this.handleCancelSearch.bind(this) } 
              />

							<Link to="/apps/new" className="btn btn-primary ml-2"><i className="fa fa-plus" /> Add App</Link>
						</div>
					</div>
				</div>
				<div className="main-container">
					<div className="container py-5">
						{
							this.props.isFetching ?
								(
									<div className="text-center">
										{/* { USERS_LIST_FETCHING } */}
									</div>
									) : null
						}
						{
							this.props.isError ?
								(
									<div className="text-center">
										{/* { USERS_LIST_FETCH_ERROR } */}
									</div>
									) : null
						}
						<div className={ 'card fade rounded-0' + (!this.props.isFetching ? ' show' : '') }>
							<div className="card-body">
								<div className="d-flex justify-content-between">
									<div>
									{/* { 
										this.props.searchTerm.length ?  
											<span>Searched for "{ this.props.searchTerm }", </span> : null
									}
									{ this.props.data.length } users found */}
									</div>
									{/* <div>
										<span className="text-muted mr-3">Legend</span>
									</div> */}
								</div>
							</div>
							<table className="table table-hover">
								<thead className="text-muted text-uppercase small">
									<tr>
										<th>{ APPS_LIST_COLUMNS.name }</th>
										<th width="220">{ APPS_LIST_COLUMNS.createdAt }</th>
									</tr>
								</thead>
								<tbody>
									{
										this.props.data.map((app, i) => {
											if (
												i < this.props.pagination.perPage * (this.props.pagination.page - 1)
												|| i >= this.props.pagination.perPage * (this.props.pagination.page))
												return null;
											return (
												<tr key={ i } style={{ transition: 'background 0.1s ease-out' }}>
													<td>
														<Link to={ '/apps/' + app.id }>{ app.name }</Link>
														<div className="text-muted small">
															1 assigned user(s)
														</div>
													</td>
													<td className="align-middle">{ app.created_at }</td>
												</tr>
												);
										})
									}
								</tbody>
								<tfoot className="text-muted text-uppercase small">
									<tr>
									<th>{ APPS_LIST_COLUMNS.name }</th>
									<th>{ APPS_LIST_COLUMNS.createdAt }</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
			);
	}

}