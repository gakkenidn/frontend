import React from "react";
import { GLOBAL_SAVE_EDIT } from "../../Lang";

export default class Edit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      app:
        props.match.params.id == props.single.app.id
          ? Object.assign({}, props.single.app)
          : {
              name: "",
              key: "",
              url: ""
            }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.single.isFetching)
      this.setState({ app: Object.assign(this.state.app, nextProps.single.app) });
  }

  handleChange(field, value) {
    let app = Object.assign({}, this.state.app);
    app[field] = value;
    this.setState({ app });
  }

  handleSave() {
    if (!this.props.single.isFetching)
			return this.props.patch(this.props.match.params.id, this.state.app);
  }

  render() {
    const { single } = this.props;
    
    return (
      <div>
        <div className="mb-5">
          <div className="row">
            <div className="col-6">
              <div className="form-group">
                <label>Name</label>
                <input
                  type="text"
                  name="name"
                  className="form-control"
                  value={this.state.app.name}
                  disabled={single.isFetching}
                  onChange={e => this.handleChange("name", e.target.value)}
                />
              </div>
            </div>
          </div>

          <div className="row mb-5">
            <div className="col-6">
							<div className="form-group">
                <label>URL</label>
                <input
                  type="text"
                  className="form-control"
                  value={this.state.app.url}
                  onChange={e =>
                    this.handleChange("url", e.target.value)
                  }
                  disabled={single.isFetching}
                />
              </div>
              <div className="form-group">
                <label>Key</label>
                <input
                  type="text"
                  className="form-control"
                  value={this.state.app.key}
                  disabled
                />
              </div>
            </div>
          </div>

          <button
            type="button"
            onClick={ this.handleSave.bind(this) }
            className="btn btn-primary"
          >
            {GLOBAL_SAVE_EDIT} <i className="fa fa-check" />
          </button>
        </div>
        <hr />
      </div>
    );
  }
}
