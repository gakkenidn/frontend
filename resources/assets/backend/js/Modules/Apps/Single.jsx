import React from "react";
import { NavLink, Link, Route, Switch } from "react-router-dom";
import Edit from "./Edit";
import AppUsers from './AppUsers';

export default class Single extends React.Component {
  constructor(props) {
    super(props);
    // this.state = {
    //   title: "",
    //   isError: false,
    //   isSaving: false,
    //   isFetching: true
    // };
  }

  componentWillMount() {
    // console.log(this.props.load);
    
		this.props.load(this.props.route.match.params.id);
	}

  render() {
    return (
      <div>
        <div className="headerbar bg-white d-flex align-items-center">
          <div className="container-fluid py-3 d-flex justify-content-between align-items-center">
            <div>
              <Link to="/apps/all">
                <i className="fa fa-angle-left" /> Apps List
              </Link>
            </div>
            <div className="h5 m-0">
              {
                // !this.state.isError ?
                // 	<span>Edit Role: { this.state.title.length ? this.state.title : 'Fetching..' }</span> : null
              }
            </div>
            <div />
          </div>
        </div>
        <div className="main-container">
          <div className="container py-5">
            <div className="row">
              <div className="col-2 sticky-top">
                <ul className="nav nav-pills flex-column">
                  <li className="nav-item">
                    <NavLink
                      exact
                      to={"/apps/" + this.props.route.match.params.id}
                      className="nav-link"
                    >
                      Overview
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink
                      exact
                      to={
                        "/apps/" +
                        this.props.route.match.params.id +
                        "/users"
                      }
                      className="nav-link"
                    >
                      Users
                    </NavLink>
                  </li>
                </ul>
              </div>
              <div className="col-10">
                <Switch>
                  <Route
                    exact
                    path="/apps/:id"
                    children={route => (
                      <div>
                        <Edit
                          { ...route }
                          { ...this.props }
                        />
                      </div>
                    )}
                  />
                  <Route
                    path="/apps/:id/users"
                    render={route => (
                      <AppUsers
                        { ...route }
                        // isFetching={this.state.isFetching}
                        // abilities={this.state.role.abilities}
                        { ...this.props }
                      />
                    )}
                  />
                </Switch>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
