import React from 'react'
import EnrolForm from './EnrolForm'
import TopicList from './TopicList'
import CertificateApi from '../../Api/Certificate'

export default class Learning extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			isEnrolling: false ,
			certificateClaimSlug: null
		};
	}

	onToggleEnrol(isEnrolling) {
		return this.setState({ isEnrolling });
	}

	claimCertificate(event) {
		const topic = event.currentTarget.dataset.topic;
		this.setState({
			certificateClaimSlug: topic
		});
		
		CertificateApi.post({
			topic ,
			email: this.props.route.match.params.email
		})
			.then(data => {
				if (data.isSuccess) {
					this.props.loadTopic();
					this.setState({
						certificateClaimSlug: null
					});
				}
			})
			.catch(e => { 
				this.setState({
					certificateClaimSlug: null
				});
			});
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.isEnrolling === false && this.props.isEnrolling === true)
			this.setState({ isEnrolling: false });
	}

	render() {
		if (this.props.isFetching || this.props.isSaving)
			return <div className="text-center">Fetching enrolled topics..</div>;

		return (
			<div>
				<div className="d-flex justify-content-between align-items-center">
					<h4 className="mb-3">
						Enrolled Topics
						{
							this.props.user.topics.length ?
								<span className="badge badge-secondary ml-2">{ this.props.user.topics.length }</span> : null
						}
					</h4>
					<div>
						{ this.state.isEnrolling ? null : <button type="button" className="btn btn-outline-primary" onClick={ () => this.onToggleEnrol(true) }><i className="fa fa-plus" /> Enrol to Topic</button> }
					</div>
				</div>
				{ this.state.isEnrolling ? <EnrolForm { ...this.props } onCancel={ () => this.onToggleEnrol(false) } /> : null }
				
				<TopicList { ...this.props } 
					claimCertificate={ this.claimCertificate.bind(this) } 
					certificateClaimSlug={ this.state.certificateClaimSlug }
				/>
			</div>
			);
	}

}