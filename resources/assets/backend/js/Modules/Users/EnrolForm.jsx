import React from 'react'
import { Typeahead } from 'react-typeahead'

export default class EnrolForm extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			selection: null,
			isSearchingTopic: false
		};
	}

	componentDidMount() {
		this.props.loadTopics();
	}

	render() {
		return (
			<div className="card mb-3">
				<div className="card-body">
					<div className="dropdown mb-3">
						<Typeahead 
							maxVisible={ 5 } 
							filterOption="title" 
							displayOption="title"
							placeholder="Search topics.."
							options={ this.props.topics.data } 
							onBlur={ () => this.setState({ isSearchingTopic: false }) }
							disabled={ this.props.isFetching || this.state.isEnrolling } 
							onOptionSelected={ selection => this.setState({ selection }) } 
							onKeyDown={ () => this.setState({ isSearchingTopic: true, selection: null }) }
							customClasses={{ 
								input: 'form-control', 
								listItem: 'dropdown-item', 
								results: 'dropdown-menu' + (this.state.isSearchingTopic ? ' show' : '')
							}} />
					</div>
					<p className="text-muted">
						Choose a topic to manually enrol { this.props.user.name } to it. Since each topic will 
						be automatically enrolled to every month, only do this action when absolutely necessary. 
						Enrolled topics would only be available during an active subscription period.
					</p>
					<button 
						type="button" 
						className="btn btn-outline-primary" 
						disabled={ this.state.selection === null || this.props.isEnrolling } 
						onClick={ () => this.props.enrol(this.state.selection) }>
						Enrol User
					</button>
					<button 
						type="button" 
						className="btn btn-link" 
						disabled={ this.props.isEnrolling } 
						onClick={ () => this.props.onCancel() }>
						Cancel
					</button>
				</div>
			</div>
			);
	}

}
