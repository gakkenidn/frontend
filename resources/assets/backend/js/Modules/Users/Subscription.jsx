import React from 'react'
import SubscriptionList from './SubscriptionList'
import { USERS_SINGLE_SUBSCRIPTION_HELP } from '../../Lang'

export default class Subscription extends React.Component {

	componentDidMount() {
		return this.props.onLoadPlans();
	}

	render() {
		return (
			<div>
				<div className="mb-5">
					<h4>Active Subscription</h4>
					{
						this.props.user.currentSubscription == null ? 
							<p>This user is not currently subscribed to any plan.</p> :
							(
								<div>
									<div className="d-flex justify-content-start align-items-start mt-4 mb-3">
										<div className="mr-4">
											<div className="text-muted">Plan</div>
											<div className="lead">{ this.props.user.currentSubscription.subscription }</div>
										</div>
										<div className="mr-4">
											<div className="text-muted">Duration</div>
											<div className="lead">{ this.props.user.currentSubscription.startsAt + ' to ' + this.props.user.currentSubscription.expiredAt }</div>
										</div>
										<div className="mr-4">
											<div className="text-muted">Type</div>
											<div className="lead">{ this.props.user.currentSubscription.isTrial ? "Trial" : "Standard" }</div>
										</div>
									</div>
									<div><a href="#" onClick={ e => { e.preventDefault(); return this.props.cancelSubscription(); }}>Cancel Subscription</a></div>
								</div>
								)
					}
				</div>
				<hr />
				<div className="my-5 row">
					<div className="col-4">
						<h4>Subscribe User</h4>
						<p>{ USERS_SINGLE_SUBSCRIPTION_HELP }</p>
					</div>
					<div className="col-8">

						<SubscriptionList 
							onSubscribe={ this.props.subscribe }
							plans={ this.props.plans.list.data } 
							isSaving={ this.props.isSubscribing }
							isFetching={ this.props.plans.list.isFetching } />

					</div>
				</div>
			</div>
			);
	}

}