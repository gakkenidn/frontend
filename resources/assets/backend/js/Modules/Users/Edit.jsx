import React from 'react'
import ResetPassword from './ResetPassword'
import Dropdown from '../../Components/Dropdown'
import { GLOBAL_SAVE_EDIT, USERS_SINGLE_EMAIL_HELP } from '../../Lang'

export default class Edit extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			user: props.route.match.params.email == props.user.email ? Object.assign({}, props.user) : {
				name: '', email: '', role: null, nameOnCertificate: '', idNo: '', profession: null
			}
		};
	}

	componentWillReceiveProps(nextProps) {
		if (!nextProps.isFetching)
			this.setState({ user: Object.assign(this.state.user, nextProps.user) });
	}

	handleChange(field, value) {
		let user = Object.assign({}, this.state.user);
		user[field] = value;
		this.setState({ user });
	}

	handleSave() {
		if (!this.props.isFetching)
			return this.props.patch(this.props.route.match.params.email, this.state.user);
	}

	render() {
		return (
			<div>
				<div className="mb-5">
					<div className="row">
						<div className="col-6">
							<div className="form-group">
								<label>Email Address</label>
								<input 
									type="text" 
									name="email" 
									className="form-control" 
									value={ this.state.user.email } 
									disabled={ this.props.isFetching }
									onChange={ e => this.handleChange('email', e.target.value) } />
								<div className="help-block small text-muted mt-2">
									{ USERS_SINGLE_EMAIL_HELP }
								</div>
							</div>
						</div>
					</div>

					<div className="row mb-5">
						<div className="col-6">
							<div className="form-group">
								<label>Full Name</label>
								<input type="text" className="form-control" value={ this.state.user.name } onChange={ e => this.handleChange('name', e.target.value) } disabled={ this.props.isFetching } />
							</div>
							<div className="form-group">
								<label>Name on Certificate</label>
								<input type="text" className="form-control" value={ this.state.user.nameOnCertificate } onChange={ e => this.handleChange('nameOnCertificate', e.target.value) } disabled={ this.props.isFetching } />
							</div>
						</div>
						<div className="col-6">
							<div className="form-group">
								<label>Role</label>
								<Dropdown color="outline-secondary" options={ this.props.availableRoles } selection={ this.state.user.role } block labelKey="name" onSelect={ role => this.handleChange('role', role) }>
									Select role
								</Dropdown>
							</div>
							<div className="row">
								<div className="col-5">
									<div className="form-group">
										<label>Profession</label>
										<Dropdown color="outline-secondary" options={[{ slug: 'doctor', label: 'Doctor'}, { slug: 'dentist', label: 'Dentist'}]} selection={ this.state.user.profession } block labelKey="label" onSelect={ profession => this.handleChange('profession', profession) }>
											Select profession
										</Dropdown>
									</div>
								</div>
								<div className="col-7">
									<div className="form-group">
										<label>Professional ID Number</label>
										<input type="text" className="form-control" value={ this.state.user.idNo } onChange={ e => this.handleChange('idNo', e.target.value) } disabled={ this.props.isFetching } />
									</div>
								</div>
							</div>
						</div>
					</div>

					<button type="button" onClick={ e => this.handleSave() } className="btn btn-primary">{ GLOBAL_SAVE_EDIT } <i className="fa fa-check" /></button>
				</div>
				<hr />
				
				<ResetPassword { ...this.props } onReset={ () => this.props.resetPassword(this.props.route.match.params.email) } />

			</div>
			);
	}

}