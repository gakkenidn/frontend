import React from 'react'

export default class SubscriptionList extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			planToConfirm: { slug: null }
		};
	}

	handleSelect(plan, confirm = false) {
		if (!confirm)
			return this.setState({ planToConfirm: Object.assign({}, plan) });
		this.setState({ planToConfirm: { slug: null }});
		return this.props.onSubscribe(plan);
	}

	render() {
		if (this.props.isFetching || this.props.isSaving)
			return <div className="text-center">Fetching available plans..</div>;

		return (
			<div className="list-group">
				{
					this.props.plans.map(plan => (
						<div key={ plan.slug } className={ 'list-group-item list-group-item-action d-flex justify-content-between align-items-center' + (this.props.isSaving ? ' disabled' : '') }>
							<div>
								<div>{ plan.name }</div>
								<div><span className="text-muted">{ plan.duration }</span></div>
							</div>
							{
								this.state.planToConfirm.slug === plan.slug ?
									(
										<div>
											<a href="#" className="btn btn-link px-0" onClick={ e => { e.preventDefault(); return this.setState({ planToConfirm: {slug: null} }) } }>Cancel</a>
											&emsp;
											<a href="#" className="btn btn-outline-secondary" onClick={ e => { e.preventDefault(); return this.handleSelect(plan, true) }}>Confirm</a> 
										</div>
										) : <a href="#" onClick={(e) => { e.preventDefault(); return this.handleSelect(plan); } }>Select <i className="fa fa-angle-right" /></a>
							}
						</div>
					))
				}
			</div>
			);
	}

}