import React from 'react'

export default class SearchBox extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			term: props.term
		};
	}

	handleCancel() {
		this.setState({ term: '' });
		return this.props.onCancel();
	}

	handleChange(e) {
		this.setState({ term: e.target.value });
	}

	handleSearch() {
		return this.props.onSearch(this.state.term);
	}

	render() {
		return (
			<div className="input-group">
				<input 
					placeholder="Search"
					className="form-control" 
					value={ this.state.term } 
					disabled={ this.props.disabled } 
					onChange={ this.handleChange.bind(this) }
					onKeyPress={ e => { if (e.key === 'Enter') return this.handleSearch(); } } />
				<span className="input-group-btn">
					<button type="button" className="btn btn-outline-secondary" onClick={ this.handleSearch.bind(this) }>
						<i className="fa fa-search fa-fw" />
					</button>
				</span>
				{
					this.props.isSearched ?
					 (
						<span className="input-group-btn">
							<button type="button" className="btn btn-outline-secondary" onClick={ this.handleCancel.bind(this) }>
								<i className="fa fa-remove fa-fw" />
							</button>
						</span>
					 	) : null
				}
			</div>
			);
	}

}