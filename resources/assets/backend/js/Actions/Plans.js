import PlansApi from '../Api/Plans'
import { PLANS_LIST_INVALIDATE, PLANS_LIST_POPULATE } from './Types'

export const loadList = (context = {type: 'init'}) => 
	(dispatch, getState) => {
		if (getState().plans.list.data.length && context.type !== 'reload')
			return;

		dispatch(invalidateList());
		PlansApi.get()
			.then(data => dispatch(populateList(data)))
			.catch(e => { throw(e) });
	}

	export const populateList = data => {
		return {
			type: PLANS_LIST_POPULATE,
			data
		}
	}

	export const invalidateList = () => {
		return {
			type: PLANS_LIST_INVALIDATE
		}
	}