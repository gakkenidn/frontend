import TopicsApi from '../Api/Topics'
import { TOPICS_LIST_INVALIDATE, TOPICS_LIST_POPULATE } from './Types'

export const loadList = (context = { type: 'init' }) =>
	(dispatch, getState) => {
		if (getState().topics.list.data.length && context.type !== 'reload')
			return;

		TopicsApi.get()
			.then(topics => dispatch(populateList(topics)))
			.catch(error => { console.log(error); });
	}

export const populateList = topics => {
	return {
		type: TOPICS_LIST_POPULATE,
		topics
	};
}