<nav class="navbar navbar-light navbar-fixed fixed-top bg-white shadowed navbar-expand-sm py-md-3 py-1">
	<div class="container d-flex justify-content-between mx-md-auto mx-0">
		<div class="d-flex align-items-md-center align-items-start">
			<div class="d-md-block d-flex align-items-center">
				<button class="navbar-toggler border-0 mr-2 pl-0" type="button" data-toggle="collapse" data-target="#navbar-collapsible-menu" style="outline: none;">
			    <b class="fa fa-bars fa-2x"></b>
			  </button>
				<a class="navbar-brand" href="{{ route('home') }}">
					<img src="{{ asset('images/GakkenIndonesia-Original.svg') }}" height="40" class="d-none d-md-block" />
					<img src="{{ asset('images/G-Original.svg') }}" height="40" class="d-md-none" />
				</a>
			</div>
		</div>

		<div class="d-flex align-items-center py-md-0 py-1">
			<div class="d-none d-md-block mr-3">
				<ul class="nav navbar-nav">
					@include('layouts.navbar-nav')
					@include('layouts.navbar-apps')
				</ul>
			</div>
			@include('layouts.navbar-account', ['ignoreInverse' => true])
		</div>
	</div>
	<div class="collapse w-100" id="navbar-collapsible-menu">
		<ul class="nav navbar-nav text-center py-4">
			@include('layouts.navbar-nav')
		</ul>
	</div>
</nav>