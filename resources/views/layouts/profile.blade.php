@extends('layouts.front')

@section('content')

	<section class="cover pt-lg-0 mb-4" style="background-image: url('{{ asset('images/profile-cover.jpg') }}');background-position: center center;">
		@include('layouts.navbar')

		<div class="container py-lg-5 pt-5 mt-lg-4 text-white">
			<div class="d-flex flex-lg-row flex-column justify-content-lg-start justify-content-center align-items-center mb-5">
				<img src={{ $user->avatar_url ?: asset('/images/user.svg') }} style="height: 100px; border-radius: 50%;" />
				<div class="ml-lg-4 text-lg-left text-center">
					<h1 class="display-4 mb-0">{{ $user->name }}</h1>
					<div class="h5">{{ $user->email }}</div>
				</div>
			</div>
		</div>
	</section>
	<div class="container pt-4">

		<div class="row">
			<div class="col-xl-3 col-lg-4 pr-lg-5">
				@include('profile.nav')
			</div>
			<div class="col-xl-9 col-lg-8">
				

				@if (Request::session()->has('alert-success'))
					<div class="alert alert-success mb-4">{{ Request::session()->get('alert-success') }}</div>
				@elseif (Request::session()->has('alert-danger'))
					<div class="alert alert-danger mb-4">{{ Request::session()->get('alert-danger') }}</div>
				@endif

				@yield('profile-content')
			</div>
		</div>
	</div>

@endsection