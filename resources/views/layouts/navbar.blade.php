<div class="container d-none d-lg-block">
	<nav class="navbar navbar-light navbar-main navbar-expand-md justify-content-between px-0">
		<div class="d-flex align-items-lg-center align-items-start">
			<a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('images/GakkenIndonesia-Original.svg') }}" height="40" /></a>
		</div>
		<div class="d-flex align-items-center">
			<ul class="nav navbar-nav mr-4">
				@include('layouts.navbar-nav')
				@include('layouts.navbar-apps')
			</ul>
			@include('layouts.navbar-account')
		</div>
	</nav>
</div>