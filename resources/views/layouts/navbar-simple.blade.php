<div class="container d-none d-lg-block">
	<nav class="navbar navbar-{{ isset($inverseNavbar) ? 'inverse' : 'light' }} navbar-main navbar-toggleable-md justify-content-between px-0">
		<div class="d-flex align-items-center">
			<a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('images/logo' . (isset($inverseNavbar) ? '-white' : '') . '.png') }}" /></a>
		</div>
		<div class="d-flex align-items-center">
			@include('layouts.navbar-account')
		</div>
	</nav>
</div>