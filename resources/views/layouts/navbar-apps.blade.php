
@if (auth()->check())
  @foreach(Request::user()->apps()->get() as $app)
    <li class="nav-item">
      <a class="nav-link h6 text-uppercase m-0" href="{{ $app->url }}"> {{ $app->name }} </a>
    </li>
  @endforeach
@endif