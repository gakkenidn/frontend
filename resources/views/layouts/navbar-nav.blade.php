<li class="nav-item{{ Request::segment(1) == 'p2kb' ? ' active' : '' }}">
	<a class="nav-link h6 text-uppercase m-0" href="{{ route('p2kb') }}">P2KB</a>
</li>
<li class="nav-item{{ Request::segment(1) == 'articles' ? ' active' : '' }}">
	<a class="nav-link h6 text-uppercase m-0" href="{{ route('articles') }}">Artikel</a>
</li>

{{-- <li class="nav-item dropdown">
	<a class="nav-link dropdown-toggle h6 text-uppercase m-0" data-toggle="dropdown" href="#!" role="button" aria-haspopup="true" aria-expanded="false">
		Media <b class="fa fa-angle-down"></b></a>
	<div class="dropdown-menu dropdown-menu-right shadowed">
		<a class="dropdown-item py-3 h6 text-uppercase" href="/infographics">Infografis</a>
		<a class="dropdown-item py-3 h6 text-uppercase d-none" href="/podcasts">Podcast</a>
	</div>
</li> --}}

<li class="nav-item dropdown">
	<a class="nav-link dropdown-toggle h6 text-uppercase m-0" data-toggle="dropdown" href="#">Referensi <b class="fa fa-angle-down"></b></a>
	<div class="dropdown-menu dropdown-menu-right shadowed">
		<a href="{{ route('journals') }}" class="dropdown-item py-3">
			<div class="h6 text-uppercase">Jurnal Premium</div>
			<div class="small text-muted">Koleksi 2000+ Jurnal Kesehatan Elsevier</div>
		</a>
		<a href="{{ route('drugs') }}" class="dropdown-item py-3">
			<div class="h6 text-uppercase">Indeks Obat</div>
			<div class="small text-muted">Database rincian dari 3000+ merk obat</div>
		</a>
		<a href="{{ route('jobs') }}" class="dropdown-item py-3">
			<div class="h6 text-uppercase">Loker Kesehatan</div>
			<div class="small text-muted">Informasi lowongan kerja di bidang kesehatan</div>
		</a>
	</div>
</li>
<li class="nav-item{{ Request::segment(1) == 'events' ? ' active' : '' }}">
	<a class="nav-link h6 text-uppercase m-0" href="{{ route('events') }}">Event</a>
</li>
<li class="nav-item{{ Request::segment(2) == 'genius' ? ' active' : '' }}">
	<a class="nav-link h6 text-uppercase m-0" href="{{ route('service', ['service' => 'genius']) }}">Kerjasama</a>
</li>