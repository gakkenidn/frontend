@extends('layouts.front')

@section('content')
	
	<section class="cover p-lg-0 pb-5" style="background-image: url('{{ asset('images/drugs-cover.jpg') }}');background-position: bottom;">
		@include('layouts.navbar')
		<div class="container py-3 text-xl-left text-center">
			<div class="row mt-5 justify-content-xl-start justify-content-center">
				<div class="col-xl-8 col-lg-10">
					<h1 class="display-4 d-none d-md-block">Rincian Obat Lengkap.</h1>
					<h1 class="d-md-none">Rincian Obat Lengkap.</h1>
					<p class="lead mb-5">
						Temukan rincian dari 3000 lebih merk obat yang beredar di
						Indonesia dengan cepat dan lengkap di Gakken Indonesia, kapan dan di mana saja.
					</p>
					<p>
						<a href="{{ route('subscribe') }}" class="btn btn-primary p-3 h6 text-uppercase">Berlangganan <b class="fa fa-angle-right"></b></a>
						<a href="#about" class="btn btn-outline-secondary p-3 h6 text-uppercase">Pelajari <span class="d-none d-md-inline">Lebih Lanjut</span> <b class="fa fa-angle-right"></b></a>
					</p>
				</div>
			</div>
		</div>
	</section>

	<section id="about">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-6 d-none d-lg-block pr-5 pb-md-0 pb-5">
					<img src="{{ asset('images/home-drugs.jpg') }}" class="w-100" />
				</div>
				<div class="col-lg-6 col-md-10">
					<div class="section-heading text-lg-left text-center">
						<h5>Rinci dan Akurat</h5>
					</div>
					<p class="lead">Database obat Gakken Indonesia memiliki lebih 
					dari 3000 merk obat dari 270 lebih produsen obat yang beredar di
					Indonesia.</p>
					<p>Sebagai dokter atau apoteker, Anda sering meresepkan pasien
					Anda berbagai obat, dan mungkin terkadang lupa, obat ini sudah
					benar belum, ya? Dan sebagai pasien, Anda mungkin sering
					bertanya-tanya, obat yang diresepkan dokter obat untuk apa
					saja, ya?</p>
					<p>Cari dan temukan rincian obat, beserta rincian
					dosis pemakaian, komposisi, dan harga serta packaging obat
					secara cepat dan tepat, kapan dan di mana saja.</p>
				</div>
			</div>
		</div>
	</section>

	<section class="bg-highlight py-5">
		<div class="container">
			<div class="row justify-content-center my-4">
				<div class="col-lg-9">
					<div class="p-5 bg-white rounded-lg shadowed">
						<div class="row align-items-center">
							<div class="col-lg-8 text-lg-left text-center mb-lg-0 mb-4">
								<div class="h2">Tunggu apa lagi?</div>
								<p class="lead mb-0">
									Dapatkan akses ke rincian obat-obatan dengan berlangganan
									Gakken P2KB atau P3KGB sekarang juga!
								</p>
							</div>
							<div class="col-lg-4">
								<a href="{{ route('subscribe') }}" class="btn btn-block btn-primary p-3 h6 text-uppercase">Berlangganan <b class="fa fa-angle-right"></b></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection