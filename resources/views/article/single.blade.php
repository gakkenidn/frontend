@extends('layouts.front')

@section('head')
	<meta property="fb:app_id"  content="{{ env('FACEBOOK_APP') }}" />
	<meta property="og:url"  content="{{ Request::fullUrl() }}" />
  <meta property="og:type" content="article" />
  <meta property="og:title" content="{{ $title or config('app.name') . ' - ' . $article->post_title }}" />
  <meta property="og:description" content="{{ substr(strip_tags($article->subheading->content), 0, 50) . '...' }}" />
  @if ($featuredImageThumbnail = $article->featuredImageSizeFile('medium_large'))
	  <meta property="og:image" content="https://gakken-idn.id/wp-content/uploads/{{ \Carbon\Carbon::parse($article->post_date)->format('Y/m') }}/{{ $featuredImageThumbnail }}" />
  @endif

  <style type="text/css">
  	.alignleft {
  		float: left;
  		margin-right: 30px;
  	}
  	.alignright {
  		float: right;
  		margin-left: 30px;
  	}
  	.alignnone {
  		display: block;
  		margin: 15px auto;
  	}
  </style>
@endsection

@section('content')
	
	<div class="container">
		<div class="py-md-5 py-3 d-md-block d-lg-none"></div>
		
		@include('layouts.navbar')

		<div class="row justify-content-center mt-5">
			<div class="col-lg-8">
				<div class="text-muted d-flex justify-content-between align-items-center flex-wrap">
					<div class="mb-2">
						<span class="h6 text-uppercase mr-2"><b class="fa fa-clock-o"></b> {{ \Carbon\Carbon::parse($article->post_date)->format('j M Y') }}</span>
						<span class="h6 text-uppercase"><b class="fa fa-pencil"></b> {{ $article->author->display_name }}</span>
					</div>
					<div class="mb-2 d-flex align-items-center">
						<div class="fb-share-button mr-1" data-href="{{ Request::fullUrl() }}" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::fullUrl() }}&src=sdkpreparse">Share</a></div>
						<a class="twitter-share-button" href="https://twitter.com/intent/tweet" data-size="large">Tweet</a>
					</div>
				</div>

				<hr />

				@if ($featuredImage = $article->featuredImageSizeFile('large'))
					<div class="rounded d-flex align-items-center mb-4" style="{{$article->categoryTaxonomies[0]->term->slug === 'infografik' ? '': 'max-height: 300px;'}} overflow: hidden;">
						<img src="https://gakken-idn.id/wp-content/uploads/{{ \Carbon\Carbon::parse($article->post_date)->format('Y/m') }}/{{ $featuredImage }}" class="w-100">
					</div>
				@endif

				<h1 class="mb-4">{{ $article->post_title }}</h1>

				<div class="article-content" style="word-break: break-word;">
					{{-- {!! clean($article->post_content, ['default', 'youtube']) !!} --}}
					{{-- {!! $article->post_content !!} --}}
					{!! clean($article->post_content) !!}
				</div>

				<h4>
					@foreach ($article->categoryTaxonomies as $cat)
						<a href="{{ route('article.category', ['slug' => $cat->term->slug]) }}" class="badge badge-default p-2 mb-1">{{ $cat->term->name }}</a>
					@endforeach
				</h4>
				
				<hr>
				
				<div class="text-muted d-flex justify-content-between align-items-center">
					<div>
						<span class="h6 text-uppercase mr-2"><b class="fa fa-clock-o"></b> {{ \Carbon\Carbon::parse($article->post_date)->format('j M Y') }}</span>
						<span class="h6 text-uppercase"><b class="fa fa-pencil"></b> {{ $article->author->display_name }}</span>
					</div>
					<div class="d-flex align-items-center">
						<div class="fb-share-button mr-1" data-href="{{ Request::fullUrl() }}" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ Request::fullUrl() }}&src=sdkpreparse">Share</a></div>
						<a class="twitter-share-button" href="https://twitter.com/intent/tweet" data-size="large">Tweet</a>
					</div>
				</div>

			</div>
		</div>

		<div class="d-block py-5">
                    <div class="related-articles py-5">
						<h6> Artikel Terkait </h6>
						<div class="row mx-0 px-0">
							@foreach ($relatedArticle as $index => $article)
                            <div class="col-md-3 p-2">
															@if(count($article->featuredImages))
                                <div class="d-inline-block img-cont related" style="max-height: 200px; height: 100%">
                                    <img src="{{$article->featuredImages[0]->url}}" class="d-block img-cover border-radius h-100 w-100">
																</div>
															@endif
                                <div class="d-block py-2">
                                    <a href="/articles/{{$article->post_name}}">{{ $article->post_title }}</a>
                                </div>
                            </div>
                        	@endforeach
						</div>
                    </div>
                </div>
		</div>

@endsection

@section('scripts')
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=831080503703720";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		window.twttr = (function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0],
		    t = window.twttr || {};
		  if (d.getElementById(id)) return t;
		  js = d.createElement(s);
		  js.id = id;
		  js.src = "https://platform.twitter.com/widgets.js";
		  fjs.parentNode.insertBefore(js, fjs);
		  t._e = [];
		  t.ready = function(f) {
		    t._e.push(f);
		  };
		  return t;
		}(document, "script", "twitter-wjs"));
	</script>
@endsection