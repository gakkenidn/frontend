@extends('layouts.front')

@section('content')
	
	@include('layouts.navbar')

	<div class="container mt-4">
		<div class="py-4 d-lg-none"></div>

		<div class="small text-muted text-uppercase">Kategori Artikel</div>
		<h1>{{ $category->name }}</h1>

		<div class="row mt-md-5 mb-5 mt-0">
			<div class="col-lg-8">
				<div class="article-list-group">
					@include('article.list')
				</div>

				{!! $articles->links() !!}

			</div>

			<div class="col-lg-4">

				<div class="mb-5 d-none d-lg-block">
					<div class="small text-muted text-uppercase">{{ \Carbon\Carbon::now()->format('l, j M Y') }}</div>
				</div>
				<div class="mb-5">
					@include('article.category-widget')
				</div>
				<div class="mb-5">
					@include('article.popular-widget')
				</div>

			</div>
		</div>
	</div>

@endsection

@section('scripts')
	<script type="text/javascript">
		$(document).ready(function () {
			$("[data-toggle=categories]").on('click', function (e) {
				e.preventDefault();
				$(".categories-list li").removeClass('d-none');
				$(this).remove();
			});
		});
	</script>
@endsection