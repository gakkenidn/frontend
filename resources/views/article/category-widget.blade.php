<div>
	<div class="h6 text-uppercase">Kategori Populer</div>
	<hr />
	<ul class="list-unstyled categories-list">
		@foreach ($categories as $index => $cat)
			<li class="{{ $index >= 5 ? "d-none" : '' }}">
				<a href="{{ route('article.category', ['slug' => $cat->term->slug]) }}">{{ $cat->term->name }}</a>
				<span class="ml-2 text-muted small">{{ $cat->count }}</span>
			</li>
		@endforeach
	</ul>
	@if ($categories->count() > 5)
		<a class="mt-2 d-block small text-muted" data-toggle="categories" href="#">Kategori lainnya <b class="fa fa-angle-down"></b></a>
	@endif
</div>