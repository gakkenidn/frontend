@extends('layouts.front')

@section('content')

	<section class="cover pt-0" style="background-image: url('{{ asset('images/pit-pogi-cover.jpg') }}')">

		@include('layouts.navbar', ['inverseNavbar' => true])
		<div class="container pt-lg-0 py-5 text-white">

			<div class="row pt-5 mt-5 justify-content-center">
				<div class="col-md-9 mt-lg-0 mt-5">
					<h1 class="d-none d-lg-block">Makassar Allergy Immunology Network (MARINE) Symposium and Workshop 2018</h1>
					<p class="lead text-lg-left text-center">
						Microbiome and Immunity: Potential Impact On Health and Disease
					</p>
				</div>
				<div class="col-lg-3 d-none d-lg-block text-right">
					<img src="{{ asset('images/marine.png') }}" class="w-50" alt="Logo Marine" />
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row">
				@foreach ($videos as $video)
					<div class="col-lg-4 col-md-6 mb-5">
						<div class="mb-3">
							<a href="{{ route('marine.video', ['slug' => $video->name]) }}" title="Nonton video">
								<img src="{{ $video->poster }}" class="mr-3 rounded mb-lg-0 mb-3 w-100" style="border: #eee thin solid;" />
							</a>
						</div>
						<div>
							<div href="#" class="h4 m-0 text-truncate">{{ $video->title }}</div>
							<div class="small text-truncate">{{ $video->lecturer }}</div>
							<div class="small text-truncate">Clarion, {{ \Carbon\Carbon::parse($video->date)->format('j M Y') }}</div>
							<div class="mt-3 row no-gutters">
								<div class="col-6 pr-1">
									<a class="btn btn-block btn-outline-primary" href="{{ route('marine.video', ['slug' => $video->name]) }}"><i class="fa fa-play"></i> Nonton Video</a>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</section>

	<section>
		<div class="container d-flex justify-content-center">
			<a href="http://www.idionline.org/" class="mr-md-5 mr-2">
				<img src="{{ asset('images/idi-logo.svg') }}" height="80" alt="Ikatan Dokter Indonesia" />
			</a>
			<a href="http://unhas.ac.id/" class="mr-md-5 mr-2">
				<img src="{{ asset('images/unhas-logo.svg') }}" height="80" alt="Universitas Hasanuddin" />
			</a>
			<a href="#">
				<img src="{{ asset('images/marine.png') }}" height="80" alt="Marine Symposium" />
			</a>
		</div>
	</section>

@endsection
