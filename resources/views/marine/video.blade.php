@extends('layouts.front')

@section('content')

	<div class="shadowed" style="z-index: 1030;">
		@include('layouts.navbar')
	</div>
	<section class="py-0 pt-lg-0 pt-5 bg-inverse">
		<div class="container px-md-3 px-0">
			<div id="player-container">
				<script type="text/javascript" src="//content.jwplatform.com/players/{{ $video->videoId }}-qJOsNWsv.js"></script>
			</div>
		</div>
	</section>
	<section class="pt-5">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<h1>{{ $video->title }}</h1>
					<p class="lead">{{ $video->lecturer }}</p>
					<p>
						<span><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($video->date)->format('j M Y') }}</span>
					</p>
					<div class="mb-4">
						{!! clean($video->description) !!}
					</div>
					<p class="mt-5"><a href="{{ route('marine') }}"><i class="fa fa-angle-left"></i> Kembali ke daftar materi</a></p>
				</div>
				<div class="col-lg-4 col-md-6 pt-lg-0 pt-5 d-none d-md-block">
					<img src="{{ asset('images/marine.png') }}" class="w-50">
				</div>
			</div>
		</div>
	</section>

@endsection
