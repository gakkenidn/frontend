@extends('new.layouts.front')

@section('content')

    @include('layouts.navbar')

    <section id="job-single" class="container-single overlay-relative">
        <div class="wrapper padding-lr1 padding-tb5-md-lg lg-container">
            <div class="page-nav padding-tb1">
                <a href="/events" class="d-block"> <i class="fa fa-angle-left"> </i> Back to List </a>
            </div>
            <div class="row no-gutters">
                @if($event->banner)
                    <div class="col-12 event-pic radius-normal">
                        <img src="{{ env('VAULT_URL') . '/event/files/' . $event->banner}}" />
                    </div>
                @endif

                <div class="col-12 col-lg-8">
                    <div class="box-wrap  padding-1">
                        <div class="row no-gutters">
                            <div class="col padding-half">
                                <p class="text-emphasis"> Events </p>
                                <h4> {{ $event->name }} </h6>
                            </div>
                        </div>

                        <div class="border-bottom">
                            <div class="event-desc">
                                <h6> Lokasi </h6>
                                <div class="padding-b1">
                                    <p> {{ $event->location }}
                                        <a href="#" class="d-inline-block">
                                            <i class="fa fa-map-marker"> </i> Lihat Peta
                                        </a>
                                    </p>
                                    <p>
                                        {{ \Carbon\Carbon::parse($event->start_at)->format('l') . ' - ' . \Carbon\Carbon::parse($event->end_at)->format('l')}},
                                        {{ \Carbon\Carbon::parse($event->start_at)->format('j F Y') . ' - ' . \Carbon\Carbon::parse($event->end_at)->format('j F Y') }}
                                    </p>
                                </div>
                            </div>

                            <div class="event-desc">
                                <h6> More Info </h6>
                                <div class="padding-b1">
                                    <p>
                                        <a href="{{ $event->website }}" class="d-inline-block">
                                            {{ $event->website }}
                                        </a>
                                    </p>
                                </div>
                            </div>

                            <div class="event-desc">
                                <h6> Hubungi Penyelenggara </h6>
                                {{-- <div class="padding-b1">
                                    <p> Departemen Bedah Kedokteran Universitas Hasanuddin </p>
                                    <p> Jl. Perintis Kemerdekaan</p>
                                </div> --}}

                                <div class="padding-b1">
                                    <p> PIC : {{ $event->phone_name }} ({{ $event->phone_number }}) </p>
                                </div>
                            </div>
                        </div>



                        <div class="row no-gutters desc">
                            <div class="col-12 padding-tb1 event-desc-container">
                                {!! $event->desc !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="padding-lr1 padding-tb1">
                        <h6> Event lainnya </h6>
                        @foreach ($upcomingEvents as $index => $data)
                            @if($index == 0)
                                <div class="event-related border-bottom padding-tb1">
                                    <a href="/event/{{ $data->slug}}" class="d-block">
                                        <h6 class="mb-0"> {{ $data->name}} </h6>
                                    </a>
                                    <p>
                                        <!-- {{ \Carbon\Carbon::parse($data->start_at)->format('l') . ' - ' . \Carbon\Carbon::parse($data->end_at)->format('l')}}, -->
                                        {{ \Carbon\Carbon::parse($data->start_at)->format('j F Y') . ' - ' . \Carbon\Carbon::parse($data->end_at)->format('j F Y') }}
                                    </p>
                                </div>
                            @else
                                @if($index == 1)
                                    <ul class="event-related row no-gutters">
                                @endif
                                <li class="col-12 col-lg-12 col-md-6 padding-r1-sm">
                                    <a href="/event/{{ $data->slug}}" class="d-block">
                                        <h6 class="mb-0"> {{ $data->name }} </h6>
                                    </a>
                                    <p>
                                        <!-- {{ \Carbon\Carbon::parse($data->start_at)->format('l') . ' - ' . \Carbon\Carbon::parse($data->end_at)->format('l')}}, -->
                                        {{ \Carbon\Carbon::parse($data->start_at)->format('j F Y') . ' - ' . \Carbon\Carbon::parse($data->end_at)->format('j F Y') }}
                                    </p>
                                </li>
                                @if((count($upcomingEvents) -1) === $index)
                                    </ul>
                                @endif
                            @endif

                        @endforeach
                        <a href="/events" class="text-center d-block"> Lihat semuanya </a>
                    </div>
                </div>
            </div>
        </div>
        <img src="/images/event-bg.jpg" class="overlay-bg" />
    </section>
@endsection
