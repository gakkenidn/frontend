@extends('new.layouts.front')

@section('content')

    @include('layouts.navbar')

    <section class="container pb-5">
        <div class="py-5">
            <div class="page-nav padding-tb1">
                <a href="/" class="d-block"> <i class="fa fa-angle-left"> </i> Back to Home </a>
            </div>
            <div class="">
                <div class="row">
                    <div class="col-md-5">
                        <img src="/images/convention.png" class="w-100">
                    </div>
                    <div class="col-md-7">
                        <h2>Events</h2>
                        <ul class="list-unstyled">
                            @foreach($events as $event)
                                <li class="py-2">
                                    <a href="/event/{{ $event->slug }}" class="d-block">
                                        <h5 class="mb-0"> {{ $event->name }} </h5>
                                    </a>
                                    <p class="mb-0">  <span class="location"> {{ $event->location }} </span> </p>
                                    <p>
                                        <span class="time" style="font-weight: 700">
                                            {{ \Carbon\Carbon::parse($event->start_at)->format('l') . ' - ' . \Carbon\Carbon::parse($event->end_at)->format('l')}},
                                            {{ \Carbon\Carbon::parse($event->start_at)->format('j F Y') . ' - ' . \Carbon\Carbon::parse($event->end_at)->format('j F Y') }}
                                        </span>
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        {{-- <img src="{{ asset('event-bg.jpg') }}" class="overlay-bg" /> --}}
    </section>
@endsection
