@extends('new.layouts.front')

@section('content')
    @include('layouts.navbar')

    <div class="container container-fluid border-top py-4">
        <div class="row mx-0 py-5 px-4">
            <div class="d-inline-block">
                <img src="/images/logo-paramount.png"  height="50px" />
            </div>
        </div>

        <div class="top-banner paramount-top-banner">   
            <div class="d-flex container-fluid container justify-content-start h-100 align-items-center">
                <div class="col-12 col-lg-5  col-md-7 d-flex flex-column">
                    <div class="px-md-4 py-5">
                        <p class="strong text-paramount h5 text-uppercase"> About Paramount Bed </p>                 
                    </div>      
                </div>
            </div>
                
            <div class="p-absolute top back w-100">
                <img src="/images/paramount-large-banner.png" class="w-100 d-block img-cover h-100" />
            </div>
           
        </div>

        <div class="row mx-0 px-md-4 py-md-5">
            <div class="col-md-6 p-4 order-2 order-md-1">
                <p> <strong class="text-paramount"> Paramount Bed  </strong> is founded in 1947 in Japan and pioneer of medical bed manufacturing. Our scope of business had also expanded to include welfare equipment for elderly and home-nursing care services. These initiatives are in line with our goal of creating healthcare environments with a human touch. </p>
            </div>
            <div class="col-md-6 p-4 order-1 order-md-2">
                <img src="/images/paramount-hq.png" class="w-100 d-block img-cover rounded" />
            </div>
        </div>

        <div class="row mx-0 px-md-4 py-md-5">

             <div class="col-md-6 p-4">
                <img src="/images/paramount-indonesia.png" class="w-100 d-block img-cover rounded" />
            </div>

            <div class="col-md-6 p-4">
                <p> <strong class="text-paramount"> PT. Paramount Bed Indonesia </strong> is founded in 1995 as a subsidiary of Paramount Bed Co., Ltd which has 69 years of experience in its field and has a 70% market share in Japan in the field of beds for hospitals. Our company produces hospital beds for domestic and foreign (export) markets since December 1996 in West Cikarang, Bekasi. Apart from Japan and Indonesia, other subsidiaries also exist in Singapore, China, Vietnam, Thailand, India, Dubai, Mexico and Brazil. </p>      
            </div>
           
        </div>

        <div class="row mx-0 p-md-4">
            <div class="p-4">   
                <a href="https://www.paramount.co.jp/english/contact/inquiry" target="_blank" class="border-box px-4 py-2 text-black-50 rounded"> 
                    <i class="fa fa-envelope"> </i> Contact Paramount Bed
                </a> 
            </div>     
        </div>
    
    </div>

    <div class="d-block paramount-bg">
        <div class="h5 strong white-text p-4 text-center"> Know More About Us </div>
    </div>

    <div class="d-block py-4 border-bottom-2 paramount-border">
        <div class="container container-fluid">
            <div class="row mx-0 align-items-center justify-content-center py-5">
            
            <div class="col-md-6 col-12">
                <div class="videoWrapper">
                    <!-- Copy & Pasted from YouTube -->
                    {{-- <iframe 
                        width="560" 
                        height="315" 
                        src="https://www.youtube.com/embed/_nq_3aCBErA#t=1m47s" 
                        frameborder="0" 
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
                        allowfullscreen
                    ></iframe> --}}
                    <div id="player" style="width: 560px; height: 315px;"></div>
                    <div id="image-player" style="
                        width:560px; 
                        height:315px;
                        background-color:#333;
                        display: block;
                        position: absolute;
                        top: 0;
                        cursor:pointer;
                    ">
                        <img src="/images/paramountbed-yt.png" class="yt-thumbnail" style="width:560px; height:315px;"/>
                        <img src="/images/youtube-btn.png" style="
                                position: absolute;
                                margin: auto;
                                left: 0;
                                right: 0;
                                width: 260px;
                                top: 60px;
                        "/>
                    </div>
                    <style>
                        #image-player:hover > .yt-thumbnail  {
                            opacity: 0.7
                        }
                    </style>
                    <script>
                        // 2. This code loads the IFrame Player API code asynchronously.
                        var tag = document.createElement('script');
                        tag.src = "https://www.youtube.com/iframe_api";
                        var firstScriptTag = document.getElementsByTagName('script')[0];
                        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                    
                        var player;
                        const imagePlayer = document.getElementById("image-player");

                        function onYouTubeIframeAPIReady() {
                            player = new YT.Player('player', {
                                height: '315',
                                width: '560',
                                videoId: '_nq_3aCBErA',
                                events: {
                                    'onReady': onPlayerReady,
                                    'onStateChange': onPlayerStateChange
                                } ,
                                playerVars: {
                                    start: 151
                                }
                            });
                        }
                    
                        function onPlayerReady(event) {
                            // event.target.playVideo();
                            imagePlayer.addEventListener('click', () => {
                                player.playVideo();
                            });
                        }

                        var playing = false;
                        function onPlayerStateChange(event) {
                            if(!playing){
                                imagePlayer.style.opacity = 0;
                                playing = true;
                            }
                        }
                        function stopVideo() {
                            player.stopVideo();
                        }
                        </script>
                </div> 
                <div class="d-block">
                    <img src="/images/catch-asia.jpg"  height="70px" class="mx-2 my-4" />
                    <img src="/images/logo-paramount.png"  height="50px" class="mx-2 my-4" />
                </div>
            </div>

            <div class="col-md-6 col-12 text-center"> 
                <a href="https://youtu.be/u0bJDTWIYDY" target="_blank" class="h3 text-center text-paramount text-underline" style="font-weight: 700"> Catch Asia Media Network </a>
            </div>
            </div>
        </div>
    </div>

    <div class="d-block py-4 border-bottom-2 paramount-border">
        <div class="container container-fluid">
            <div class="row mx-0 align-items-center py-5"> 
                <div class="col-md-6">
                    <div class="h3 text-center text-paramount text-underline" style="font-weight: 700"> <a href="/files/Rajiv-Gandhi-Centre-F.pdf" target="_blank" class="text-paramount"> User's Voice </a> </div>
                </div>
                <div class="col-md-6">
                    <img src="/images/users-voice.jpg" class="w-100 d-block img-cover rounded" />
                </div>
            </div>

        </div>
    </div>

    <div class="container container-fluid py-4"> 
        <div class="row mx-0 p-md-4">
            <div class="p-4">   
                <a href="https://www.paramount.co.jp/english/contact/inquiry" target="_blank" class="border-box px-4 py-2 text-black-50 rounded"> 
                    <i class="fa fa-envelope"> </i> Contact Paramount Bed
                </a> 
            </div>     
        </div>
    </div>

@endsection