@extends('new.layouts.front')

@section('head')
    <script 
        type="text/javascript"
        src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="{{ env('MIDTRANS_CLIENT') }}">
    </script>
@endsection

@section('content')
    @include('layouts.navbar')

     <div class="top-banner paramount-top-banner my-5 my-md-0">                  
            <div class="p-relative w-100 mt-3 mt-md-0">
                <img src="/images/paramount-large-banner.png" class="w-100 d-block img-cover h-100" />
            </div>       
    </div>

    <div class="container container-fluid">
        <div class="row mx-0 px-md-4 py-5 border-bottom">
            <div class="col-md-6 p-4 order-2 order-md-1">
                <h5 class="strong"> Subscribe and get access for Prodia Premium Content </h5> 
                <p> Cras quis nulla commodo, aliquam lectus sed, blandit augue. Cras ullamcorper bibendum bibendum. Duis tincidunt urna non pretium porta. Nam condimentum vitae ligula vel ornare. Phasellus at semper turpis. Nunc eu tellus tortor. Etiam at condimentum nisl, vitae sagittis orci. Donec id dignissim nunc. Donec elit ante, eleifend a dolor et, venenatis facilisis dolor. In feugiat orci odio, sed lacinia sem elementum quis.  </p>
                <div class="d-block justify-content-center align-items-center">
                    @if(auth()->check())
                        <button class="btn btn-primary shadowed" id="subscribe-btn"> 
                            <i class="fa fa-lock"> </i> Subscribe to get Premium Access 
                        </button>
                    @else
                        <button class="btn btn-primary shadowed" id="subscribe-btn" onclick="javascript:location.href='/login?redirect=/prodia'"> 
                            <i class="fa fa-lock"> </i> Login to subscribe Premius Access 
                        </button>
                    @endif
                </div>
            </div>
        </div>

        <div class="row border-bottom my-5">  
            @foreach ($premiumTopic as $topic)
                <div class="col-md-4 mb-5">
                    <div>
                        <a href="{{ route('p2kb.single', ['slug' => $topic->topic->slug])}}">
                            <div class="d-inline-block premium-badge"> <i class="fa fa-lock"> </i> Premium Content </div>
                            <img src="{{ env("VAULT_URL") . "/topic/files/". $topic->topic->featured_image }}" class="w-100 mb-3">
                        </a>
    
                        <a href="{{ route('p2kb.single', ['slug' => $topic->slug])}}">
                            <div class="text-dark d-flex justify-content-between align-items-center mb-2">
                                <h6 class="text-truncate m-0"> {{ $topic->topic->title }} </h6>
                            </div>
                        </a>
                        <div class="ellipsis-3">
                            {!! $topic->topic->summary  !!}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div style="background: rgba(0,0,0,0.05)">
        <div class="container container-fluid border-top">
            <div class="row mx-0 px-md-4 py-5">
                <div class="col-md-6 p-4 order-2 order-md-1">
                    <h5 class="strong"> Free to Access just for you </h5> 
                    <p> Cras quis nulla commodo, aliquam lectus sed, blandit augue. Cras ullamcorper bibendum bibendum. Duis tincidunt urna non pretium porta. Nam condimentum vitae ligula vel ornare. Phasellus at semper turpis. Nunc eu tellus tortor. Etiam at condimentum nisl, vitae sagittis orci. Donec id dignissim nunc. Donec elit ante, eleifend a dolor et, venenatis facilisis dolor. In feugiat orci odio, sed lacinia sem elementum quis.  </p>
                    <div class="d-block justify-content-center align-items-center">
                        <button class="btn btn-primary shadowed"> <i class="fa fa-lock"> </i> Subscribe to get Premium Access </button>
                    </div>
                </div>
            </div>

            <div class="row my-5">  
                @foreach ($freeTopic as $topic)
                    <div class="col-md-4 mb-5">
                        <div>
                            <a href="{{ route('p2kb.single', ['slug' => $topic->slug])}}">
                                <img src="{{ env("VAULT_URL") . "/topic/files/". $topic->featured_image }}" class="w-100 mb-3">
                            </a>
    
                            <a href="{{ route('p2kb.single', ['slug' => $topic->slug])}}">
                                <div class="text-dark d-flex justify-content-between align-items-center mb-2">
                                    <h6 class="text-truncate m-0">{{ $topic->title }}</h6>
                                </div>
                            </a>
                            <div class="ellipsis-3">
                                {!! $topic->summary !!}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <form id="payment-form" method="post" action="{{ route('pay.done') }}" class="hidden-xl-down" accept-charset="utf-8">
        <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
        <input type="hidden" name="payment_method" id="payment-method" />
        <input type="hidden" name="result_type" id="result-type" />
        <input type="hidden" name="result_data" id="result-data" />
    </form>

    @if(auth()->check())
        <script type="text/javascript">
            const subscribeButton = document.getElementById('subscribe-btn');

            const submitPayment = (type, data) => {
                subscribeButton.disabled = false;
                $("#payment-form #result-type").val(type);
                $("#payment-form #payment-method").val('midtrans');
                $("#payment-form #result-data").val(JSON.stringify(data));
                $("#payment-form").submit();
            }

            subscribeButton.addEventListener('click', (e) => {
                const xhr = new XMLHttpRequest();
                const formData = new FormData();

                subscribeButton.disabled = true;

                formData.append('type', 'subscription');
                formData.append('subscription', parseInt('{{ $subscription->id }}'));
                formData.append('method', 'mt-cc');

                xhr.open("POST", "{{ route('mt.snaptoken') }}", true);
                xhr.setRequestHeader("X-CSRF-TOKEN", "{{ csrf_token() }}");
                xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                xhr.onreadystatechange = function() {
                    if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                        snap.pay(xhr.response,  {
                            onSuccess: result => submitPayment('success', result),
                            onPending: result => submitPayment('pending', result),
                            onError: result => submitPayment('error', result)
                        });
                    }
                }
                xhr.send(formData);
            });
        </script>
    @endif

@endsection