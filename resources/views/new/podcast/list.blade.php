@extends('new.layouts.front')

@section('content')
   @include('layouts.navbar')

   <div style="background-color: #F2F5F7;">
        <div class="container container-fluid">
            <div class="row mx-0 px-0 border-bottom py-4"> 
                <div class="col-md-6 py-4"> 
                    <div class="d-block text-uppercase py-4 text-strong h6 color-red"> Podcast Terbaru </div>
                    <div class="h5">  
                        <a 
                            href="/articles/#" 
                            class="text-black text-strong h5 d-block"> 
                            Manajemen Rumah Sakit
                        </a>
                       
                    </div>
                    <p> Bincang-bincang bersama Prof. Handre  <p>
                    <p class="small text-small text-secondary"> Durasi 30 menit </p>
                    <button class="py-2 px-4 rounded shadowed d-inline-block btn btn-primary"> <i class="fa fa-play-circle"> </i>  Play </button>
                </div>
                <div class="col-md-6 justify-content-center align-items-center d-flex p-4"> 
                    <a 
                        href="/articles/#" 
                        class="text-black text-strong h5 d-block" style="width: 200px; height: 200px; "> 
                        <div style="display: block;
                            position: relative;
                            height: 100%;
                            width: 100%;
                            background-image: url('/images/paramount-bed-banner.png');
                            background-size: cover;
                            background-position: center center;
                            background-repeat: no-repeat; border-radius: 50%;" class="shadowed">  
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div> 

   <section>
        <div class="container py-5 mb-5">
          <div class="d-block"> 
                <div class="d-block px-4 pb-2 opacity-half strong"> Podcast Lainnya </div>
                <div class="row mx-0 px-0"> 
                    <div class="col-md-3 p-md-2 p-4"> 
                        <div class="rounded shadowed text-center py-4">
                           <div class="d-flex flex-column align-items-center justify-content-center p-4">
                              <div class="d-inline-block"> <img src="/images/paramount-bed-banner.png" width="150px" height="150px" style="border-radius: 50%; object-fit: cover"/> </div>
                           </div>
                           <div class="h6 mb-0"> Test Lorem </div>
                           <p class="small text-small text-secondary d-block"> Durasi 30 menit </p>
                           <button class="py-2 px-4 rounded shadowed d-inline-block btn"> <i class="fa fa-play-circle"> </i>  Play </button>
                        </div>
                    </div>  
                </div>
            </div> 
        </div>
   </section>

@endsection
