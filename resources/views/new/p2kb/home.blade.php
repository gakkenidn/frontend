@extends('layouts.front')

@section('content')

  <section class="cover p-0" style="background-image: url('{{ asset('images/p2kb-cover.jpg') }}');background-position: bottom;">
    @include('layouts.navbar')

    <div class="container">

      <div class="d-lg-none py-5"></div>

      <div class="row my-5">
        <div class="col-xl-6">
          <h1 class="display-4">Gakken P2KB</h1>
          <br />
          <p style="line-height: 1.6em;margin-bottom: 14px;">
            Sebagai salah satu syarat pembaruan SIP, dokter kini diwajibkan untuk memperoleh poin SKP dari berbagai kegiatan seminar, pelatihan, simposium, dan lain-lain. Namun, padatnya jadwal praktek dan kegiatan lainnya menyebabkan banyak dokter tidak memiliki waktu untuk itu.
          </p>
          <p style="line-height: 1.6em;">
            Gakken Indonesia dengan layanan Gakken P2KB hadir untuk mempermudah pada dokter untuk dapat memperoleh poin SKP, kapan dan dimana saja, secara nyaman dan fleksibel.
          </p>
        </div>
      </div>

    </div>
  </section>
  <div class="container">
    <div id="catalog-app"></div>
  </div>

@endsection

@section('scripts')
  <script>
    window.CatalogApp = {
      baseUrl: '{{ url('/') }}'
    }
  </script>
  <script src="{{ mix('js/CatalogApp.js') }}"></script>
@endsection

