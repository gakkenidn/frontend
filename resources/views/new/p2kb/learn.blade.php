<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Gakken Indonesia | P2KB: {{ $userTopic['title'] }}</title>
  <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">

	<link rel="icon" href="{{ asset('images/icon.png') }}">
	<link rel="icon" sizes="48x48" href="{{ asset('images/icon@0,25x.png') }}">
	<link rel="icon" sizes="96x96" href="{{ asset('images/icon@0,5x.png') }}">
	<link rel="icon" sizes="144x144" href="{{ asset('images/icon@0,75x.png') }}">

	<link rel="apple-touch-icon" href="{{ asset('images/icon.png') }}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/touch-icon-ipad.png') }}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('images/touch-icon-iphone-retina.png') }}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('images/touch-icon-ipad-retina.png') }}">

	<meta name="msapplication-square70x70logo" content="{{ asset('images/icon_smalltile.png') }}">
	<meta name="msapplication-square150x150logo" content="{{ asset('images/icon_mediumtile.png') }}">

	<meta name="theme-color" content="#a4151a">
	<script type="text/javascript">
		window.App = {
			url: {
				base: '{{ route('home') }}',
				rest: '{{ url('/app/rest') }}',
				logout: '{{ route('logout') }}',
			},
			user: {
				id: '{{ auth()->user()->id }}',
				email: '{{ auth()->user()->email }}',
				name: '{{ auth()->user()->name }}',
				profession: '{{ auth()->user()->profession }}'
			},
			analytics: {
				active: {{ config('app.env') == 'production' && env('GOOGLE_ANALYTICS_ID', false) ? 'true' : 'false' }},
				trackingId: '{{ env('GOOGLE_ANALYTICS_ID', '') }}'
      },
      topic: {!! json_encode($userTopic) !!},
      remainingQuota: {{ $remainingQuota }}
		};
	</script>
</head>
<body>

  <div id="learn-app">
    <div class="d-flex flex-column justify-content-center align-items-center" style="height: 500px;">
      <img src="{{ asset('/images/loading.svg') }}" />
      <div class="lead">Memuat aplikasi...</div>
    </div>
	</div>

	<form id="payment-form" method="post" action="{{ route('pay.done') }}" class="hidden-xl-down" accept-charset="utf-8">
		<input type="hidden" name="_token" value="{!! csrf_token() !!}" />
		<input type="hidden" name="payment_method" id="payment-method" />
		<input type="hidden" name="result_type" id="result-type" />
		<input type="hidden" name="result_data" id="result-data" />
	</form>

	<script type="text/javascript">
		window.SubscribeApp = {
			userEmail: "{{ auth()->user()->email }}",
			userName: "{{ auth()->user()->name }}",
			snapTokenUrl: "{{ route('mt.snaptoken') }}",
		};
	</script>
	<script type="text/javascript" src="https://{{ env('MIDTRANS_PRODUCTION', false) ? 'app.midtrans.com' : 'app.sandbox.midtrans.com' }}/snap/snap.js" data-client-key="{{ env('MIDTRANS_CLIENT') }}"></script>

  <script type="text/javascript" src="{{ mix('js/LearnApp.js') }}"></script>
	@if (env('APP_ENV') == 'production')
		<script type="text/javascript">
			window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set._.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");$.src="https://v2.zopim.com/?4skLxMDjkpWOEU3uSrAbWoid3b68AOuN";z.t=+new Date;$.type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script>
	@endif

	@if (env('GOOGLE_ANALYTICS_ID', false))
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', '{{ env('GOOGLE_ANALYTICS_ID') }}', 'auto');
		  @if (auth()->check())
		  	ga('set', 'userId', {{ auth()->user()->id }});
		  @endif
		  ga('send', 'pageview');

		</script>
	@endif

</body>
</html>