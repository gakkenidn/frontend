@extends('layouts.front')

@section('content')

  @include('layouts.navbar')

  @if($topic->sponsorContent)
    <div class="top-banner banner-bg banner-after paramount-bed">
        <div class="d-flex container-fluid container justify-content-end">
            <div class="col-12 col-lg-5  col-md-7 d-flex flex-column">
                <div class="white-bg shadowed rounded p-4">
                    <p class="text-secondary"> Sponsored by</p>
                    <div class="text-center">
                        <div class="mr-auto py-4">
                          <img src="/images/logo-paramount.png"  height="50px" />
                        </div>
                        {!! $topic->sponsorContent()->first()->sponsor->desc !!}   
                    </div>
                    <div class="d-block text-center py-4">
                      <a href="/paramountbed" class="btn btn-paramount mt-4"> Learn More </a>
                    </div>                           
                </div>      
            </div>
        </div>
    </div>
  @endif

  <div class="container pb-5">
    
    <div class="my-5 py-5 d-flex flex-column align-items-center">
      <div class="text-muted mb-4 text-center">Topik Gakken P2KB</div>
      <h1 class="d-none d-md-block display-4 text-center">{{ $topic->title }}</h1>
      <h1 class="d-md-none h3 text-center">{{ $topic->title }}</h1>
      {{-- <div class="text-center">Topik ini disponsori oleh</div> --}}

      <div class="mt-5">
        <a href="{{ route('p2kb.learn', ['slug' => $topic->slug]) }}" class="btn btn-primary btn-lg">Mulai Belajar</a>
      </div>
    </div>

    <div class="d-md-flex justify-content-center mb-5">
      <div class="mb-3">
        <div class="text-muted">Sertifikasi</div>
        <div class="d-none d-md-block h3 m-0">Jumlah SKP: 2</div>
        <div class="d-md-none h5 m-0">Jumlah SKP: 2</div>
        <div class="text-muted">{{ $topic->category === 'doctor' ? "IDI" : "PDGI" }} Wilayah Sulselbar</div>
      </div>
      <div class="mb-3 mx-md-5">
        <div class="text-muted">Standar Kelulusan</div>
        <div class="d-none d-md-block h3 m-0">80%</div>
        <div class="d-md-none h5 m-0">80%</div>
      </div>
      <div class="mb-3">
        <div class="text-muted">Pengajar</div>
        <div class="d-none d-md-block h3 m-0">{{ $topic->lecturers[0]->front_title . ' ' . $topic->lecturers[0]->name . ', ' . $topic->lecturers[0]->back_title }}</div>
        <div class="d-md-none h5 m-0">{{ $topic->lecturers[0]->front_title . ' ' . $topic->lecturers[0]->name . ', ' . $topic->lecturers[0]->back_title }}</div>
      </div>
    </div>

    <div class="row justify-content-center">
      <div class="col-xl-9">
        <div class="mb-5">
          {!! clean($topic->summary) !!}
        </div>

        <div class="shadowed">
          <div class="bg-light px-4 py-3">
            <h6 class="m-0">Materi Pembelajaran</h6>
          </div>
          @foreach($topic->contents()->orderBy('order')->get() as $content)
            @if ($content->appearance !== 'download')
              <div class="px-4 py-4 border-bottom d-flex justify-content-start align-items-start">

                <h5 class="mr-4 text-muted"><i class="fa fa-{{ $content->type }}"></i></h5>
                <div>
                  <h5>{{ $content->label }}</h5>
                  <div>
                    @if(str_contains(strtolower($content->label), ['rangkuman', 'summary']))
                      Rangkuman singkat tentang {{ $topic->title }} secara umum.
                    @elseif(str_contains(strtolower($content->label), 'presentasi'))
                      Dokumen slide pengajar dengan materi lengkap.
                    @elseif($content->appearance === 'video')
                      Materi video dibawakan oleh pengajar selama kurang lebih 30 menit.
                    @elseif($content->appearance === 'quiz')
                      Uji kompetensi untuk mendapatkan sertifikat
                    @endif
                  </div>
                </div>
              </div>
            @endif
          @endforeach
        </div>

      </div>
    </div>

  </div>

@endsection
