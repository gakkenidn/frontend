@extends('layouts.front')

@section('content')

  @include('layouts.navbar')

  <div class="container py-5">
    <div class="py-lg-0 py-5"></div>
    <div class="row justify-content-center">
      <div class="col-lg-8">

        <a href="{{ route('journals') }}" class="d-inline-block mb-3">
          <i class="fa fa-arrow-left"></i> Kembali ke Jurnal
        </a>

        <div class="row mb-5">
          <div class="col-3">
            <img src="{{ (starts_with($journal->image_url, 'http://') || starts_with($journal->image_url, 'https://')) ? $journal->image_url : asset('images/journals/' . $journal->image_url) }}" class="w-100 rounded" />
          </div>
          <div class="col-9">
            <div class="text-muted">{{ $journal->publisher }}</div>
            <h1>{{ $journal->title }}</h1>
            <div class="text-muted mb-5">ISSN {{ $journal->issn }} / eISSN {{ $journal->e_issn }}</div>

            <div>
              @if (auth()->check())
                <button type="button" id="read-btn" class="btn btn-primary">Baca Jurnal</button>
              @else
                <div class="alert alert-warning">
                  Silakan <a href="{{ route('login') }}" data-toggle="modal" data-target="#auth-dialog">masuk</a> ke akun Gakken Anda untuk membaca jurnal.
                </div>
              @endif
            </div>
          </div>
        </div>

        {!! $journal->description !!}
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script type="text/javascript">
    $(document).ready(function () {
      if ($("#read-btn").length) {
        $("#read-btn").on('click', function readJournal() {
          $.get("{{ route('journal.read', ['slug' => $journal->slug]) }}")
            .done(function (response) {
              window.location.href = response;
              return;
            });
        });
      }
    });
  </script>
@endsection