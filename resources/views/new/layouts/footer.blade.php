<footer>
    <div class="lg-container border-top padding-1 row no-gutters flex-column flex-md-row">
        <div class="col-md-2 col-6 col-md-2">
            <img src="/images/logo-color.png" class="img-fluid padding-1"/>
        </div>
        <div class="padding-1">
            <h6> Layanan </h6>
            <ul>
                <li> <a href="/articles"> Artikel Kesehatan</a></li>
                <li> <a href="#"> Gakken P2KB</a>  </li>
                <li> <a href="/journals"> Jurnal Premium</a> </li>
                <li>
                    <a href="/drugs"> Indeks Obat </a>
                </li>
                <li>
                    <a href="/service/genius"> Genius</a>
                </li>
            </ul>
        </div>
        <div class="padding-1">
            <h6> PT. Gakken </h6>
            <ul>
                <li>
                    <a href="http://gakken-idn.co.id"> Tentang Kami </a>
                </li>
                <li>
                    <a href="http://gakken-idn.co.id/category/news"> Berita Perusahaan </a>
                </li>
            </ul>
        </div>
        <div class=" padding-1">
            <h6> Bantuan </h6>
            <ul>
                <li> <a href="#"> Syarat dan Ketentuan </a> </li>
                <li> <a href="#"> Kebijakan Privasi </a> </li>
                <li> <a href="#"> FAQ </a> </li>
            </ul>
        </div>
        <div class="col-12 row">
            <span class="d-block col padding-1">
                 © 2017 PT. Gakken Health and Education Indonesia
            </span>

            <div class="col d-block padding-1 social-icon text-right">
                <a href="#"> <i class="fa fa-twitter"> </i> </a>
                <a href="#"> <i class="fa fa-facebook"> </i>  </a>
                <a href="#"> <i class="fa fa-instagram"> </i> </a>
                <a href="#"> <i class="fa fa-envelope"> </i> </a>
            </div>

        </div>
    </div>
</footer>
