<tr>
    <td>
        <table class="header" width="570" cellpadding="0" cellspacing="0">
            <tr>
                <td class="content-cell">
						        <a href="{{ $url }}">
						            {{ $slot }}
						        </a>
						    </td>
						</tr>
				</table>
    </td>
</tr>
