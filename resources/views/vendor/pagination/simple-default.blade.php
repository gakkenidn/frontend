@if ($paginator->hasPages())
    <div class="d-flex justify-content-between">
        {{-- Previous Page Link --}}
        @if (!$paginator->onFirstPage())
            <a class="btn btn-outline-secondary" href="{{ $paginator->previousPageUrl() }}" rel="prev">
                <b class="fa fa-angle-left"></b> Sebelumnya
            </a>
        @else
            <div></div>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="btn btn-outline-primary" href="{{ $paginator->nextPageUrl() }}" rel="next">
                Berikutnya <b class="fa fa-angle-right"></b>
            </a>
        @endif
    </div>
@endif
