@extends('layouts.front')

@section('content')
	
	@include('layouts.navbar')
	<div class="py-5 d-lg-none"></div>
	<div class="container my-5">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10">

				<div class="mb-5">
					<div class="h6 text-muted text-uppercase">
						Topik Gakken {{ $topic->category == 'doctor' ? 'P2KB' : 'P3KGB' }}&reg;
						{{ \Carbon\Carbon::parse($topic->publish_at)->format('F Y') }}
					</div>
					<h1>{{ $topic->title }}</h1>
				</div>

				<div class="d-md-flex mb-5">
					<div class="mr-4 mb-md-0 mb-3">
						<div class="h6 text-uppercase text-muted ">Jumlah SKP</div>
						<div class="lead">2</div>
					</div>
					<div class="ml-md-4 mr-4 mb-md-0 mb-3">
						<h6 class="text-uppercase text-muted">Standar Kelulusan</h6>
						<div class="lead">80%</div>
					</div>
					<div class="ml-md-4 mr-4 mb-md-0 mb-3">
						<h6 class="text-uppercase text-muted">Pengajar</h6>
						<div class="lead">{{ $topic->lecturers[0]->nameWithTitles }}</div>
					</div>
				</div>

				<div class="mb-5">
					<div class="h3">Ringkasan Topik</div>
					{!! clean($topic->summary) !!}

					<div class="mt-4">
						Topik diakreditasi oleh
						<img src="{{ asset('images/' . ($topic->category == 'doctor' ? 'idi' : 'pdgi') . '-logo.svg') }}" height="50" />
					</div>
				</div>

				<div class="mb-5 bg-highlight{{ $topic->category == 'dentist' ? '-2' : '' }} rounded-lg p-5">
					<div class="h3 text-white">Mulai Belajar Sekarang</div>
					<div class="row justify-content-between align-items-center">
						<div class="col-md-8 text-white">
							<div class="lead">Beli Backnumber</div>
							<p>
								Topik ini dapat Anda beli sebagai topik backnumber setelah berlangganan
								layanan Gakken, senilai IDR 500,000.
							</p>
						</div>
						<div class="col-md-4 text-white">
							<a href="#" class="btn btn-block btn-primary{{ $topic->category == 'dentist' ? '-2' : '' }} h6 text-uppercase">
								Berlangganan <b class="fa fa-angle-right"></b>
							</a>
						</div>
					</div>
				</div>

				<div class="mb-5">
					<div class="h3">Konten Pembelajaran</div>
					<p class="lead">
						Dalam topik ini, Anda akan mendapatkan konten-konten pembelajaran berikut.
					</p>
					<div class="topic-content-list mt-5">
						@foreach ($topic->contents as $content)
							<div class="topic-content-list-item-primary{{ $topic->category == 'dentist' ? '-2' : '' }}">
								<div class="h6 text-uppercase mb-0">{{ $content->appearance }}</div>
								<div class="lead">{{ $content->label }}</div>
								@if ($content->is_free && $content->type == 'file')
									<div><a href="{{ asset('topics/files/' . $content->data) }}" target="_blank"><i class="fa fa-download"></i> Unduh konten</a></div>
								@endif
							</div>
						@endforeach
					</div>
				</div>

			</div>
		</div>
	</div>

@endsection