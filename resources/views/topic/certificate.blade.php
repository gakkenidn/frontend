<!DOCTYPE html>
<html>
<head>
	<title>Sertifikat - {{ $topic->title }}</title>
	<style type="text/css">
		html {
			margin: 12pt;
		}
		body {
			font-family: "helvetica";
			font-size: 16pt;
			padding: 36pt;
			margin-bottom: -12pt;
			border: #ed1c24 6pt solid;
			background-image: url({{ resource_path('assets/images/certificate/2jp2BtLsDFM5FmfFhZPH6TVJzaO7IBbq.png') }});
			background-repeat: repeat;
		}
		h1, h2, h3, h4, h5, h6 {
			margin: 0;
			padding: 0;
			font-weight: 300;
		}
		p {
		}
		.heading {
			text-align: center;
			text-transform: uppercase;
			letter-spacing: 10pt;
		}
		h1.user-name {
			font-size: 42pt;
			text-align: center;
			color: #ed1c24;
			margin-top: 42pt;
		}
		h2.topic-title {
			text-align: center;
			font-size: 28pt;
			margin-top: 0;
		}
		h3.pre-topic-title {
			text-align: center;
			margin-bottom: 16pt;
		}
		.pass-date {
			margin-top: 36pt;
		}
		.point {
			text-align: center;
		}
		.fixed-footer {
			position: absolute;
			bottom: -70pt;
			left: 0;
			width: 100%;
			padding: 24pt 36pt;
		}
		.certificate-no {
			position: absolute;
			top: 0;
			left: 36pt;
			font-size: 9pt;
		}
		.fixed-footer p {
			font-size: 10pt;
		}
		.fixed-footer table {
			border: none;
			border-collapse: collapse;
			width: 100%;
		}
		.trial-overlay {
			position: absolute;
			top: 30px;
			left: 100px;
			right: 0;
			bottom: 0;
			text-align: center;
			opacity: 0.1;
		}
		strong {
			font-weight: 600;
		}
		hr {
			border: none;
			border-bottom: #ccc thin solid;
		}
	</style>
</head>
<body>

	<div class="certificate-no">
		{{ $certificateNo }}
		@if ($isTrial)
			(TRIAL)
		@endif
	</div>

	<h4 class="heading">Sertifikat</h4>
	<hr />
	<h1 class="user-name">{{ $user->name_on_certificate }}</h1>
	<h3 class="pre-topic-title">telah lulus dalam modul</h3>
	<h2 class="topic-title">
		@if ($topic->category == 'doctor')
			Pengembangan Pendidikan<br />Keprofesian Berkelanjutan
		@elseif ($topic->category == 'dentist')
			Pendidikan dan Pelatihan Profesionalisme<br />Kedokteran Gigi Berkelanjutan
		@endif
	</h2>
	<p style="text-align: center;">Modul e-Learning oleh PT. Gakken Health and Education Indonesia</p>
	<h2 class="point">{{ $topic->title }}: 2 SKP</h2>

	<div class="fixed-footer">
		<p>{{ \Carbon\Carbon::parse($passedAt)->format('j M Y') }}</p>
		<table>
			<tr>
				<td style="padding-right: 16pt; width: 200pt;">
					@if ($topic->category == 'doctor')
						{{-- <img src="{{ resource_path('assets/images/certificate/asadul.png') }}" height="80" />
						<p>
							<strong>Prof. Dr. dr. Andi Asadul Islam, Sp.B</strong><br />
							Dekan Fakultas Kedokteran UNHAS
						</p> --}}
						@if(\Carbon\Carbon::parse($passedAt)->gt(\Carbon\Carbon::parse('2018-01-01 00:00:00')))
							<img src="{{ resource_path('assets/images/certificate/budu.png') }}" height="80" />
							<p>
								<strong> Prof. dr. Budu, Ph.D., Sp.M., MMedEd </strong><br />
								Dekan Fakultas Kedokteran UNHAS
							</p>
						@else
							<img src="{{ resource_path('assets/images/certificate/asadul.png') }}" height="80" />
							<p>
								<strong> 	Prof. Dr. dr. Andi Asadul Islam, Sp.BS </strong><br />
								Dekan Fakultas Kedokteran UNHAS
							</p>
						@endif
					@elseif ($topic->category == 'dentist')
						<img src="{{ resource_path('assets/images/certificate/RLVhY4NaKuCikXwdXB1Eevm0IYuQz5sI.png') }}" height="80" />
						<p>
							<strong>Dr. drg. Bahruddin Thalib, M.Kes, Sp.Pros</strong><br />
							Dekan Fakultas Kedokteran Gigi UNHAS
						</p>
					@endif
				</td>
				<td style="width: 200pt;">
					@if ($topic->category == 'doctor')
						@if(\Carbon\Carbon::parse($passedAt)->gt(\Carbon\Carbon::parse('2018-01-01 00:00:00')))
							<img src="{{ resource_path('assets/images/certificate/ihsan.png') }}" height="80" />
							<p>
								<strong>dr. Muhammad Ihsan Mustari, MHM</strong><br />
								Ketua IDI Wilayah Sulselbar
							</p>
						@else
							<img src="{{ resource_path('assets/images/certificate/abd. kadir2.png') }}" height="80" />
							<p>
								<strong>Prof. Dr. Abdul Kadir, Ph.D, Sp.THT</strong><br />
								Ketua IDI Wilayah Sulselbar
							</p>
						@endif
					@elseif ($topic->category == 'dentist')
						@if(\Carbon\Carbon::parse($passedAt)->gt(\Carbon\Carbon::parse('2018-01-01 00:00:00')))
							<img src="{{ resource_path('assets/images/certificate/asdar_gani.png') }}" height="80" />
							<p>
								<strong>Dr. drg. Asdar Gani, M.Kes</strong><br />
								Ketua PDGI Wilayah Sulselbar
							</p>
						@else
							<img src="{{ resource_path('assets/images/certificate/2g8W7ekp1uml3D2wByNUfnc1g81z4LWX.png') }}" height="80" />
							<p>
								<strong>Prof. Dr. drg. Edy Machmud, Sp.Pros (K)</strong><br />
								Ketua PDGI Wilayah Sulselbar
							</p>
						@endif
						
					@endif
				</td>
				<td style="text-align: right; width: 320pt; vertical-align: bottom;">
					<img alt="Logo UNHAS" src="{{ resource_path('/assets/images/certificate/7Ko9I0VvuZ1OJFfB32WK9LvFxQHo6x0q.png') }}" height="70" style="margin-right: 8pt;" />
					@if ($topic->category == 'doctor')
						<img alt="Logo IDI" src="{{ resource_path('/assets/images/certificate/IoiuR5WH5NTG25PjYMmo22cXBDhU7Muz.png') }}" height="70" style="margin-right: 8pt;" />
					@elseif($topic->category == 'dentist')
						<img alt="Logo PDGI" src="{{ resource_path('/assets/images/certificate/RY3rj7QsHUv1JCgK9yVgt1uFPvBq5TGG.png') }}" height="70" style="margin-right: 8pt;" />
					@endif
					<img alt="Logo PT. Gakken" src="{{ resource_path('/assets/images/certificate/cetRLFCLdgew1bhTw9VlW76BrQgBntW3.png') }}" height="70" />
				</td>
			</tr>
		</table>
	</div>

	@if ($isTrial)
		<div class="trial-overlay">
			<img src="{{ resource_path('assets/images/certificate/trial-overlay.png') }}" height="700">
		</div>
	@endif

</body>
</html>