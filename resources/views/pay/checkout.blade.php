@extends('layouts.front-simple')

@section('content')
	
	@include('layouts.navbar-simple')

	<div class="container mt-4" style="min-height: 400px;">

		@if (Request::session()->get('alert-danger'))
			<div class="alert alert-danger mb-4">{{ Request::session()->get('alert-danger') }}</div>
		@endif

		<div class="pt-4 pb-5 text-md-left text-center d-lg-none">
			<img src="{{ asset('images/GakkenIndonesia-Original.svg') }}" height="50" alt="Gakken Indonesia">
		</div>
		<div class="row">
			<div class="col-lg-5">
				<h1>Beli Paket Langganan Gakken</h1>

				<p class="mb-4">Dapatkan manfaat berikut dengan berlangganan layanan Gakken Indonesia:</p>
				<div class="d-flex justify-content-start align-items-start">
					<div class="mx-3"><b class="fa fa-check text-success"></b></div>
					<p>Akses ke Topik Gakken P2KB&reg; / Gakken P3KGB&reg;, mengikuti tes dan mendapatkan sertifikat terakreditasi IDI/PDGI.</p>
				</div>
				<div class="d-flex justify-content-start align-items-start">
					<div class="mx-3"><b class="fa fa-check text-success"></b></div>
					<p>Akses ke lebih dari 400 jurnal premium Wiley.</p>
				</div>
				<div class="d-flex justify-content-start align-items-start">
					<div class="mx-3"><b class="fa fa-check text-success"></b></div>
					<p>Akses ke database rincian lebih dari 3000 merk obat Indonesia.</p>
				</div>
				<div class="d-flex justify-content-start align-items-start">
					<div class="mx-3"><b class="fa fa-check text-success"></b></div>
					<p>Dapat membeli Topik backnumber untuk Topik-Topik yang rilis sebelumnya.</p>
				</div>
				<hr />
				<div class="text-center">
					<div>hanya senilai</div>
					<div class="display-4 text-primary">IDR 666 ribu</div>
					<div>per bulan*</div>
				</div>
				<hr />
				<p class="text-center">Punya kode unik untuk paket berlangganan?</p>
				{!! Form::open(['route' => 'subscribe.redeem', 'id' => 'redeem']) !!}
					<div class="form-group">
						{!! Form::text('code', Request::input('redeem-code'), ['class' => 'form-control', 'placeholder' => 'Masukkan kodenya disini', 'autofocus' => Request::input('redeem-code') != null]) !!}
					</div>

					<button type="submit" class="btn btn-primary btn-block">Kirim</button>
				{!! Form::close() !!}

				<div class="d-md-none py-4"></div>
			</div>
			<div class="col-lg-7" id="subscribe-app">
				<div class="text-center lead">
					<b class="fa fa-circle-o-notch fa-spin fa-3x text-primary"></b>
					<p>
						Memuat opsi pembayaran..
					</p>
				</div>
			</div>
		</div>
	</div>

	<form id="payment-form" method="post" action="{{ route('pay.done') }}" class="hidden-xl-down" accept-charset="utf-8">
		<input type="hidden" name="_token" value="{!! csrf_token() !!}" />
		<input type="hidden" name="payment_method" id="payment-method" />
		<input type="hidden" name="result_type" id="result-type" />
		<input type="hidden" name="result_data" id="result-data" />
	</form>

@endsection

@section('scripts')
	<script type="text/javascript">
		window.SubscribeApp = {
			userEmail: "{{ auth()->user()->email }}",
			userName: "{{ auth()->user()->name }}",
			snapTokenUrl: "{{ route('mt.snaptoken') }}",
			bniBillingUrl: "{{ route('bni.billing') }}",
			subscriptions: {!! $subscriptions->toJson() !!}
		};
	</script>
	<script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ env('MIDTRANS_CLIENT') }}"></script>
	<script type="text/javascript" src="{{ mix('js/SubscribeApp.js') }}"></script>
@endsection