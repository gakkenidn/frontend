@extends('layouts.front-simple')

@section('content')
	@include('layouts.navbar-simple')

	<div class="container mt-4">
		<div class="row justify-content-center">
			<div class="col-8">
				@if (!in_array($status, ['success', 'pending']))

					<div>
						<div class="display-4">Pembayaran Gagal</div>
						<p class="lead">
							Halo, {{ auth()->user()->name }}. Terima kasih atas transaksi Anda di
							Gakken Indonesia.
						</p>
						<p>
							Pembayaran Anda tidak dapat diproses. Silakan mengulangi proses checkout di halaman
							<a href="{{ route('subscribe') }}">berlangganan</a>, atau pilih metode pembayaran lain. 
							Apabila dana terdebet dari akun bank Anda karena transaksi yang gagal ini, harap jangan 
							khawatir. Kami akan segera memproses pengembalian dana dalam waktu 2 hari kerja.
						</p>
						<p><a href="{{ route('home') }}">Kembali ke Beranda</a></p>
					</div>

				@elseif ($method == 'banktransfer')

					<div>
						<div class="display-4">Terima kasih</div>
						<p class="lead">
							Halo, {{ auth()->user()->name }}. Terima kasih telah melakukan pembelian di
							Gakken Indonesia. Saat ini transaksi Anda dalam status pending untuk menunggu 
							pembayaran Anda.
						</p>
						<ol>
							<li>
								Mohon segera transfer ke rekening berikut:
								<p class="lead">
									Bank Negara Indonesia 46<br />
									Nomor Rekening: <strong>{{ $vaNo }}</strong><br />
									Jumlah Pembayaran: <strong>Rp. {{ number_format($totalAmount, '0', '.', ',') }}</strong>
								</p>
							</li>
							<li>
								Setelah transfer harap segera melakukan <a href="#">Konfirmasi Pembayaran</a>
								agar dapat kami proses.
							</li>
							<li>
								Mohon melakukan pembayaran selambat-lambatnya 2 x 24 jam, atau pesanan Anda
								akan otomatis dibatalkan.
							</li>
						</ol>
						<p><a href="{{ route('home') }}">Kembali ke Beranda</a></p>
					</div>
					
				@else

					@if ($status == 'success')
						<div>
							<div class="display-4">Terima kasih</div>
							<p class="lead">
								Halo, {{ auth()->user()->name }}. Terima kasih telah melakukan pembayaran di
								Gakken Indonesia. 
							</p>
							<p>
								Status pembayaran Anda sukses. Terima kasih telah berlangganan di Gakken Indonesia. 
								Silakan ikuti petunjuk berikut untuk mulai belajar topik-topik Gakken P2KB dan P3KGB.
							</p>
							<p><a href="{{ route('home') }}">Kembali ke Beranda</a></p>
						</div>
					@elseif ($status == 'pending')
						<div>
							<div class="display-4">Terima kasih</div>
							<p class="lead">
								Halo, {{ auth()->user()->name }}. Terima kasih telah melakukan pembayaran di
								Gakken Indonesia.
							</p>
							<p>
								Saat ini transaksi Anda dalam status pending untuk dikonfirmasi oleh pihak kami.
								Mohon menunggu email konfirmasi selambat-lambatnya 2 hari kerja sebelum dapat 
								mengakses layanan premium Gakken Indonesia.
							</p>
							<p><a href="{{ route('home') }}">Kembali ke Beranda</a></p>
						</div>
					@endif

				@endif
			</div>
		</div>
	</div>

@endsection