@extends('layouts.front-simple')

@section('content')

	@include('layouts.navbar')

	<section>
		<div class="container">

			<div class="row justify-content-center align-items-center">
				<div class="col-5">
					<h1 class="text-right text-muted" style="font-size: 200px;"><strong>404</strong></h1>
				</div>
				<div class="col-7">
					<p class="h1">Ups, halaman yang Anda cari tidak ada :(</p>
					<p>Silakan kembali ke <a href="{{ route('home') }}">halaman beranda</a>.</p>
				</div>
			</div>

		</div>
	</section>

@endsection