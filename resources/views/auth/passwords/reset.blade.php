@extends('layouts.auth')

@section('content')
    <div class="row justify-content-center align-items-center h-100">
        <div class="col-md-4 pb-4">
            <div class="text-center pb-4 w-100">
                <img src="{{ asset('images/icon.png') }}" class="w-25" /><br />
            </div>

            <h1 class="h2 text-center mb-4">Buat Kata Sandi Baru</h1>

            <p>Silakan isi alamat email Anda serta kata sandi baru untuk
            memulihkan akun Anda.</p>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form role="form" method="POST" action="{{ route('password.request') }}">
                {{ csrf_field() }}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email">Alamat Email</label>

                    <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Kata Sandi Baru</label>

                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm">Ulangi Kata Sandi</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-primary">
                        Perbarui Kata Sandi
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
