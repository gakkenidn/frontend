@extends('layouts.auth')

@section('content')
    <div class="row justify-content-center align-items-center h-100">
        <div class="col-md-4 pb-4">
            <div class="text-center pb-4 w-100">
                <img src="{{ asset('images/icon.png') }}" class="w-25" /><br />
            </div>

            <h1 class="h2 text-center mb-4">Pulihkan Akun Anda</h1>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <form role="form" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}

                <p>Masukkan alamat email Anda yang sudah terdaftar untuk akun Gakken, dan
                kami akan mengirimkan tautan untuk pemulihan akun Anda.</p>

                <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                    <input id="email" type="email" class="form-control form-control-minimal" name="email" value="{{ old('email') }}" required placeholder="Alamat email">

                    @if ($errors->has('email'))
                        <span class="form-control-feedback small">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-block">
                        Kirim Tautan Pemulihan <b class="fa fa-angle-right"></b>
                    </button>
                </div>
                <div class="form-group">
                    <a href="{{ route('login') }}" class="btn btn-link btn-block">Batal</a>
                </div>
            </form>
        </div>
    </div>
@endsection
