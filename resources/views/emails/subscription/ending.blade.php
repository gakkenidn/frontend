@component('mail::message')
# Halo, {{ $userSubscription->user->name }}!

Masa aktif paket berlangganan
{{ $userSubscription->subscription->label }}
Anda untuk layanan Gakken Indonesia akan segera berakhir pada tanggal 
{{ \Carbon\Carbon::parse($userSubscription->expired_at)->format('d/m/Y') }}.
Pastikan Anda telah mempelajari dan melulusi setiap topik Anda!
Kami senang dapat memberi Anda layanan P2KB online terbaik.

Terima kasih, dan selamat belajar!
@endcomponent
