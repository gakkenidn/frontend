@component('mail::message')
# Halo, {{ $userSubscription->user->name }}!

Selamat! Paket berlangganan
{{ $userSubscription->subscription->label }}
Anda untuk layanan Gakken Indonesia telah
aktif. Masa berlangganan Anda akan berhenti pada tanggal 
{{ \Carbon\Carbon::parse($userSubscription->expired_at)->format('d/m/Y') }}.
Selama masa berlangganan, Anda dapat mengakses topik-topik P2KB yang rilis
tiap bulannya, mengakses jurnal premium Wiley, dan rincian obat lengkap.

@component('mail::button', ['url' => route('app', ['route' => '/'])])
Mulai Belajar
@endcomponent

Terima kasih, dan selamat belajar!
@endcomponent
