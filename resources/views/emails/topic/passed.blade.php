@component('mail::message')
# Halo, {{ $userTopic->user->name }}!

Selamat, Anda telah melulusi topik GakkenP2KB
"{{ $userTopic->topic->title }}"! Silakan mengakses sertifikat Anda 
melalui tombol dibawah. Pastikan Anda mendapatkan cap asli dari
@if ($userTopic->user->profession == 'doctor')
	Ikatan Dokter Indonesia (IDI) Wilayah Sulselbar
@else
	Persatuan Dokter Gigi Indonesia (PDGI) Wilayah Sulselbar
@endif
sebelum menggunakan sertifikat Anda untuk pembaruan STR/SIP.

@component('mail::button', ['url' => route('app', ['route' => '/profile/certificates'])])
Unduh Sertifikat
@endcomponent

Terima kasih, dan selamat belajar!
@endcomponent
