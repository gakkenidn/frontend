@component('mail::message')

# Halo, {{ $user->name }}!

Selamat bergabung di Gakken Indonesia, situs pembelajaran lengkap
untuk dokter dan dokter gigi. Anda terdaftar di situs Gakken dengan
alamat email {{ $user->email }}.


Dengan berlangganan layanan Gakken Indonesia, Anda dapat mengakses
topik pembelajaran P2KB bernilai poin SKP, jurnal premium Wiley dan
database rincian obat lengkap.

@if (strlen($user->verify_token) > 0)

Silakan memverifikasi akun Anda.

@component('mail::button', ['url' => route('verify') . '?token=' . $user->verify_token])
Verifikasi Akun
@endcomponent

@else
@component('mail::button', ['url' => url('/')])

Ke Gakken Indonesia

@endcomponent
@endif

Terima kasih telah bergabung! Selamat belajar! Klik
<a href="{{ route('login') }}">disini</a> untuk login.

@endcomponent