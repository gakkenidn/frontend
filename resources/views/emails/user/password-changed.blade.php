@component('mail::message')
# Halo, {{ $user->name }}

Anda baru saja mengubah kata sandi untuk akun Anda {{ $user->email }}. 
Jika Anda tidak mengetahui aktivitas ini, harap segera menghubungi 
tim support kami melalui email ke support@gakken-idn.co.id, call
center 0800-1-401652, atau layanan chat di 
<a href="{{ route('home') }}">situs Gakken</a>.

Untuk keamanan akun Gakken Anda, ubah kata sandi Anda secara berkala.

Terima kasih.
@endcomponent
