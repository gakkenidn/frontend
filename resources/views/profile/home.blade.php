@extends('layouts.profile')

@section('profile-content')

	<div class="row">
		<div class="col-md-4 mb-5">
			<h4>Profil</h4>
			<hr />
			<div class="mb-3">
				<div class="small text-muted text-uppercase">Bergabung sejak</div>
				<div class="lead">{{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y') }}</div>
			</div>
			<div class="mb-3">
				<div class="small text-muted text-uppercase">Profesi</div>
				<div class="lead">
					{{ $user->profession == 'doctor' ? "Dokter" : ( $user->profession == 'dentist' ? "Dokter Gigi" : "Belum diatur" ) }}
				</div>
			</div>
			<div class="mb-3">
				<div class="small text-muted text-uppercase">Nomor ID Keprofesian</div>
				<div class="lead">{{ $user->id_no ?: 'Belum diatur' }}</div>
			</div>
			<a href="{{ route('profile.edit') }}"><i class="fa fa-pencil"></i> Edit profil</a>
		</div>
		<div class="col-md-8">
			{{-- <h4>Status Berlangganan</h4>
			<hr />

			@if ($sub = $user->userSubscriptions()->active()->with('subscription')->first())
				<div>
					<div class="text-success d-flex justify-content-start align-items-center mb-3">
						<i class="fa fa-check-circle fa-2x mr-2"></i>
						<div>{{ $sub->subscription->label }}</div>
					</div>
					<p>
						Saat ini Anda sedang berlangganan paket Gakken P2KB
						yang akan berakhir pada {{ \Carbon\Carbon::parse($sub->expired_at)->format('d/m/Y') }}.
					</p>
				</div>

			@else
				@if (!$user->id_no || !$user->profession)
					<div class="alert alert-warning">
						Harap <a href="{{ route('profile.edit') }}">mengatur profesi dan nomor ID keprofesian Anda</a> untuk dapat membeli paket berlangganan.
					</div>
				@endif
				<div>
					<p>
						Saat ini, Anda belum berlangganan paket Gakken P2KB. Anda
						dapat membeli paket berlangganan di <a href="{{ route('subscribe') }}">Halaman berlangganan</a>.
					</p>
				</div>
			@endif --}}
		</div>
	</div>

@endsection