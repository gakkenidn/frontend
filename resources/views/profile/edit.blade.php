@extends('layouts.profile')

@section('profile-content')
	
	{!! Form::model($user, ['route' => 'profile.patch', 'files' => true, 'method' => 'patch']) !!}

		<div class="form-group row align-items-center">
			<div class="text-muted col-form-label col-lg-3">Alamat Email</div>
			<div class="col-lg-9">
				@if(filter_var($user->email, FILTER_VALIDATE_EMAIL ))
					{{ $user->email }}&emsp;<a href="#" data-toggle="modal" data-target="#infoModal">Ubah</a>
				@else
					{!! Form::text('email', null, ['class' => 'form-control']) !!}
				@endif
			</div>
		</div>

		<div class="form-group row align-items-center{{ $errors->first('name') ? ' has-danger' : '' }}">
			<div class="text-muted col-form-label col-lg-3">Nama Lengkap</div>
			<div class="col-lg-9">
				{!! Form::text('name', null, ['class' => 'form-control']) !!}
				@if ($errors->first('name'))
					<span class="form-control-feedback">{{ $errors->first('name') }}</span>
				@endif
			</div>
		</div>

		<div class="form-group row align-items-center">
			<div class="text-muted col-form-label col-lg-3">Nama di Sertifikat</div>
			<div class="col-lg-9">
				{{ $user->name_on_certificate }}&emsp;<a href="#" data-toggle="modal" data-target="#infoModal">Ubah</a>
			</div>
		</div>

		<div class="form-group row align-items-center{{ $errors->first('profession') ? ' has-danger' : '' }}">
			<div class="text-muted col-form-label col-lg-3">Profesi</div>
			<div class="col-lg-9">
				{!! Form::select('profession', ['doctor' => 'Dokter', 'dentist' => 'Dokter Gigi'], null, ['class' => 'form-control', 'placeholder' => 'Harap dipilih']) !!}
				@if ($errors->first('profession'))
					<span class="form-control-feedback">{{ $errors->first('profession') }}</span>
				@endif
			</div>
		</div>

		<div class="form-group row align-items-center{{ $errors->first('idNo') ? ' has-danger' : '' }}">
			<div class="text-muted col-form-label col-lg-3">Nomor ID Keprofesian</div>
			<div class="col-lg-9">
				{!! Form::text('idNo', $user->id_no, ['class' => 'form-control']) !!}
				@if ($errors->first('idNo'))
					<span class="form-control-feedback">{{ $errors->first('idNo') }}</span>
				@endif
			</div>
		</div>

		<div class="form-group row align-items-center{{ $errors->first('avatar') ? ' has-danger' : '' }}">
			<div class="text-muted col-form-label col-lg-3">Ubah Avatar</div>
			<div class="col-lg-3 col-md-3">
				<img src="{{ $user->avatar_url ?: asset('images/user.svg') }}" class="w-100 rounded-circle" alt="Avatar" />
			</div>
			<div class="col-md-6">
				{!! Form::file('avatar', ['class' => 'form-control']) !!}
				<p class="small text-muted">File gambar maks. 2MB</p>

				@if ($errors->first('avatar'))
					<span class="form-control-feedback">{{ $errors->first('avatar') }}</span>
				@endif
			</div>
		</div>

		<div class="form-group row mt-5">
			<div class="col-lg-9 offset-lg-3">
				<button type="submit" class="btn btn-primary">Simpan</button>
			</div>
		</div>

	{!! Form::close() !!}

	<div class="modal fade" id="infoModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header justify-content-start">
					<i class="fa fa-info-circle mr-3"></i> Informasi
				</div>
				<div class="modal-body">
					<p>
						Untuk mengubah data alamat email atau nama di sertifikat, harap menghubungi staf kami melalui telepon 0800-1-401652,
						email {{ 'support@gakken-idn.co.id' }}, atau Live Chat di situs ini.
					</p>
					<div class="d-flex justify-content-end"><a href="#" data-dismiss="modal" class="btn btn-link">Tutup</a></div>
				</div>
			</div>
		</div>
	</div>

@endsection

