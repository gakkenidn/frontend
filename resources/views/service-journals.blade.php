@extends('layouts.front')

@section('content')
	
	<section class="cover p-lg-0 pb-5" style="background-image: url('{{ asset('images/journals-cover.jpg') }}');background-position: bottom;">
		
		@include('layouts.navbar')
		<div class="container py-3 text-xl-left text-center">
			<div class="row mt-5 justify-content-xl-start justify-content-center">
				<div class="col-xl-8 col-lg-10">
					<h1 class="display-4 d-none d-md-block">Jurnal Premium untuk Pembelajaran Anda.</h1>
					<h1 class="d-md-none">Jurnal Premium untuk Pembelajaran Anda.</h1>
					<p class="lead mb-5">
						Gakken Indonesia bekerjasama dengan Wiley Publishings untuk
						menunjang pembelajaran Anda dengan lebih dari 400 jurnal premium.
					</p>
					<p>
						<a href="{{ route('subscribe') }}" class="btn btn-primary p-3 h6 text-uppercase">Berlangganan <b class="fa fa-angle-right"></b></a>
						<a href="#about" class="btn btn-outline-secondary p-3 h6 text-uppercase">Pelajari <span class="d-none d-md-inline">Lebih Lanjut</span> <b class="fa fa-angle-right"></b></a>
					</p>
				</div>
			</div>
		</div>

	</section>

	<section id="about">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-6 d-none d-lg-block pr-5 text-center pb-md-0 pb-5">
					<img src="{{ asset('images/home-journals.jpg') }}" class="w-100" />
				</div>
				<div class="col-lg-6 col-md-10">
					<div class="section-heading text-lg-left text-center">
						<h5>Lengkapi Pembelajaran Anda</h5>
					</div>
					<p class="lead">Koleksi jurnal Wiley Publishing kami hadirkan untuk
					kenyamanan dan kelengkapan pembelajaran Anda di situs Gakken Indonesia.</p>
					<p>Temukan jurnal dari berbagai bidang medis secara lengkap dari 
					100 tahun lalu hingga sekarang. Jurnal Premium dapat diakses secara 
					full dan otomatis ketika Anda berlangganan paket Gakken P2KB atau P3KGB.</p>
					<p>Ingin melihat-lihat daftar jurnal kami? Silakan
					masuk dengan akun Gakken Anda, dan <a href="/">buka daftar jurnalnya</a>.</p>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="container">
			<div class="row justify-content-center align-items-center">
				<div class="col-md-8">
					<div class="section-heading">
						<h5 class="text-center">Jurnal Wiley Populer</h5>
					</div>
				</div>
			</div>
			<div class="row mb-5 no-gutters">
				@foreach ($journals as $index => $journal)
					<div class="col-lg-2 col-md-4 col-6 px-2 mb-lg-0 mb-3">
						<a href="{!! route('app', ['route' => '/journals/' . $journal->slug]) !!}" class="card shadowed">
							<div class="card-img-top" style="overflow: hidden;">
								<img src="{{ asset('/images/journals/' . $journal->image_url) }}" class="w-100" />
							</div>
							<div class="card-body p-3">
								<div class="card-title ellipsis-3">
									{{ $journal->title }}
								</div>
								<div class="card-text text-muted small">{{ $journal->issn }}</div>
							</div>
						</a>
					</div>
				@endforeach
				<div class="col-lg-2 col-md-4 col-6 px-2 mb-lg-0 mb-3">
					<a href="#" class="card shadowed h-100 w-100">
						<div class="card-body d-flex flex-column justify-content-center">
							<div class="card-text text-center text-muted small">
								<b class="fa fa-arrow-right fa-3x mb-3"></b><br />
								dan masih banyak jurnal lainnya
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>

	<section class="bg-highlight py-5">
		<div class="container">
			<div class="row justify-content-center my-4">
				<div class="col-lg-9">
					<div class="p-5 bg-white rounded-lg shadowed">
						<div class="row align-items-center">
							<div class="col-lg-8 text-lg-left text-center mb-lg-0 mb-4">
								<div class="h2">Tunggu apa lagi?</div>
								<p class="lead mb-0">
									Dapatkan akses ke Jurnal Premium Wiley dengan berlangganan
									Gakken P2KB atau P3KGB sekarang juga!
								</p>
							</div>
							<div class="col-lg-4">
								<a href="#" class="btn btn-block btn-primary p-3 h6 text-uppercase">Berlangganan <b class="fa fa-angle-right"></b></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

@endsection