<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ page_title('Backend') }}</title>
	<link rel="stylesheet" type="text/css" href="{{ mix('css/backend-app.css') }}">
	<script type="text/javascript">
		window.App = {
			url: '{{ route('backend', ['route' => '/']) }}',
			rootUrl: '{{ route('home') }}',
			restUrl: '{{ url('/app/rest/admin') }}',
			user: {
				name: '{{ Auth::user()->name }}',
				avatar: '{{ Auth::user()->avatar_url }}'
			}
		};
	</script>
</head>
<body class="bg-light">

	<div id="app"></div>

	<script type="text/javascript" src="{{ mix('js/BackendApp.js') }}"></script>
</body>
</html>