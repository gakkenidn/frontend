const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
	.js('resources/assets/js/front.js', 'public/js')
	// .react('resources/assets/js/App.jsx', 'public/js')
	.react('resources/assets/backend/js/BackendApp.jsx', 'public/js')
	// .react('resources/assets/js/SubscribeApp.jsx', 'public/js')
	.react('resources/assets/catalog/CatalogApp.jsx', 'public/js')
	.react('resources/assets/journals/JournalsApp.jsx', 'public/js')
	.react('resources/assets/drugs/DrugsApp.jsx', 'public/js')
	.react('resources/assets/drugs/DrugsSingleApp.jsx', 'public/js')
	.react('resources/assets/learn/LearnApp.jsx', 'public/js')
	.sass('resources/assets/sass/front/front.scss', 'public/css')
	.sass('resources/assets/sass/app/app.scss', 'public/css')
	.sass('resources/assets/backend/sass/backend-app.scss', 'public/css');

// Comment the following line if developing assets
mix.version();

mix.browserSync('http://gakken-idn.local');
