<!DOCTYPE html>
<html lang="id-ID">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

	<title><?php echo e(isset($title) ? $title : config('app.name')); ?></title>

	<link rel="stylesheet" type="text/css" href="<?php echo e(mix('css/front.css')); ?>">
	<link href="/css/sponsor.css" rel="stylesheet">

	<link rel="icon" href="<?php echo e(asset('images/icon.png')); ?>">
	<link rel="icon" sizes="48x48" href="<?php echo e(asset('images/icon@0,25x.png')); ?>">
	<link rel="icon" sizes="96x96" href="<?php echo e(asset('images/icon@0,5x.png')); ?>">
	<link rel="icon" sizes="144x144" href="<?php echo e(asset('images/icon@0,75x.png')); ?>">

	<link rel="apple-touch-icon" href="<?php echo e(asset('images/icon.png')); ?>">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('images/touch-icon-ipad.png')); ?>">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo e(asset('images/touch-icon-iphone-retina.png')); ?>">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo e(asset('images/touch-icon-ipad-retina.png')); ?>">

	<link rel="manifest" href="<?php echo e(asset('manifest.json')); ?>">

	<meta name="msapplication-square70x70logo" content="<?php echo e(asset('images/icon_smalltile.png')); ?>">
	<meta name="msapplication-square150x150logo" content="<?php echo e(asset('images/icon_mediumtile.png')); ?>">

	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>


	<meta name="theme-color" content="#a4151a">

	<?php echo $__env->yieldContent('head'); ?>
	<script type="text/javascript">
		window.App = {
			authUrl: "<?php echo e(route('login')); ?>"
		};
	</script>
</head>
<body>

	<?php echo $__env->yieldContent('content'); ?>

	<?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<?php echo $__env->make('layouts.navbar-fixed', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	<script type="text/javascript" src="<?php echo e(mix('js/front.js')); ?>"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

	<?php if(Request::session()->get('profession-select') && auth()->check()): ?>
		<?php echo $__env->make('profile.profession-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php endif; ?>

	<script type="text/javascript">
		$(document).ready(function () {
			$("[data-toggle=auth-dialog]").on('click', function (e) {
				e.preventDefault();
				$("#auth-dialog-social, #auth-dialog-email").toggleClass('hidden-xl-down');
			});

			$('.homepage-slider').slick({
				dots: true,
				infinite: true,
				speed: 300,
				arrows: false,
				slidesToShow: 1,
				adaptiveHeight: true,
				// autoplay: true
			});
		});

	</script>

	<?php if(env('APP_ENV') == 'production'): ?>
		<!--Start of Zendesk Chat Script-->
		
		<!--End of Zendesk Chat Script-->
	<?php endif; ?>
	<?php if(env('GOOGLE_ANALYTICS_ID', false)): ?>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', '<?php echo e(env('GOOGLE_ANALYTICS_ID')); ?>', 'auto');
		  <?php if(auth()->check()): ?>
		  	ga('set', 'userId', <?php echo e(auth()->user()->id); ?>);
		  	ga('set', 'dimension1', <?php echo e(auth()->user()->id); ?>);
		  <?php endif; ?>
		  ga('send', 'pageview');

		</script>
	<?php endif; ?>
	<?php echo $__env->yieldContent('scripts'); ?>

</body>
</html>