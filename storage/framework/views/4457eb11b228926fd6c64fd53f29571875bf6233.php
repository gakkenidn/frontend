<?php $__env->startSection('content'); ?>

    <section class="cover p-0"
        style="
        background-color: #F2F5F7;
        
        
        ">
        <div class="mb-5">
            <?php echo $__env->make('layouts.navbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="container">
                <div class="homepage-slider align-items-center pb-2 pt-5 pt-lg-0">
                    <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="rounded d-flex align-items-end" style="display: block;
                            position: relative;
                            height: 500px;
                            background-color: rgba(102, 102, 102, 0.3);
                            background-image: url('<?php echo e($slider->featuredImages[0]->url); ?>');
                            background-blend-mode: multiply;
                            background-size: cover;
                            background-position: center center;
                            background-repeat: no-repeat;
                            ">
                            <div class="bg-white text-grey col-md-6 col-8 mx-4 my-4 rounded p-4 shadowed"> 
                                <p class="text-small small strong text-strong my-0 mb-0 text-uppercase opacity-half" > 
                                    <?php echo e($slider->categoryTaxonomies[0]->term->name); ?>

                                </p>
                                <h4 class="display-5 my-0 mb-4"><?php echo e($slider->post_title); ?></h4>
                                <a 
                                    href="<?php echo e(route('article.single', ['slug' => $slider->post_name])); ?>" 
                                    class="btn btn-primary shadowed"> 
                                    Read More 
                                </a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>
            </div>
        </div>
    </section>

    <div class="container py-5 mb-5">
        <div class="row justify-content-center">
            <div class="col-12 row mx-0 px-0"> 
                <?php $__currentLoopData = $recentArticles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-md-4 py-2">
                        <a
                            href="<?php echo e(route('articles') . '/' . $article->post_name); ?>"
                            style="
                            display: block;
                            position: relative;
                            height: 320px;
                            <?php if($article->featuredImages->count()): ?>
                                background-color: rgba(0,0,0,.4);
                                background-image: url('<?php echo e($article->featuredImages[0]->url); ?>');
                                background-blend-mode: multiply;
                                background-size: cover;
                                background-position: center center;
                                background-repeat: no-repeat;
                            <?php endif; ?>
                            "
                            class="text-white rounded">
                            <span class="px-3 py-2 text-uppercase" style="position: absolute; left: 0; top: 0;">
                                <?php echo e($article->categoryTaxonomies[0]->term->name); ?>

                            </span>
                            <h5 class="px-3 py-2" style="position: absolute; left: 0; bottom: 0;"><?php echo e($article->post_title); ?></h5>
                        </a>

                        
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>

            
        </div>
        <div class="text-center mt-5">
            <a href="<?php echo e(route('articles')); ?>" class="see-more"> Semua Artikel Kesehatan <i class="fa fa-angle-right"> </i> </a>
        </div>
    </div>

    <div class="container mb-5">
        <a class="text-white box-shadow rounded p-5 d-flex flex-md-row flex-column justify-content-between align-items-center" style="background: url('<?php echo e(asset('images/survey.png')); ?>') center center no-repeat; background-size: cover; text-decoration: none;" href="https://form.responster.com/rnfIUf">
            <div class="d-flex flex-md-row flex-column justify-content-start align-items-center pr-md-4">
                <div class="text-md-left text-center col-md-8 mx-0 px-0">
                    <div class="h2 m-0"> Seberapa penting terapi kesehatan untuk Anda? </div>
                    <p class="lead m-0">
                       Bagikan pendapat Anda dengan mengikuti survey kami.
                    </p>

                     <div class="d-block mt-4">
                         <button class="btn shadowed" style="background: #C33C54; font-weight: 700; color: #fff"> Ikuti Survey </button>
                     </div>

                </div>
            </div>
        </a>
    </div>

    <div class="container py-5 mb-5">
        <div class="d-block text-uppercase py-4 text-strong h6 color-red"> Artikel Populer </div>
        <div class="row mx-0 px-0"> 
            <div class="col-md-6 mx-0 row p-2"> 
                <?php $__currentLoopData = $popularArticles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="mx-0 row p-2">
                        <div class="d-inline-block h1 popular-width"> <?php echo e($key + 1); ?>. </div>
                        <div class="col"> 
                            <a 
                                href="/articles/<?php echo e($article->post->post_name); ?>" 
                                class="text-black text-strong h5 d-block"> 
                                <?php echo e($article->post->post_title); ?>  
                            </a>
                            <a 
                                href="/articles/<?php echo e($article->post->post_name); ?>" 
                                class="text-black text-small text-uppercase text-strong d-block" style="opacity: 0.5"> 
                                <?php echo e($article->post->author->display_name); ?> 
                            </a>
                        </div>
                    </div>
                    <?php if(($key+1) % 2 === 0): ?> 
                        </div>
                        <div class="col-md-6 mx-0 row p-2">
                    <?php endif; ?>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>   
    </div>
    
    <?php if(count($infographics) > 0): ?>   
        <div class="container py-5 mb-5">
            <div class="row mx-0 px-0 py-4"> 
                <div class="col-md-6 py-4"> 
                    <div class="d-block text-uppercase py-4 text-strong h6 color-red"> Infografis </div>
                    <div class="h5">  
                        <a 
                            href="/articles/<?php echo e($infographics[0]->post_name); ?>" 
                            class="text-black text-strong h5 d-block"> 
                            <?php echo e($infographics[0]->post_title); ?> 
                        </a>
                       
                    </div>
                    <p> <?php echo $infographics[0]->subheading->content; ?> <p>
                </div>
                <div class="col-md-6 justify-content-center align-items-center d-flex"> 
                    <a 
                        href="/articles/<?php echo e($infographics[0]->post_name); ?>" 
                        class="text-black text-strong h5 d-block"> 
                        <div style="display: block;
                            position: relative;
                            height: 500px;
                            width: 350px;
                            background-image: url('<?php echo e($infographics[0]->featuredImages[0]->url); ?>');
                            background-size: cover;
                            background-position: top center;
                            background-repeat: no-repeat;" class="shadowed rounded">  
                        </div>
                    </a>
                </div>
            </div>
       
         
            <div class="d-block border-top"> 
                <div class="d-block px-4 pt-4 pb-2 opacity-half"> Infografis Lainnya </div>
                <div class="row mx-0 px-0"> 
                    <?php $__currentLoopData = $infographics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $infographic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($key > 0): ?>
                            <div class="col-md-4"> 
                                <div style="display: block;
                                        position: relative;
                                        height: 246px;
                                        width: 100%;
                                        background-image: url('<?php echo e($infographic->featuredImages[0]->url); ?>');
                                        background-size: cover;
                                        background-color: rgba(0,0,0,0.4);
                                        background-position: top center;
                                        background-repeat: no-repeat;" class="shadowed rounded">  

                                        <a href="/articles/<?php echo e($infographic->post_name); ?>" class="px-3 py-2 text-white h5" style="position: absolute; left: 0; bottom: 0;"> 
                                            <?php echo e($infographic->post_title); ?> 
                                        </a>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if(count($p2kbTopics)): ?>
        <div class="bg-dark py-5"> 
            <div class="container my-5"> 
                    <div class="row mx-0 px-0 py-4"> 
                        <div class="col-md-6 py-4 order-2 order-md-1"> 
                            <div class="d-block text-uppercase text-strong h6 color-blue"> TOPIK P2KB TERBARU</div>
                            <div class="h5 text-white py-2 mb-0"> <?php echo e($p2kbTopics[0]->title); ?> </div>
                            <div class="text-white"> 
                                <?php echo $p2kbTopics[0]->summary; ?>

                            </div>
                            <a href="<?php echo e(route('p2kb.single', ['slug' => $p2kbTopics[0]->slug])); ?>" class="btn primary-btn accentblue text-white strong text-uppercase"> Pelajari </a>
                        </div>
                        <div class="col-md-6 justify-content-center align-items-center d-flex order-1 order-md-2"> 
                            <div class="col-md-10">
                                <img src="<?php echo e(env('VAULT_URL') . '/topic/files/' . $p2kbTopics[0]->featured_image); ?>" class="w-100 img-fluid rounded shadowed" />
                            </div>
                        </div>
                </div>
            </div>
        </div>
    <?php endif; ?>


    <div class="py-5 px-2">
        <div class="container my-5">

            <div class="row border-bottom mb-5">
                <div class="col-md-4 mb-5"> 
                    <div class="border rounded white-bg overflow-hidden"> 
                        <a href="#" class="d-block"> <img src="/images/paramount-bed-banner.png" class="ads-home-images" /> </a>
                        <div class="px-4 pb-4 pt-3 h-100"> 
                            <p class="text-secondary mb-0 pb-0"> Sponsored by</p>
                            <div class="text-center">
                                <div class="mr-auto py-2">
                                <img src="/images/logo-paramount.png"  height="50px" class="mw-100"/>
                                </div>
                            
                                <a class="btn btn-paramount mt-4" href="/paramountbed"> Learn More </a>   
                            </div>
                        </div>
                    </div>
                </div>
                <?php if($sponsor): ?>
                    <?php $__currentLoopData = $sponsor->sponsorContent; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $content): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-4 mb-5">
                            <div class="">
                                <a href="<?php echo e(route('p2kb.single', ['slug' => $content->content->slug])); ?>">
                                    <img src="<?php echo e(env('VAULT_URL') . '/topic/files/' . $content->content->featured_image); ?>" class="w-100 mb-3">
                                </a>

                                <a href="<?php echo e(route('p2kb.single', ['slug' => $content->content->slug])); ?>">
                                    <div class="text-dark d-flex justify-content-between align-items-center mb-2">
                                        <h6 class="text-truncate m-0"><?php echo e($content->content->title); ?></h6>
                                    </div>
                                </a>
                                <div class="ellipsis-3">
                                    <?php echo $content->content->summary; ?>

                                </div>

                                <div class="mt-4 row align-items-center">
                                    <span class="d-block col-8"> 2 SKP | IDI </span>
                                    <div class="col text-right">
                                        <a href="<?php echo e(route('p2kb.single', ['slug' => $content->content->slug])); ?>">
                                            <img src="<?php echo e(asset('images/idi-logo.svg')); ?>" class="img-fluid icon" style="width: 40px;" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
            
            <div> 
                <div class="row mx-0 px-0">
                     <p class="text-small small py-2 opacity-half col-lg-3 mx-0 px-0"> Temukan topik pembelajaran berkualitas lainnya untuk Dokter dan Dokter
                    Gigi di Gakken Indonesia. </p>
                </div>
                
                <div class="row">
                    <?php $__currentLoopData = $p2kbTopics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $topic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($index > 0): ?>
                            <div class="col-md-2 mb-5">
                                <div class="">
                                    <a href="<?php echo e(route('p2kb.single', ['slug' => $topic->slug])); ?>">
                                        <img src="<?php echo e(env('VAULT_URL') . '/topic/files/' . $topic->featured_image); ?>" class="w-100 mb-3">
                                    </a>

                                    <div class="row mx-0 px-0 align-items-center">
                                        <div class="mx-0 px-0 col-10 text-dark justify-content-between align-items-center mb-0">
                                            <a href="<?php echo e(route('p2kb.single', ['slug' => $topic->slug])); ?>"> <h6 class="text-truncate m-0"><?php echo e($topic->title); ?></h6> </a>
                                        </div>

                                        <div class="mx-0 px-0 col text-right">
                                            <img src="<?php echo e(asset('images/idi-logo.svg')); ?>" class="img-fluid icon" style="width: 20px;" />
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    <?php $__currentLoopData = $p3kgbTopics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $topic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col-md-2 mb-5">
                            <div class="">
                                <a href="<?php echo e(route('p2kb.single', ['slug' => $topic->slug])); ?>">
                                    <img src="<?php echo e(env('VAULT_URL') . '/topic/files/' . $topic->featured_image); ?>" class="w-100 mb-3">
                                </a>

                         

                                <div class="row mx-0 px-0 align-items-center">
                                        <div class="mx-0 px-0 col-10 text-dark justify-content-between align-items-center mb-0">
                                            <a href="<?php echo e(route('p2kb.single', ['slug' => $topic->slug])); ?>"> <h6 class="text-truncate m-0"><?php echo e($topic->title); ?></h6> </a>
                                        </div>

                                        <div class="mx-0 px-0 col text-right">
                                            <img src="<?php echo e(asset('images/pdgi-logo.svg')); ?>" class="img-fluid icon" style="width: 20px;" />
                                        </div>
                                    </div>
                                <!-- <div class="ellipsis-3">
                                    <?php echo $topic->summary; ?>

                                </div> -->

                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                </div>

                <div class="text-center d-block">
                    <a href="<?php echo e(route('p2kb')); ?>"> Semua Topik P2KB <i class="fa fa-angle-right"> </i> </a>
                </div>

            </div>
           

        </div>
    </div>

    <div class="container py-5">
        <div class="row my-5">
            <div class="d-none d-md-block col-md-5">
                <img src="/images/convention.png" class="w-100 rounded" />
            </div>
            <div class="col-md-7 padding-1">
                <h4>Events</h4>
                <ul class="list-unstyled">
                    <?php $__currentLoopData = $recentEvents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $event): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <li class="py-2">
                                    <a href="/event/<?php echo e($event->slug); ?>" class="d-block">
                                        <h5 class="mb-0"> <?php echo e($event->name); ?> </h5>
                                    </a>
                                    <p class="mb-0">  <span class="location"> <?php echo e($event->location); ?> </span> </p>
                                    <p>
                                        <span class="time" style="font-weight: 700">
                                            <?php echo e(\Carbon\Carbon::parse($event->start_at)->format('l') . ' - ' . \Carbon\Carbon::parse($event->end_at)->format('l')); ?>,
                                            <?php echo e(\Carbon\Carbon::parse($event->start_at)->format('j F Y') . ' - ' . \Carbon\Carbon::parse($event->end_at)->format('j F Y')); ?>

                                        </span>
                                    </p>
                                </li>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
                <a href="/events" class="d-block see-more"> Lihat semua event <i class="fa fa-angle-right"> </i> </a>
            </div>
        </div>
    </div>

    <div class="container py-5">
        <div class="my-5 row justify-content-around no-gutters">
            <div class="col-12 col-lg-4 col-md-6 mb-5 row mx-0 px-4">
                <div class="d-inline-block"> <img src="/images/new-icons/education.png" style="height: 50px;" class="mb-3" /> </div>
                <div class="col text-left">
                    <h5>Gakken P2KB</h5>
                    <p>Tersedia topik-topik yang khusus dibuat untuk memenuhi kebutuhan dokter dan dokter gigi Indonesia yang dilengkapi dengan sertifikat SKP.</p>
                </div> 
            </div>

            <div class="col-12 col-lg-4 col-md-6 mb-5 row mx-0 px-4">
                <div class="d-inline-block"> <img src="/images/new-icons/search.png" style="height: 50px;" class="mb-3" /> </div>
                <div class="col text-left"> 
                    <h5>Artikel Kesehatan</h5>
                    <p>Selalu ada terobosan dan temuan mutakhir di dunia kesehatan. Kami hadir untuk menyajikan update berita-berita terhangat untuk Anda.</p>
                </div>  
            </div>

            <div class="col-12 col-lg-4 col-md-6 mb-5 row mx-0 px-4">
                <div class="d-inline-block"> <img src="/images/new-icons/sheets.png" style="height: 50px;" class="mb-3" /> </div>
                <div class="col text-left"> 
                    <h5>Jurnal Kesehatan</h5>
                    <p> Terdapat lebih dari 2250 jurnal yang dapat diakses dengan leluasa, untuk kenyamanan pembelajaran Anda. Disediakan oleh Elsevier.</p>
                </div>  
            </div>

            <div class="col-12 col-lg-4 col-md-6 mb-5 row mx-0 px-4">
                <div class="d-inline-block"> <img src="/images/new-icons/drugs.png" style="height: 50px;" class="mb-3" /> </div>
                <div class="col text-left"> 
                    <h5>Indeks Obat</h5>
                    <p>Direktori ribuan merek obat dengan penjelasan lengkap untuk pengetahuan medikasi Anda.</p>
                </div>  
            </div>

            <div class="col-12 col-lg-4 col-md-6 mb-5 row mx-0 px-4">
                <div class="d-inline-block"> <img src="/images/new-icons/business.png" style="height: 50px;" class="mb-3" /> </div>
                <div class="col text-left"> 
                    <h5>Loker Kesehatan</h5>
                    <p>Temukan lowongan pekerjaan di berbagai klinik, rumah sakit, dan tempat praktek lainnya.</p>
                </div>  
            </div>

            <div class="col-12 col-lg-4 col-md-6 mb-5 row mx-0 px-4">
                <div class="d-inline-block"> <img src="/images/new-icons/world.png" style="height: 50px;" class="mb-3" /> </div>
                <div class="col text-left"> 
                    <h5>Kerjasama Bisnis</h5>
                    <p>Bekerjasama dengan kami untuk mendapatkan berbagai benefit untuk bisnis dan institusi Anda.</p>
                </div>  
            </div>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.front', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>