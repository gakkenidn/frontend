<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php echo e(page_title('Backend')); ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo e(mix('css/backend-app.css')); ?>">
	<script type="text/javascript">
		window.App = {
			url: '<?php echo e(route('backend', ['route' => '/'])); ?>',
			rootUrl: '<?php echo e(route('home')); ?>',
			restUrl: '<?php echo e(url('/app/rest/admin')); ?>',
			user: {
				name: '<?php echo e(Auth::user()->name); ?>',
				avatar: '<?php echo e(Auth::user()->avatar_url); ?>'
			}
		};
	</script>
</head>
<body class="bg-light">

	<div id="app"></div>

	<script type="text/javascript" src="<?php echo e(mix('js/BackendApp.js')); ?>"></script>
</body>
</html>