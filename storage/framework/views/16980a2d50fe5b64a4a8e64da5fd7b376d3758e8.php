<section class="footer">
	<div class="container">
		<div class="row justify-content-md-start justify-content-center align-items-start">
			<div class="col-md-8 d-flex justify-content-md-start justify-content-center">
				<div class="mr-5 d-none d-md-block">
					<img src="<?php echo e(asset('images/GakkenIndonesia-Original.svg')); ?>" style="height: 40px" />
				</div>
				<div class="mx-3">
					<h6 class="text-uppercase text-muted">Layanan</h6>
					<ul class="list-unstyled small">
						<li><a href="<?php echo e(route('articles')); ?>">Artikel Kesehatan</a></li>
						<li><a href="<?php echo e(route('p2kb')); ?>">Gakken P2KB&reg;</a></li>
						<li><a href="<?php echo e(route('journals')); ?>">Jurnal Premium</a></li>
						<li><a href="<?php echo e(route('drugs')); ?>">Indeks Obat</a></li>
						<li><a href="<?php echo e(route('service', ['service' => 'genius'])); ?>">GENIUS&reg;</a></li>
					</ul>
				</div>
				<div class="mx-3">
					<h6 class="text-uppercase text-muted">PT. Gakken</h6>
					<ul class="list-unstyled small">
						<li><a href="<?php echo e(env('CORPORATE_URL')); ?>/about">Tentang Kami</a></li>
						<li><a href="<?php echo e(env('CORPORATE_URL')); ?>/category/news">Berita Perusahaan</a></li>
						<li><a href="<?php echo e(env('CORPORATE_URL')); ?>/services">Layanan</a></li>
						<li><a href="<?php echo e(env('CORPORATE_URL')); ?>/schedule">Jadwal</a></li>
					</ul>
				</div>
				<div class="mx-3">
					<h6 class="text-uppercase text-muted">Bantuan</h6>
					<ul class="list-unstyled small">
						<li><a href="<?php echo e(env('CORPORATE_URL')); ?>/syarat-dan-ketentuan">Syarat dan Ketentuan</a></li>
						<li><a href="<?php echo e(env('CORPORATE_URL')); ?>/privacy-policy">Kebijakan Privasi</a></li>
						<li><a href="<?php echo e(env('CORPORATE_URL')); ?>/faq">FAQ</a></li>
					</ul>
				</div> 
			</div>

			<div class="col-md-4 p-4 p-md-2">
				<p class="text-small my-0 pt-0 pb-4">Temukan semua kebutuhan pembelajaran dan keprofesian kesehatan di Gakken Indonesia.</p>
				<div class="d-flex align-items-center no-gutters justify-content-start">
					<a href='https://itunes.apple.com/id/app/gakken-indonesia/id1385305439?mt=8' class="d-block mr-2" target="_blank">
						<img src="/images/appstore-badge.png" style="height: 50px;" />
					</a>
					<a href='https://play.google.com/store/apps/details?id=id.gakkenidn.gakkenmobile' class="d-block" target="_blank">
						<img src="/images/googleplay-badge.png" style="height: 50px;" />
					</a>
				</div>
			</div>

			
		</div>

		

		

		<hr />
		<div class="d-md-flex justify-content-between align-items-center text-muted">
			<div class="small text-md-left text-center mb-md-0 mb-3">
				&copy; 2017 PT. Gakken Health and Education Indonesia
			</div>
			<div class="text-md-right text-center">
				<a class="h5 text-muted mx-1" href="https://www.facebook.com/GakkenIDN"><b class="fa fa-facebook fa-fw"></b></a>
				<a class="h5 text-muted mx-1" href="https://twitter.com/GakkenIDN"><b class="fa fa-twitter fa-fw"></b></a>
				<a class="h5 text-muted mx-1" href="https://www.instagram.com/gakken.idn/"><b class="fa fa-instagram fa-fw"></b></a>
				<a class="h5 text-muted mx-1" href="mailto:support@gakken-idn.id"><b class="fa fa-envelope fa-fw"></b></a>
			</div>
		</div>
	</div>

</section>

<?php if(!Auth::check()): ?>
	<?php echo $__env->make('auth.login-dialog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php else: ?>
	<form accept-charset="utf-8" action="<?php echo e(route('logout')); ?>" method="post" id="logout-form">
		<?php echo csrf_field(); ?>

	</form>
<?php endif; ?>