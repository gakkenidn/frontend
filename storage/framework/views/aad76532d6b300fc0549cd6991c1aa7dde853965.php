<?php $__env->startSection('content'); ?>
    <div class="row justify-content-center align-items-center" style="height: 100%;">
        <div class="col-sm-4 pr-md-5 pt-md-0 pt-5" style="border-right: #eee thin solid;">
            <div class="text-center pb-4 w-100">
                <img src="<?php echo e(asset('images/icon.png')); ?>" class="w-25" /><br />
            </div>
            
            <form role="form" method="POST" action="<?php echo e(route('login')); ?>">
                <?php echo csrf_field(); ?>


                <?php if($errors->has('email') || $errors->has('password')): ?>
                    <div class="alert alert-danger small">
                        Akun tersebut tidak dapat kami temukan. Mohon coba lagi.
                    </div>
                <?php endif; ?>

                <div class="form-group">
                    <input id="email" type="text" class="form-control form-control-minimal" name="email" value="<?php echo e(old('email')); ?>" required autofocus placeholder="Alamat Email">
                </div>

                <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                    <input id="password" type="password" class="form-control form-control-minimal" name="password" required placeholder="Kata Sandi">
                </div>

                <div class="form-group mt-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>> Ingat saya
                        </label>
                    </div>
                    <p class="small text-muted">Harap tidak dicentang jika menggunakan komputer umum.</p>
                </div>

                <div class="form-group mt-5">
                    <button type="submit" class="btn btn-primary btn-block">
                        Masuk <b class="fa fa-angle-right"></b>
                    </button>

                    <div class="text-center text-muted mt-3">
                        <a class="small" href="<?php echo e(route('password.request')); ?>">Lupa kata sandi?</a> |
                        <a class="small" href="<?php echo e(route('register')); ?>">Belum punya akun?</a>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-sm-4 ml-md-5">
            <p class="text-md-left text-center">atau masuk menggunakan akun sosial media favorit Anda</p>
            <a href="<?php echo e(route('provider.login', ['provider' => 'facebook'])); ?>" class="btn btn-facebook d-flex align-items-center py-3 mb-2">
                <div class="mr-3"><img src="<?php echo e(asset('images/facebook-logo.svg')); ?>" height="25" width="25" /></div>
                <div>Masuk dengan Facebook</div>
            </a>
            <a href="<?php echo e(route('provider.login', ['provider' => 'twitter'])); ?>" class="btn btn-twitter d-flex align-items-center py-3 mb-2">
                <div class="mr-3"><img src="<?php echo e(asset('images/twitter-logo.svg')); ?>" height="25" width="25" /></div>
                <div>Masuk dengan Twitter</div>
            </a>
            <a href="<?php echo e(route('provider.login', ['provider' => 'google'])); ?>" class="btn btn-outline-secondary d-flex align-items-center py-3">
                <div class="mr-3"><img src="<?php echo e(asset('images/google-logo.svg')); ?>" height="25" width="25" /></div>
                <div>Masuk dengan Google</div>
            </a>

            <p class="small text-md-center text-center mt-4"><a href="<?php echo e(route('home')); ?>"><i class="fa fa-arrow-left"></i> Kembali ke Beranda</a></p>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>