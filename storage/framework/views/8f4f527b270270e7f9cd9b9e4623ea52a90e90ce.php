<div class="modal fade" id="auth-dialog">
	<div class="modal-dialog">
		<div class="modal-content rounded-sm border-0 text-left">
			<div class="modal-header bg-light border-0">
				<div class="text-center py-5 w-100">
					<img src="<?php echo e(asset('images/icon.png')); ?>" class="w-25" /><br />
				</div>
			</div>
			<div class="modal-body border-0 pt-4 pb-5 px-5">
				<p class="text-muted mb-4">
					Bergabung dengan Gakken Indonesia untuk pembelajaran
					medis yang berkualitas.
				</p>

				<div class="mb-4">
					<a href="<?php echo e(route('provider.login', ['provider' => 'facebook'])); ?>" class="btn btn-facebook d-flex align-items-center py-3 mb-2">
						<div class="mr-3"><img src="<?php echo e(asset('images/facebook-logo.svg')); ?>" height="25" width="25" /></div>
						<div>Masuk dengan Facebook</div>
					</a>
					<a href="<?php echo e(route('provider.login', ['provider' => 'twitter'])); ?>" class="btn btn-twitter d-flex align-items-center py-3 mb-2">
						<div class="mr-3"><img src="<?php echo e(asset('images/twitter-logo.svg')); ?>" height="25" width="25" /></div>
						<div>Masuk dengan Twitter</div>
					</a>
					<a href="<?php echo e(route('provider.login', ['provider' => 'google'])); ?>" class="btn btn-outline-secondary d-flex align-items-center py-3">
						<div class="mr-3"><img src="<?php echo e(asset('images/google-logo.svg')); ?>" height="25" width="25" /></div>
						<div>Masuk dengan Google</div>
					</a>
				</div>

				<div class="text-center">
					<a href="<?php echo e(route('login')); ?>">Masuk / daftar menggunakan email</a>
				</div>

			</div>
		</div>
	</div>
</div>