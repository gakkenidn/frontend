<div class="container d-none d-lg-block">
	<nav class="navbar navbar-light navbar-main navbar-expand-md justify-content-between px-0">
		<div class="d-flex align-items-lg-center align-items-start">
			<a class="navbar-brand" href="<?php echo e(route('home')); ?>"><img src="<?php echo e(asset('images/GakkenIndonesia-Original.svg')); ?>" height="40" /></a>
		</div>
		<div class="d-flex align-items-center">
			<ul class="nav navbar-nav mr-4">
				<?php echo $__env->make('layouts.navbar-nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php echo $__env->make('layouts.navbar-apps', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</ul>
			<?php echo $__env->make('layouts.navbar-account', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		</div>
	</nav>
</div>